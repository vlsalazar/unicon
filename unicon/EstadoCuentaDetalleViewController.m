//
//  EstadoCuentaDetalleViewController.m
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EstadoCuentaDetalleViewController.h"
@implementation EstadoCuentaDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    BOOL esSoles = false;
    BOOL esDolares = false;
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
    numberFormatter.minimumFractionDigits = 2;
    numberFormatter.maximumFractionDigits = 2;
    numberFormatter.perMillSymbol = @",";
    self.acuerdoLbl.text = [self.dicEstadoCuenta objectForKey:@"acuerdoComercial"];
    self.obraLbl.text = [self.dicEstadoCuenta objectForKey:@"descripcionObra"];
    NSString *tipoMoneda = [self.dicEstadoCuenta objectForKey:@"tipoMoneda"];
    if([tipoMoneda isEqualToString:@"S"] || [tipoMoneda isEqualToString:@"A"]){
        esSoles = true;
    }
    if([tipoMoneda isEqualToString:@"D"] || [tipoMoneda isEqualToString:@"A"]){
        esDolares = true;
    }
    if(esSoles){
        self.impFacSolesLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"importeOriginalSoles"] doubleValue]]];
        self.deudaTotalSolesLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"deudaPendienteSoles"] doubleValue]]];
        self.deudaVencidaSolesLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"deudaVencidaSoles"] doubleValue]]];
    }else{
        [self.titleImpFacSolesLbl removeFromSuperview];
        [self.impFacSolesLbl removeFromSuperview];
        [self.titleDeudaTotalSolesLbl removeFromSuperview];
        [self.deudaTotalSolesLbl removeFromSuperview];
        [self.titleDeudaVencidaSolesLbl removeFromSuperview];
        [self.deudaVencidaSolesLbl removeFromSuperview];
    }
    if(esDolares){
        self.impFacDolaresLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"importeOriginalDolares"] doubleValue]]];
        self.deudaTotalDolaresLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"deudaPendienteDolares"] doubleValue]]];
        self.deudaVencidaDolaresLbl.text = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[self.dicEstadoCuenta objectForKey:@"deudaVencidaDolares"] doubleValue]]];
    }else{
        [self.titleImpFacDolaresLbl removeFromSuperview];
        [self.impFacDolaresLbl removeFromSuperview];
        [self.titleDeudaTotalDolaresLbl removeFromSuperview];
        [self.deudaTotalDolaresLbl removeFromSuperview];
        [self.titleDeudaVencidaDolaresLbl removeFromSuperview];
        [self.deudaVencidaDolaresLbl removeFromSuperview];
    }
}
@end