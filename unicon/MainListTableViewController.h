//
//  MainListTableViewController.h
//  unicon
//
//  Created by DSB Mobile on 5/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface MainListTableViewController:UITableViewController
@property(nonatomic,strong) NSString *option;
@property(nonatomic,strong) NSArray *arrList;
@end
