//
//  EstadoCuentaViewController.h
//  unicon
//
//  Created by victor salazar on 6/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EstadoCuentaViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    BOOL esSoles, esDolares;
    NSNumber *totalIFacSoles, *totalIFacDolares, *totalDeudaTotalSales, *totalDeudaTotalDolares, *totalDeudaVencidaSoles, *totalDeudaVencidaDolares;
}
@property(nonatomic,weak) NSArray *arrEstadoCuenta;
@property(nonatomic,weak) IBOutlet UITableView *estadosCuentasTableView;
@property(nonatomic,weak) IBOutlet UILabel *totalIFacSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalIFacDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalDeudaTotalSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalDeudaTotalDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalDeudaVencidaSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalDeudaVencidaDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *iFacSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *iFacDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaDolaresLbl;
@end