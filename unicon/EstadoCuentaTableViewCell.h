//
//  EstadoCuentaTableViewCell.h
//  unicon
//
//  Created by victor salazar on 6/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EstadoCuentaTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *acuerdoLbl;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UILabel *iFacSolesLbl;
@property(nonatomic,weak) IBOutlet UIView *iFacSolesView;
@property(nonatomic,weak) IBOutlet UILabel *iFacDolaresLbl;
@property(nonatomic,weak) IBOutlet UIView *iFacDolaresView;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalSolesLbl;
@property(nonatomic,weak) IBOutlet UIView *deudaTotalSolesView;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalDolaresLbl;
@property(nonatomic,weak) IBOutlet UIView *deudaTotalDolaresView;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaSolesLbl;
@property(nonatomic,weak) IBOutlet UIView *deudaVencidaSolesView;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaDolaresLbl;
@end