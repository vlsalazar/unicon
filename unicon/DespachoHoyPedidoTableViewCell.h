//
//  DespachoHoyOrdenTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 17/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ProgressView.h"
#import "FragmentedView.h"
@interface DespachoHoyPedidoTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *pedidoLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *horaInicioLbl;
@property(nonatomic,weak) IBOutlet UILabel *frecVacLbl;
@property(nonatomic,weak) IBOutlet UILabel *tipoLbl;
@property(nonatomic,weak) IBOutlet UILabel *programadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *avanceLbl;
@property(nonatomic,weak) IBOutlet UILabel *codigoOrdenLbl;
@property(nonatomic,weak) IBOutlet ProgressView *progOrdenView;
@property(nonatomic,weak) IBOutlet UILabel *estadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *tiquetesLbl;
@property(nonatomic,weak) IBOutlet UIButton *mapaBtn;
@property(nonatomic,weak) IBOutlet UIButton *listaBtn;
@property(nonatomic,weak) IBOutlet FragmentedView *porcEstFrgView;
@end