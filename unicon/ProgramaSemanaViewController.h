//
//  TwoEntriesTableViewController.h
//  unicon
//
//  Created by DSB Mobile on 5/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ProgramaSemanaViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *arrPedidos;
}
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UITableView *programasTableView;
@property(nonatomic,weak) NSDictionary *dicObra;
@end