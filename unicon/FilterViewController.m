//
//  FilterTableViewController.m
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "FilterViewController.h"
#import "UserViewController.h"
#import "TextFldTableViewCell.h"
#import "DoubleTextFieldTableViewCell.h"

#define CellTypeTextFld @1
#define CellDoubleTypeTextFld @2

#define TypeTextFldPicker @1
#define TypeTextFldDate @2
#define TypeTextFldEmpresa @3
#define TypeTextFldNumero @4

#define PickerObras @1
#define PickerMetodosFacturacion @2
#define PickerEstado @3
#define PickerTipoResultado @4

@implementation FilterViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrObras = @[];
    arrResult = @[];
    checkDatePicker = false;
    esCreditos = false;
    reloadObras = true;
    NSArray *arrMenu = [ToolBox getMenu];
    NSArray *arrIndexes = [self.option componentsSeparatedByString:@"-"];
    int indEmpTemp = 0;
    int firstIndex = [arrIndexes[0] intValue];
    if(arrIndexes.count == 1){
        self.navigationItem.title = [[arrMenu objectAtIndex:firstIndex] objectForKey:@"title"] ;
    }else{
        int secIndex = [arrIndexes[1] intValue];
        self.navigationItem.title = [[[arrMenu objectAtIndex:firstIndex] objectForKey:@"suboptions"] objectAtIndex:secIndex];
    }
    esCreditos = (firstIndex == 4);
    arrObras = (esCreditos ? [[UsuarioActual currentUser] arrObrasCredito] : [[UsuarioActual currentUser] arrObras]);
    if(arrObras){
        Obra *obraTemp = [[UsuarioActual currentUser] obraSeleccionada];
        if(obraTemp){
            currentObra = obraTemp;
        }else{
            currentObra = [arrObras firstObject];
        }
    }
    NSMutableDictionary *dicObras = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Obras:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"text": (currentObra?currentObra.descripcion:@"SIN OBRAS"), @"textAlig": @"left", @"backColor":[UIColor whiteColor], @"textColor": [UIColor blackColor], @"typeTextFld": TypeTextFldPicker, @"pickerType": PickerObras}]}];
    if([self.option isEqualToString:@"0-0"]){
        checkDatePicker = true;
        tipoConsulta = @"1";
        indEmpTemp = 2;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"minDate": @1, @"date": @1, @"maxDate": @8}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @1, @"date": @8}]}];
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras]];
    }else if([self.option isEqualToString:@"0-1"]){
        indEmpTemp = 1;
        tipoConsulta = @"2";
        [self.exportBtn removeFromSuperview];
        NSMutableDictionary *dicFechaPedido = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Fecha de Pedido", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": @0, @"minDate": [NSNumber numberWithInt:-1], @"maxDate": @1}]}];
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicFechaPedido, dicObras]];
    }else if([self.option isEqualToString:@"0-2"]){
        indEmpTemp = 2;
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": [NSNumber numberWithInt:-15], @"maxDate": [NSNumber numberWithInt:-1]}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"date": [NSNumber numberWithInt:-1], @"maxDate": [NSNumber numberWithInt:-1], @"minDate": [NSNumber numberWithInt:-15]}]}];
        NSDictionary *dicNumPed = @{@"typeCell": CellTypeTextFld, @"title": @"Número de pedido:", @"extra": @{@"typeTextFld": TypeTextFldNumero, @"backColor": [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0], @"textColor": [UIColor blackColor]}};
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras, dicNumPed]];
    }else if([self.option isEqualToString:@"1-0"] || [self.option isEqualToString:@"1-1"] || [self.option isEqualToString:@"1-2"]){
        [self.exportBtn removeFromSuperview];
        indEmpTemp = 2;
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": [NSNumber numberWithInt:-8], @"maxDate": @1}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @1, @"date": @1}]}];
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras]];
    }else if([self.option isEqual:@"2"]){
        indEmpTemp = 2;
        checkDatePicker = true;
        [self.exportBtn removeFromSuperview];
        arrTiposResultados = [ToolBox getArrTipoResultadosCertificados];
        NSMutableDictionary *dicFechaVacIni = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Fecha de Vaciado Inicio:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": [NSNumber numberWithInt:0], @"maxDate": @0}]}];
        NSMutableDictionary *dicFechaVacFin = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Fecha de Vaciado Fin:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @0, @"date": @0}]}];
        NSDictionary *dicNumGuia = @{@"typeCell": CellDoubleTypeTextFld, @"title": @"Número de Guía:"};
        NSDictionary *dicTipoRes = @{@"typeCell": CellTypeTextFld, @"title": @"Tipo de Resultado:", @"extra": @{@"text": [arrTiposResultados.firstObject objectForKey:@"v"], @"textAlig": @"left", @"backColor":[UIColor whiteColor], @"textColor": [UIColor blackColor], @"typeTextFld": TypeTextFldPicker, @"pickerType": PickerTipoResultado}};
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicFechaVacIni, dicFechaVacFin, dicObras, dicNumGuia, dicTipoRes]];
    }else if([self.option isEqualToString:@"3-0"]){
        indEmpTemp = 2;
        [self.exportBtn removeFromSuperview];
        arrMetodosFacturacion = [ToolBox getArrMetodosFacturacionAcuerdosComerciales];
        arrEstados = [ToolBox getArrEstadosAcuerdosComerciales];
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": @0, @"maxDate": @0}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @0, @"date": @0}]}];
        NSDictionary *dicMetodosFacturacion = @{@"typeCell": CellTypeTextFld, @"title": @"Método de facturación:", @"extra": @{@"text": [arrMetodosFacturacion.firstObject objectForKey:@"v"], @"textAlig": @"left", @"backColor":[UIColor whiteColor], @"textColor": [UIColor blackColor], @"typeTextFld": TypeTextFldPicker, @"pickerType": PickerMetodosFacturacion}};
        NSDictionary *dicEstado = @{@"typeCell": CellTypeTextFld, @"title": @"Estado:", @"extra": @{@"text": [arrMetodosFacturacion.firstObject objectForKey:@"v"], @"textAlig": @"left", @"backColor":[UIColor whiteColor], @"textColor": [UIColor blackColor], @"typeTextFld": TypeTextFldPicker, @"pickerType": PickerEstado}};
        NSDictionary *dicNumAcuerdo = @{@"typeCell": CellTypeTextFld, @"title": @"Número de Acuerdo:", @"extra": @{@"typeTextFld": TypeTextFldNumero, @"backColor": [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0], @"textColor": [UIColor blackColor]}};
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras, dicMetodosFacturacion, dicEstado, dicNumAcuerdo]];
    }else if([self.option isEqualToString:@"4-0"]){
        indEmpTemp = 0;
        arrInfoCells = [NSMutableArray arrayWithObject:dicObras];
    }else if([self.option isEqualToString:@"4-1"]){
        indEmpTemp = 2;
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": @0, @"maxDate": @0}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @0, @"date": @0}]}];
        NSDictionary *dicAcuerdo = @{@"typeCell": CellTypeTextFld, @"title": @"Acuerdo:", @"extra": @{@"typeTextFld": TypeTextFldNumero, @"backColor": [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0], @"textColor": [UIColor blackColor]}};
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras, dicAcuerdo]];
    }else if([self.option isEqualToString:@"4-2"]){
        indEmpTemp = 2;
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell": CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor": [ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": @0, @"maxDate": @0}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"minDate": @0, @"date": @0}]}];
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicObras]];
    }else if([self.option isEqualToString:@"4-3"]){
        [self.exportBtn removeFromSuperview];
        reloadObras = false;
        indEmpTemp = 2;
        checkDatePicker = true;
        NSMutableDictionary *dicDesde = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Desde el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[ToolBox getColorYellow], @"textColor": [ToolBox getColorBlue], @"typeTextFld": TypeTextFldDate, @"date": @0, @"maxDate": @0}]}];
        NSMutableDictionary *dicHasta = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Hasta el:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"textAlig": @"center", @"backColor":[UIColor lightGrayColor], @"textColor": [UIColor whiteColor], @"typeTextFld": TypeTextFldDate, @"date": @0, @"minDate": @0}]}];
        NSDictionary *dicDocVenta = @{@"typeCell": CellDoubleTypeTextFld, @"title": @"Documento de Venta:"};
        NSDictionary *dicNumComp = @{@"typeCell": CellDoubleTypeTextFld, @"title": @"Número de Comprobante:"};
        NSDictionary *dicAcuerdo = @{@"typeCell": CellTypeTextFld, @"title": @"Acuerdo:", @"extra": @{@"typeTextFld": TypeTextFldNumero, @"backColor": [UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1.0], @"textColor": [UIColor blackColor]}};
        arrInfoCells = [NSMutableArray arrayWithArray:@[dicDesde, dicHasta, dicDocVenta, dicNumComp, dicAcuerdo]];
    }else if([self.option isEqualToString:@"4-4"]){
        reloadObras = false;
        arrInfoCells = [NSMutableArray array];
    }
    if([[UsuarioActual currentUser] esUsuarioUnicon]){
        indEmpCell = indEmpTemp;
        indObrCell = indEmpTemp + 1;
        Empresa *empresa = [[UsuarioActual currentUser] empresaSeleccionada];
        NSString *strEmpresa = empresa ? empresa.descripcionCliente : @"SELECCIONE";
        NSMutableDictionary *dicEmpresas = [NSMutableDictionary dictionaryWithDictionary:@{@"typeCell":CellTypeTextFld, @"title": @"Empresas:", @"extra": [NSMutableDictionary dictionaryWithDictionary:@{@"typeTextFld": TypeTextFldEmpresa, @"text": strEmpresa, @"textAlig": @"left", @"backColor":[UIColor whiteColor], @"textColor": [UIColor blackColor]}]}];
        [arrInfoCells insertObject:dicEmpresas atIndex:indEmpTemp];
        if(empresa){
            if((arrObras == nil) && !(esCreditos ? [[UsuarioActual currentUser] pidioObrasCreditos] : [[UsuarioActual currentUser] pidioObras])){
                [self loadObras];
            }
        }
    }
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [self.filterTableView setScrollEnabled:false];
        self.heightMainView.priority = 900;
        self.heightMainView.constant = arrInfoCells.count*40 + 40;
    }
    UIBarButtonItem *menuBarBtnItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"menuSI"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(actionMenu:)];
    self.navigationItem.leftBarButtonItem = menuBarBtnItem;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(arrResult){
            arrResult = nil;
        }
    }
    if(currentData){
        currentData = nil;
    }
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(currentTxtFld){
        [currentTxtFld resignFirstResponder];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showList"]){
        [segue.destinationViewController setValue:self.option forKey:@"option"];
        [segue.destinationViewController setValue:arrResult forKey:@"arrList"];
    }else if([segue.identifier isEqualToString:@"showProgamaSemanaViewCont"]){
        [segue.destinationViewController setValue:dicObra forKey:@"dicObra"];
    }else if([segue.identifier isEqualToString:@"showReporteHistoricoLista"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrReportes"];
        [segue.destinationViewController setValue:dicParamsRepHis forKey:@"dicParams"];
    }else if([segue.identifier isEqualToString:@"showCertificados"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrCertificados"];
    }else if([segue.identifier isEqualToString:@"showAcuerComer"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrAcuerComers"];
    }else if([segue.identifier isEqualToString:@"showEstadoCuenta"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrEstadoCuenta"];
    }else if([segue.identifier isEqualToString:@"showEntregas"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrEntregas"];
    }else if([segue.identifier isEqualToString:@"showHistorialPagos"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrHistorialPagos"];
    }else if([segue.identifier isEqualToString:@"showComprobantes"]){
        [segue.destinationViewController setValue:arrResult forKey:@"arrComprobantes"];
    }else if([segue.identifier isEqualToString:@"showEstadoCredito"]){
        [segue.destinationViewController setValue:arrResult.firstObject forKey:@"dicEstadoCredito"];
    }
}
#pragma mark - IBAction
-(void)actionMenu:(id)sender{
    [currentTxtFld resignFirstResponder];
    UserViewController *usuViewCont = (UserViewController *)self.navigationController.parentViewController;
    [usuViewCont showSideMenuWithOption:-1];
}
-(IBAction)dissmiss:(id)sender{
    if(currentTxtFld){
        [currentTxtFld resignFirstResponder];
    }
}
-(IBAction)actionSearch:(id)sender{
    isSearch = true;
    [self initializeConnection];
}
-(IBAction)actionExport:(id)sender{
    isSearch = false;
    [self initializeConnection];
}
-(void)initializeConnection{
    [currentTxtFld resignFirstResponder];
    if([[UsuarioActual currentUser] esUsuarioUnicon]){
        if(![[UsuarioActual currentUser] empresaSeleccionada]){
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Alerta" message:@"Seleccione una empresa" preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertCont animated:true completion:nil];
            return;
        }
    }
    if([self.option isEqualToString:@"0-0"]){
        NSString *strUrl = isSearch?[ServiceUrl getUrlProgramaSemana]:[ServiceUrl getUrlProgramaSemanaPDF];
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario], @"codigoCliente": [UsuarioActual codigoCliente], @"codigoObra": currentObra?currentObra.codigo:@"", @"tipoConsulta": tipoConsulta};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }else if([self.option isEqualToString:@"0-1"]){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": desdeCell.valueTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario], @"codigoCliente": [UsuarioActual codigoCliente], @"codigoObra": currentObra?currentObra.codigo:@"", @"tipoConsulta": tipoConsulta};
        [[UsuarioActual currentUser] setDicBusqDespHoy:dicParams];
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlProgramaSemana] andParams:dicParams];
    }else if([self.option isEqualToString:@"0-2"]){
        NSString *strUrl = isSearch?[ServiceUrl getUrlReporteHistoricoResumen]:[ServiceUrl getUrlReporteHistoricoPDF];
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        TextFldTableViewCell *pedidoCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(indEmpCell > 0)?4:3 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario], @"codigoCliente": [UsuarioActual codigoCliente], @"codigoObra": currentObra?currentObra.codigo:@"", @"pedido": pedidoCell.valueTxtFld.text};
        dicParamsRepHis = dicParams;
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }else if([self.option isEqualToString:@"1-0"] || [self.option isEqualToString:@"1-1"] || [self.option isEqualToString:@"1-2"]){
        isSearch = false;
        NSArray *arrOption = [self.option componentsSeparatedByString:@"-"];
        int option = [[arrOption objectAtIndex:1] intValue];
        NSArray *arrUrls = @[[ServiceUrl getUrlGestionDescarga], [ServiceUrl getUrlGestionProgramacion], [ServiceUrl getUrlPuntualidadServicio]];
        NSString *strUrl = [arrUrls objectAtIndex:option];
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario], @"codigoCliente": [UsuarioActual codigoCliente], @"codigoObra": currentObra?currentObra.codigo:@""};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }else if([self.option isEqualToString:@"2"]){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        DoubleTextFieldTableViewCell *numGuiaCell = (DoubleTextFieldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(indEmpCell > 0)?4:3 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoObra": currentObra?currentObra.codigo:@"", @"codigoCliente": [UsuarioActual codigoCliente], @"UsuarioActual": [UsuarioActual codigoUsuario], @"serieGuia": numGuiaCell.firstTxtFld.text, @"numeroGuia": numGuiaCell.secondTxtFld.text, @"tipoResultado": [[arrTiposResultados objectAtIndex:indTipoRes] objectForKey:@"k"], @"fgMuestreo": @"S"};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlCertificados] andParams:dicParams];
    }else if([self.option isEqualToString:@"3-0"]){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        TextFldTableViewCell *numAcuerdoCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((indEmpCell > 0)?6:5) inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoObra": currentObra?currentObra.codigo:@"", @"codigoCliente": [UsuarioActual codigoCliente], @"codigoUsuario": [UsuarioActual codigoUsuario], @"estado": [[arrEstados objectAtIndex:indEstado] objectForKey:@"k"], @"metodoFacturacion": [[arrMetodosFacturacion objectAtIndex:indMetFac] objectForKey:@"k"], @"codigoAcuerdo": numAcuerdoCell.valueTxtFld.text};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlAcuerdosComerciales] andParams:dicParams];
    }else if([self.option isEqualToString:@"4-0"]){
        NSString *strUrl = isSearch?[ServiceUrl getUrlEstadoCuenta]:[ServiceUrl getUrlEstadoCuentaPDF];
        NSDictionary *dicParams = @{@"codigoCliente": [UsuarioActual codigoCliente], @"codigoObra": currentObra?currentObra.codigo:@"", @"codigoUsuario": [UsuarioActual codigoUsuario]};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }else if([self.option isEqualToString:@"4-1"]){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        NSCalendar *gregCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
        dateFormatter.calendar = gregCalendar;
        dateFormatter.dateFormat = @"dd/MM/yyyy";
        NSDate *desdeDate = [dateFormatter dateFromString:desdeCell.valueTxtFld.text];
        NSDate *hastaDate = [dateFormatter dateFromString:hastaCell.valueTxtFld.text];
        NSDateComponents *dateComps = [gregCalendar components:NSCalendarUnitDay fromDate:desdeDate toDate:hastaDate options:0];
        int days = (int) dateComps.day;
        if(days > 15){
            [ToolBox showAlertaWithMessage:@"El intervalo de fechas debe ser máximo 15 días." inViewCont:self];
            return;
        }else{
            NSString *strUrl = isSearch?[ServiceUrl getUrlEntregaClienteObra]:[ServiceUrl getUrlEntregaClienteObraPDF];
            TextFldTableViewCell *acuerdoCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((indEmpCell > 0)?4:3) inSection:0]];
            NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoObra": currentObra?currentObra.codigo:@"", @"codigoCliente": [UsuarioActual codigoCliente], @"codigoAcuerdo": acuerdoCell.valueTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario]};
            currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
        }
    }else if([self.option isEqualToString:@"4-2"]){
        NSString *strUrl = isSearch?[ServiceUrl getUrlHistorialPagos]:[ServiceUrl getUrlHistorialPagosPDF];
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoObra": currentObra?currentObra.codigo:@"", @"codigoCliente": [UsuarioActual codigoCliente], @"codigoUsuario": [UsuarioActual codigoUsuario]};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }else if([self.option isEqualToString:@"4-3"]){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        TextFldTableViewCell *acuerdoCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((indEmpCell > 0)?5:4) inSection:0]];
        DoubleTextFieldTableViewCell *docVentaCell = (DoubleTextFieldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(indEmpCell > 0)?3:2 inSection:0]];
        DoubleTextFieldTableViewCell *numCompCell = (DoubleTextFieldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(indEmpCell > 0)?4:3 inSection:0]];
        NSDictionary *dicParams = @{@"desde": desdeCell.valueTxtFld.text, @"hasta": hastaCell.valueTxtFld.text, @"codigoCliente": [UsuarioActual codigoCliente], @"codigoAcuerdo": acuerdoCell.valueTxtFld.text, @"serieComprobante": numCompCell.firstTxtFld.text, @"numeroComprobante": numCompCell.secondTxtFld.text, @"serieDocumento": docVentaCell.firstTxtFld.text, @"numeroDocumento": docVentaCell.secondTxtFld.text, @"codigoUsuario": [UsuarioActual codigoUsuario]};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlComprobanteRecepcion] andParams:dicParams];
    }else if([self.option isEqualToString:@"4-4"]){
        NSString *strUrl = isSearch?[ServiceUrl getUrlEstadoCredito]:[ServiceUrl getUrlEstadoCreditoPDF];
        NSDictionary *dicParams = @{@"codigoCliente": [UsuarioActual codigoCliente], @"codigoUsuario": [UsuarioActual codigoUsuario]};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:strUrl andParams:dicParams];
    }
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    currentData = [NSMutableData data];
    [currentCon start];
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrInfoCells.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicInfoCell = arrInfoCells[indexPath.row];
    NSNumber *nTypeCell = [dicInfoCell objectForKey:@"typeCell"];
    if([nTypeCell isEqualToNumber:CellTypeTextFld]){
        NSDictionary *dicExtra = dicInfoCell[@"extra"];
        NSString *strTextAlig = [dicExtra objectForKey:@"textAlig"];
        TextFldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textFldCell" forIndexPath:indexPath];
        cell.contentView.layoutMargins = UIEdgeInsetsZero;
        cell.contentView.preservesSuperviewLayoutMargins = false;
        cell.titleLbl.text = dicInfoCell[@"title"];
        cell.valueTxtFld.textAlignment = ([strTextAlig isEqualToString:@"center"])?NSTextAlignmentCenter:NSTextAlignmentLeft;
        UIColor *backColor = [dicExtra objectForKey:@"backColor"];
        cell.valueTxtFld.backgroundColor = backColor;
        cell.valueTxtFld.borderStyle = UITextBorderStyleNone;
        cell.valueTxtFld.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0] CGColor];
        if([backColor isEqual:[UIColor whiteColor]]){
            cell.valueTxtFld.layer.borderWidth = 1.0;
        }else{
            cell.valueTxtFld.layer.borderWidth = 0.0;
        }
        cell.valueTxtFld.textColor = [dicExtra objectForKey:@"textColor"];
        cell.valueTxtFld.inputView = nil;
        NSNumber *typeTextFld = [dicExtra objectForKey:@"typeTextFld"];
        cell.valueTxtFld.tag = [typeTextFld intValue];
        if([typeTextFld isEqualToNumber:TypeTextFldDate]){
            NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
            NSDate *today = [NSDate date];
            NSDateComponents *dateComps = [NSDateComponents new];
            dateComps.day = [dicExtra[@"date"] intValue];
            UIDatePicker *datePicker = [[UIDatePicker alloc] init];
            datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"es_ES"];
            datePicker.datePickerMode = UIDatePickerModeDate;
            datePicker.date = [calendar dateByAddingComponents:dateComps toDate:today options:0];
            if(dicExtra[@"minDate"]){
                dateComps.day = [dicExtra[@"minDate"] intValue];
                datePicker.minimumDate = [calendar dateByAddingComponents:dateComps toDate:today options:0];
            }
            if(dicExtra[@"maxDate"]){
                dateComps.day = [dicExtra[@"maxDate"] intValue];
                datePicker.maximumDate = [calendar dateByAddingComponents:dateComps toDate:today options:0];
            }
            datePicker.tag = indexPath.row;
            [datePicker addTarget:self action:@selector(datePickerChangeValue:) forControlEvents:UIControlEventValueChanged];
            cell.valueTxtFld.text = [ToolBox convertDateToString:datePicker.date];
            cell.valueTxtFld.inputView = datePicker;
        }else if([typeTextFld isEqualToNumber:TypeTextFldPicker]){
            cell.valueTxtFld.text = dicExtra[@"text"];
            UIPickerView *pickerView = [UIPickerView new];
            pickerView.tag = [dicExtra[@"pickerType"] intValue];
            pickerView.dataSource = self;
            pickerView.delegate = self;
            cell.valueTxtFld.inputView = pickerView;
        }else if ([typeTextFld isEqualToNumber:TypeTextFldEmpresa]){
            cell.valueTxtFld.text = [dicExtra objectForKey:@"text"];
        }else if([typeTextFld isEqualToNumber:TypeTextFldNumero]){
            cell.valueTxtFld.keyboardType = UIKeyboardTypeNumberPad;
        }
        return cell;
    }else if([nTypeCell isEqualToNumber:CellDoubleTypeTextFld]){
        DoubleTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"doubleTextFldCell" forIndexPath:indexPath];
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        cell.titleLbl.text = [dicInfoCell objectForKey:@"title"];
        return cell;
    }
    return nil;
}
#pragma mark - UITextField
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField.tag == TypeTextFldEmpresa.intValue){
        [self performSegueWithIdentifier:@"showEmpresas" sender:self];
        return false;
    }else{
        return true;
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    currentTxtFld = textField;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([self.option isEqualToString:@"2"]){
        if(textField.tag == 100){
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if (newText.length > 3) {
                return false;
            }
        }else if (textField.tag == 101){
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if (newText.length > 7) {
                return false;
            }
        }
    }else if([self.option isEqualToString:@"4-1"]){
        if(textField.tag == TypeTextFldNumero.intValue){
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if (newText.length > 6) {
                return false;
            }
        }
    }else if([self.option isEqualToString:@"4-3"]){
        if(textField.tag == 100){
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if (newText.length > 3) {
                return false;
            }
        }else if(textField.tag == 101){
            DoubleTextFieldTableViewCell *cell = (DoubleTextFieldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:((indEmpCell > 0)?3:2) inSection:0]];
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if(newText.length > ([textField isEqual:cell.secondTxtFld]?8:7)){
                return false;
            }
        }
    }
    return true;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    currentTxtFld = nil;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}
#pragma mark - UIDatePicker
-(void)datePickerChangeValue:(UIDatePicker *)datePicker{
    if(checkDatePicker){
        TextFldTableViewCell *desdeCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TextFldTableViewCell *hastaCell = (TextFldTableViewCell *)[self.filterTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        UIDatePicker *desdeDatePicker = (UIDatePicker *)desdeCell.valueTxtFld.inputView;
        UIDatePicker *hastaDatePicker = (UIDatePicker *)hastaCell.valueTxtFld.inputView;
        if([datePicker isEqual:desdeDatePicker]){
            [hastaDatePicker setMinimumDate:[datePicker date]];
        }else{
            if([datePicker isEqual:hastaDatePicker]){
                [desdeDatePicker setMaximumDate:[datePicker date]];
            }
        }
    }
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDate *today = [NSDate date];
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitDay fromDate:today toDate:datePicker.date options:0];
    if(datePicker.tag == 0){
        NSMutableDictionary *dicDesde = [arrInfoCells objectAtIndex:0];
        NSMutableDictionary *dicExtraDesde = [dicDesde objectForKey:@"extra"];
        [dicExtraDesde setObject:[NSNumber numberWithInteger:dateComps.day] forKey:@"date"];
        if(checkDatePicker){
            NSMutableDictionary *dicHasta = [arrInfoCells objectAtIndex:1];
            NSMutableDictionary *dicExtraHasta = [dicHasta objectForKey:@"extra"];
            [dicExtraHasta setObject:[NSNumber numberWithInteger:dateComps.day] forKey:@"minDate"];
        }
    }else{
        NSMutableDictionary *dicHasta = [arrInfoCells objectAtIndex:1];
        NSMutableDictionary *dicExtraHasta = [dicHasta objectForKey:@"extra"];
        [dicExtraHasta setObject:[NSNumber numberWithInteger:dateComps.day] forKey:@"date"];
        if(checkDatePicker){
            NSMutableDictionary *dicDesde = [arrInfoCells objectAtIndex:0];
            NSMutableDictionary *dicExtraDesde = [dicDesde objectForKey:@"extra"];
            [dicExtraDesde setObject:[NSNumber numberWithInteger:dateComps.day] forKey:@"maxDate"];
        }
    }
    currentTxtFld.text = [ToolBox convertDateToString:[datePicker date]];
}
#pragma mark - UIPicker
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == PickerObras.intValue){
        if(arrObras){
            return arrObras.count;
        }else{
            return 1;
        }
    }else if (pickerView.tag == PickerMetodosFacturacion.intValue){
        return arrMetodosFacturacion.count;
    }else if (pickerView.tag == PickerEstado.intValue){
        return arrEstados.count;
    }else if (pickerView.tag == PickerTipoResultado.intValue){
        return arrTiposResultados.count;
    }
    return 0;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == PickerObras.intValue){
        if(arrObras){
            return [arrObras[row] descripcion];
        }else{
            return @"SIN OBRAS";
        }
    }else if (pickerView.tag == PickerMetodosFacturacion.intValue){
        return [[arrMetodosFacturacion objectAtIndex:row] objectForKey:@"v"];
    }else if (pickerView.tag == PickerEstado.intValue){
        return [[arrEstados objectAtIndex:row] objectForKey:@"v"];
    }else if (pickerView.tag == PickerTipoResultado.intValue){
        return [[arrTiposResultados objectAtIndex:row] objectForKey:@"v"];
    }
    return @"";
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == PickerObras.intValue){
        if(arrObras){
            currentObra = [arrObras objectAtIndex:row];
            [[UsuarioActual currentUser] setObraSeleccionada:currentObra];
            currentTxtFld.text = currentObra.descripcion;
        }
    }else if(pickerView.tag == PickerMetodosFacturacion.intValue){
        indMetFac = (int)row;
        currentTxtFld.text = [[arrMetodosFacturacion objectAtIndex:indMetFac] objectForKey:@"v"];
    }else if(pickerView.tag == PickerEstado.intValue){
        indEstado = (int)row;
        currentTxtFld.text = [[arrEstados objectAtIndex:indEstado] objectForKey:@"v"];
    }else if (pickerView.tag == PickerTipoResultado.intValue){
        indTipoRes = (int)row;
        currentTxtFld.text = [[arrTiposResultados objectAtIndex:indTipoRes] objectForKey:@"v"];
    }
}
#pragma mark - NSURLConnection
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [currentData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [currentData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [SVProgressHUD dismiss];
    if([ToolBox isErrorConnectionInternet:error.code]){
        [ToolBox showAlertaErrorConnectionInViewCont:self];
    }else{
        if(error.code == NSURLErrorTimedOut){
            NSLog(@"[%s] error: %@", __FUNCTION__, error.localizedDescription);
            [ToolBox showAlertaTimeOutInViewCont:self];
        }else{
            [ToolBox showAlertaErrorInViewCont:self];
        }
    }
    if(self.contentView.subviews.count > 0){
        self.navigationItem.rightBarButtonItem = nil;
        [self.contentView.subviews.firstObject removeFromSuperview];
        [self.childViewControllers.firstObject removeFromParentViewController];
    }
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    if(self.contentView.subviews.count > 0){
        self.navigationItem.rightBarButtonItem = nil;
        [self.contentView.subviews.firstObject removeFromSuperview];
        [self.childViewControllers.firstObject removeFromParentViewController];
    }
    if(isSearch){
        NSError *jsonError;
        id dicResultTemp = [NSJSONSerialization JSONObjectWithData:currentData options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonError];
        if(jsonError){
            [ToolBox showAlertaErrorInViewCont:self];
        }else{
            if([dicResultTemp isEqual:[NSNull null]]){
                [ToolBox showNoDataInViewCont:self];
            }else{
                if([self.option isEqualToString:@"0-0"]){
                    id obrasEnDespacho = [dicResultTemp objectForKey:@"obraEnDespachoBean"];
                    if([obrasEnDespacho isKindOfClass:[NSDictionary class]]){
                        dicObra = obrasEnDespacho;
                        [self performSegueWithIdentifier:@"showProgamaSemanaViewCont" sender:self];
                    }else{
                        arrResult = obrasEnDespacho;
                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"MainListTableViewCont"];
                            [viewCont setValue:self.option forKey:@"option"];
                            [viewCont setValue:arrResult forKey:@"arrList"];
                            CGRect frame = viewCont.view.frame;
                            frame.size = self.contentView.frame.size;
                            viewCont.view.frame = frame;
                            [self addChildViewController:viewCont];
                            [self.contentView addSubview:viewCont.view];
                        }else{
                            [self performSegueWithIdentifier:@"showList" sender:self];
                        }
                    }
                }else if([self.option isEqualToString:@"0-1"]){
                    id obrasEnDespacho = [dicResultTemp objectForKey:@"obraEnDespachoBean"];
                    if([obrasEnDespacho isKindOfClass:[NSDictionary class]]){
                        obrasEnDespacho = @[obrasEnDespacho];
                    }
                    arrResult = obrasEnDespacho;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"MainListTableViewCont"];
                        [viewCont setValue:self.option forKey:@"option"];
                        [viewCont setValue:arrResult forKey:@"arrList"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showList" sender:self];
                    }
                }else if([self.option isEqualToString:@"0-2"]){
                    id despachosCSBean = [dicResultTemp objectForKey:@"despachosCSBean"];
                    if([despachosCSBean isKindOfClass:[NSDictionary class]]){
                        despachosCSBean = @[despachosCSBean];
                    }
                    arrResult = despachosCSBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"RepHisListViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrReportes"];
                        [viewCont setValue:dicParamsRepHis forKey:@"dicParams"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showReporteHistoricoLista" sender:self];
                    }
                }else if([self.option isEqualToString:@"2"]){
                    id guiaDeRemisionBean = [dicResultTemp objectForKey:@"guiaDeRemisionBean"];
                    if([guiaDeRemisionBean isKindOfClass:[NSDictionary class]]){
                        guiaDeRemisionBean = @[guiaDeRemisionBean];
                    }
                    arrResult = guiaDeRemisionBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"CertificadosViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrCertificados"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showCertificados" sender:self];
                    }
                }else if([self.option isEqualToString:@"3-0"]){
                    id acuerdoBean = [dicResultTemp objectForKey:@"acuerdoBean"];
                    if([acuerdoBean isKindOfClass:[NSDictionary class]]){
                        acuerdoBean = @[acuerdoBean];
                    }
                    arrResult = acuerdoBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"AcuerComerViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrAcuerComers"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showAcuerComer" sender:self];
                    }
                }else if([self.option isEqualToString:@"4-0"]){
                    id estadoCuentaBean = [dicResultTemp objectForKey:@"estadoCuentaBean"];
                    if([estadoCuentaBean isKindOfClass:[NSDictionary class]]){
                        estadoCuentaBean = @[estadoCuentaBean];
                    }
                    arrResult = estadoCuentaBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"EstadoCuentaViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrEstadoCuenta"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showEstadoCuenta" sender:self];
                    }
                }else if([self.option isEqualToString:@"4-1"]){
                    id detalleEntregaClienteObraBean = [dicResultTemp objectForKey:@"detalleEntregaClienteObraBean"];
                    if([detalleEntregaClienteObraBean isKindOfClass:[NSDictionary class]]){
                        detalleEntregaClienteObraBean = @[detalleEntregaClienteObraBean];
                    }
                    arrResult = detalleEntregaClienteObraBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"EntregasViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrEntregas"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showEntregas" sender:self];
                    }
                }else if([self.option isEqualToString:@"4-2"]){
                    id historialDePagoBean = [dicResultTemp objectForKey:@"historialDePagoBean"];
                    if([historialDePagoBean isKindOfClass:[NSDictionary class]]){
                        historialDePagoBean = @[historialDePagoBean];
                    }
                    arrResult = historialDePagoBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"HistorialPagosViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrHistorialPagos"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showHistorialPagos" sender:self];
                    }
                }else if([self.option isEqualToString:@"4-3"]){
                    id documentoPercepcionBean = [dicResultTemp objectForKey:@"documentoPercepcionBean"];
                    if([documentoPercepcionBean isKindOfClass:[NSDictionary class]]){
                        documentoPercepcionBean = @[documentoPercepcionBean];
                    }
                    arrResult = documentoPercepcionBean;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ComprobantesViewCont"];
                        [viewCont setValue:arrResult forKey:@"arrComprobantes"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showComprobantes" sender:self];
                    }
                }else if([self.option isEqualToString:@"4-4"]){
                    arrResult = @[dicResultTemp];
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"EstadoCreditoViewCont"];
                        [viewCont setValue:arrResult.firstObject forKey:@"dicEstadoCredito"];
                        CGRect frame = viewCont.view.frame;
                        frame.size = self.contentView.frame.size;
                        viewCont.view.frame = frame;
                        [self addChildViewController:viewCont];
                        [self.contentView addSubview:viewCont.view];
                    }else{
                        [self performSegueWithIdentifier:@"showEstadoCredito" sender:self];
                    }
                }
            }
        }
    }else{//Export
        if(currentData.length == 0){
            [ToolBox showNoDataInViewCont:self];
        }else{
            NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *strDocumentDirectory = arr.firstObject;
            NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
            [currentData writeToFile:filePath atomically:true];
            UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
            docIntCont.name = @"";
            docIntCont.delegate = self;
            [docIntCont presentPreviewAnimated:true];
        }
    }
}
#pragma mark - Auxiliar
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
-(void)loadObras{
    if(reloadObras){
        UsuarioActual *currentUser = [UsuarioActual currentUser];
        NSDictionary *dicParams = @{@"codigoCliente": currentUser.empresaSeleccionada.codigoCliente, @"codigoUsuario": @""};
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        [ServiceConector connectToUrl:(esCreditos?[ServiceUrl getUrlObrasCredito]:[ServiceUrl getUrlObras]) withParameters:dicParams withResponse:^(id receivedData, NSError *error){
            [SVProgressHUD dismiss];
            if(error){
                if([ToolBox isErrorConnectionInternet:error.code]){
                    [ToolBox showAlertaErrorConnectionInViewCont:self];
                }else{
                    if(error.code == NSURLErrorTimedOut){
                        [ToolBox showAlertaTimeOutInViewCont:self];
                    }else{
                        [ToolBox showAlertaErrorInViewCont:self];
                    }
                }
            }else{
                NSError *jsonError;
                id dicObras = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError){
                    [ToolBox showAlertaErrorInViewCont:self];
                }else{
                    NSMutableDictionary *dicEmpresas = [arrInfoCells objectAtIndex:indEmpCell];
                    Empresa *empresa = [[UsuarioActual currentUser] empresaSeleccionada];
                    NSMutableDictionary *dicExtraEmp = [dicEmpresas objectForKey:@"extra"];
                    [dicExtraEmp setObject:empresa.descripcionCliente forKey:@"text"];
                    if([dicObras isEqual:[NSNull null]]){
                        arrObras = nil;
                        currentObra = nil;
                    }else{
                        NSMutableArray *arrObrasTemp = [NSMutableArray array];
                        Obra *obraTodo = [Obra new];
                        obraTodo.codigo = @"";
                        obraTodo.descripcion = @"TODOS";
                        [arrObrasTemp addObject:obraTodo];
                        id arrList = [dicObras objectForKey:@"obra"];
                        if([arrList isKindOfClass:[NSDictionary class]]){
                            arrList = @[arrList];
                        }
                        for(NSDictionary *dicObraTemp in arrList){
                            Obra *obraTemp = [Obra new];
                            obraTemp.codigo = [dicObraTemp objectForKey:@"codigo"];
                            obraTemp.descripcion = [dicObraTemp objectForKey:@"descripcion"];
                            [arrObrasTemp addObject:obraTemp];
                        }
                        arrObras = arrObrasTemp;
                        currentObra = arrObras.firstObject;
                    }
                    if(esCreditos){
                        [[UsuarioActual currentUser] setArrObrasCredito:arrObras];
                        [[UsuarioActual currentUser] setPidioObrasCreditos:true];
                    }else{
                        [[UsuarioActual currentUser] setArrObras:arrObras];
                        [[UsuarioActual currentUser] setPidioObras:true];
                    }
                    NSMutableDictionary *dicObras = [arrInfoCells objectAtIndex:indObrCell];
                    NSMutableDictionary *dicExtraObra = [dicObras objectForKey:@"extra"];
                    [dicExtraObra setObject:(currentObra?currentObra.descripcion:@"SIN OBRAS") forKey:@"text"];
                    [self.filterTableView beginUpdates];
                    [self.filterTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indEmpCell inSection:0], [NSIndexPath indexPathForRow:indObrCell inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self.filterTableView endUpdates];
                }
            }
        }];
    }else{
        NSMutableDictionary *dicEmpresas = [arrInfoCells objectAtIndex:indEmpCell];
        Empresa *empresa = [[UsuarioActual currentUser] empresaSeleccionada];
        NSMutableDictionary *dicExtraEmp = [dicEmpresas objectForKey:@"extra"];
        [dicExtraEmp setObject:empresa.descripcionCliente forKey:@"text"];
        [self.filterTableView beginUpdates];
        [self.filterTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:indEmpCell inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self.filterTableView endUpdates];
    }
}
@end
