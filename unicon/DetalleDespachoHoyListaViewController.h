//
//  DetalleDespachoHoyListaViewController.h
//  unicon
//
//  Created by victor salazar on 23/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface DetalleDespachoHoyListaViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicDespacho;
@property(nonatomic,weak) NSString *strTitle;
@property(nonatomic,weak) IBOutlet UITextView *detailTxtView;
@end