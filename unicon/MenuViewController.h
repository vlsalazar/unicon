//
//  MenuViewController.h
//  unicon
//
//  Created by DSB Mobile on 1/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface MenuViewController:UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    UIView *sideMenuView;
    NSString *selectedOption;
    NSArray *arrMenu;
}
@property(nonatomic,weak) IBOutlet UICollectionView *menuCollectionView;
-(void)optionSelected:(NSString *)option;
@end