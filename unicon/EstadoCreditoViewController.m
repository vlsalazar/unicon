//
//  EstadoCreditoViewController.m
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EstadoCreditoViewController.h"
@implementation EstadoCreditoViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    double vencida = [[self.dicEstadoCredito objectForKey:@"vencida"] doubleValue];
    double porVencer = [[self.dicEstadoCredito objectForKey:@"porVencer"] doubleValue];
    double porFacturar = [[self.dicEstadoCredito objectForKey:@"porFacturar"] doubleValue];
    double saldoAdelantoValorizaciones = [[self.dicEstadoCredito objectForKey:@"saldoAdelantoValorizaciones"] doubleValue];
    double saldoPorEntregar = [[self.dicEstadoCredito objectForKey:@"saldoPorEntregar"] doubleValue];
    double saldoFacturaAdelantada = [[self.dicEstadoCredito objectForKey:@"saldoFacturaAdelantada"] doubleValue];
    double limiteCredito = [[self.dicEstadoCredito objectForKey:@"limiteCredito"] doubleValue];
    double facturado = vencida + porVencer;
    double deudaTotal = facturado + porFacturar;
    double saldoFavor = saldoAdelantoValorizaciones + saldoPorEntregar + saldoFacturaAdelantada;
    double saldoDisponible = limiteCredito + saldoFavor - deudaTotal;
    self.estadoLbl.text = (saldoDisponible < 0)?@"Sobregiro:":@"S. Disponible:";
    self.estadoLbl.textColor = (saldoDisponible < 0)?[UIColor colorWithRed:241.0/255.0 green:90.0/255.0 blue:76.0/255.0 alpha:1.0]:[UIColor colorWithRed:72.0/255.0 green:72.0/255.0 blue:72.0/255.0 alpha:1.0];
    self.lineaCreditoLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:limiteCredito]];
    self.deudaTotalLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:deudaTotal]];
    self.facturadoLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:facturado]];
    self.porFacturarLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:porFacturar]];
    self.saldoFavorLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:saldoFavor]];
    self.sAdelantadosLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:saldoAdelantoValorizaciones]];
    self.sEntregarLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:saldoPorEntregar]];
    self.sFacAdelantadaLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:saldoFacturaAdelantada]];
    self.totalLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:saldoDisponible]];
    self.totalLbl.textColor = (saldoDisponible < 0)?[UIColor colorWithRed:241.0/255.0 green:90.0/255.0 blue:76.0/255.0 alpha:1.0]:[UIColor colorWithRed:72.0/255.0 green:72.0/255.0 blue:72.0/255.0 alpha:1.0];
}
@end