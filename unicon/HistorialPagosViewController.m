//
//  HistorialPagosViewController.m
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "HistorialPagosViewController.h"
#import "HistorialPagoTableViewCell.h"
@implementation HistorialPagosViewController
-(void)viewDidLoad{
    [super viewDidLoad];    
    for(NSDictionary *dicHisPago in self.arrHistorialPagos){
        NSString *tipoMoneda = [dicHisPago objectForKey:@"tipoMoneda"];
        if([tipoMoneda isEqualToString:@"S"] || [tipoMoneda isEqualToString:@"A"]){
            esSoles = true;
        }
        if([tipoMoneda isEqualToString:@"D"] || [tipoMoneda isEqualToString:@"A"]){
            esDolares = true;
        }
    }
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrHistorialPagos.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistorialPagoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *dicHistorialPago = [self.arrHistorialPagos objectAtIndex:indexPath.row];
    cell.acuerdoLbl.text = [dicHistorialPago objectForKey:@"codigoAcuerdo"];
    cell.obraLbl.text = [dicHistorialPago objectForKey:@"descripcionObra"];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    if(esSoles){
        self.totalSoleslbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalSoles]];
        NSString *strImportePagoSoles = [dicHistorialPago objectForKey:@"importePagoSoles"];
        cell.pagoSolesLbl.text = (strImportePagoSoles.length == 0)?@"":[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strImportePagoSoles.doubleValue]];
    }else{
        [cell.pagoSolesLbl removeFromSuperview];
        if(self.headerPagoSoleslbl.superview) {
            [self.headerPagoSoleslbl removeFromSuperview];
            [self.totalSoleslbl removeFromSuperview];
        }
    }
    if(esDolares){
        self.totalDolareslbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalDolares]];
        NSString *strImportePagoDolares = [dicHistorialPago objectForKey:@"importePagoDolares"];
        cell.pagoDolaresLbl.text = (strImportePagoDolares.length == 0)?@"":[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strImportePagoDolares.doubleValue]];
    }else{
        [cell.pagoDolaresLbl removeFromSuperview];
        [cell.separadorSolesDolaresView removeFromSuperview];
        if(self.headerPagoDolareslbl.superview){
            [self.headerPagoDolareslbl removeFromSuperview];
            [self.totalDolareslbl removeFromSuperview];
        }
    }
    return cell;
}
#pragma mark - Auxiliar
-(NSNumber *)getTotalSoles{
    if(!totalSoles){
        double totalSolesTemp = 0.0;
        for(NSDictionary *dicHisPago in self.arrHistorialPagos){
            NSString *strImportePagoSoles = [dicHisPago objectForKey:@"importePagoSoles"];
            if (strImportePagoSoles.length > 0) {
                totalSolesTemp += strImportePagoSoles.doubleValue;
            }
        }
        totalSoles = [NSNumber numberWithDouble:totalSolesTemp];
    }
    return totalSoles;
}
-(NSNumber *)getTotalDolares{
    if(!totalDolares){
        double totalDolaresTemp = 0.0;
        for(NSDictionary *dicHisPago in self.arrHistorialPagos){
            NSString *strImportePagoDolares = [dicHisPago objectForKey:@"importePagoDolares"];
            if (strImportePagoDolares.length > 0) {
                totalDolaresTemp += strImportePagoDolares.doubleValue;
            }
        }
        totalDolares = [NSNumber numberWithDouble:totalDolaresTemp];
    }
    return totalDolares;
}
@end