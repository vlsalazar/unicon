//
//  DespachoHoyListaViewController.h
//  unicon
//
//  Created by victor salazar on 22/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface DespachoHoyListaViewController:UIViewController<NSURLConnectionDataDelegate,NSURLConnectionDelegate,UITableViewDataSource,UITableViewDelegate>{
    float originalHeightLeyendaView;
    NSDictionary *dicParams;
    NSURLConnection *currentCon;
    NSMutableData *currentData;
    NSArray *arrResult;
    UIRefreshControl *obraRefCtrl;
}
@property(nonatomic,weak) NSDictionary *dicPedido;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightLeyendaView;
@property(nonatomic,weak) IBOutlet UILabel *nomObraLbl;
@property(nonatomic,weak) IBOutlet UITableView *despachosObraHoyTableView;
@end