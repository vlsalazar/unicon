//
//  EntregaDetalleViewController.h
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EntregaDetalleViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicEntrega;
@property(nonatomic,weak) IBOutlet UILabel *tituloLbl;
@property(nonatomic,weak) IBOutlet UILabel *tipoLbl;
@property(nonatomic,weak) IBOutlet UILabel *disenioLbl;
@property(nonatomic,weak) IBOutlet UILabel *cantidadLbl;
@property(nonatomic,weak) IBOutlet UILabel *uMedidaLbl;
@property(nonatomic,weak) IBOutlet UILabel *pUnitarioLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalLbl;
@end