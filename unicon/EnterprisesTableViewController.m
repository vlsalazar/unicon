//
//  EnterprisesTableViewController.m
//  unicon
//
//  Created by DSB Mobile on 10/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EnterprisesTableViewController.h"
#import "FilterViewController.h"
@implementation EnterprisesTableViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrEnterprises = [[UsuarioActual currentUser] arrEmpresas];
    searching = false;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.searchTxtFld becomeFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.searchTxtFld resignFirstResponder];
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrEnterprises.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"enterpriseCell" forIndexPath:indexPath];
    Empresa *empTemp = [arrEnterprises objectAtIndex:indexPath.row];
    cell.textLabel.text = empTemp.descripcionCliente;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UsuarioActual *usuario = [UsuarioActual currentUser];
    Empresa *empTemp = [arrEnterprises objectAtIndex:indexPath.row];
    BOOL nuevaEmpresa = (usuario.empresaSeleccionada) ? (![usuario.empresaSeleccionada isEqual:empTemp]) : true;
    int ind = (int)[self.navigationController.viewControllers indexOfObject:self];
    if(nuevaEmpresa){
        [[UsuarioActual currentUser] setPidioObras:false];
        [[UsuarioActual currentUser] setPidioObrasCreditos:false];
        [[UsuarioActual currentUser] setArrObras:nil];
        [[UsuarioActual currentUser] setArrObrasCredito:nil];
        [[UsuarioActual currentUser] setEmpresaSeleccionada:[arrEnterprises objectAtIndex:indexPath.row]];
    }
    [self.navigationController popViewControllerAnimated:true];
    if(nuevaEmpresa){
        FilterViewController *filterViewCont = self.navigationController.viewControllers[ind - 1];
        [filterViewCont loadObras];
    }
}
#pragma - mark UITextField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *finalStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.searchButton.enabled = (finalStr.length > 3);
    return true;
}
#pragma - mark IBAction
-(IBAction)actionSearch:(id)sender{
    if(!searching){
        searching = true;
        [self.searchTxtFld resignFirstResponder];
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        NSDictionary *dicParams = @{@"message": self.searchTxtFld.text};
        currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlEmpresasUsuarioUnicon] andParams:dicParams];
        finalData = [NSMutableData data];
        [currentCon start];
    }
}
#pragma mark - NSURLConnection
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    searching = false;
    [SVProgressHUD dismiss];
    if([ToolBox isErrorConnectionInternet:error.code]){
        [ToolBox showAlertaErrorConnectionInViewCont:self];
    }else{
        if(error.code == NSURLErrorTimedOut){
            [ToolBox showAlertaTimeOutInViewCont:self];
        }else{
            [ToolBox showAlertaErrorInViewCont:self];
        }
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    finalData.length = 0;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [finalData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    searching = false;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    NSError *jsonError;
    id dicResult = [NSJSONSerialization JSONObjectWithData:finalData options:NSJSONReadingAllowFragments error:&jsonError];
    if(jsonError){
        [ToolBox showAlertaErrorInViewCont:self];
    }else{
        if([dicResult isEqual:[NSNull null]]){
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Atención" message:@"No hay datos disponibles" preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertCont animated:true completion:nil];
        }else{
            NSMutableArray *arrEnterprisesTemp = [NSMutableArray array];
            id arrList = [dicResult objectForKey:@"baseBean"];
            if ([arrList isKindOfClass:[NSDictionary class]]) {
                arrList = @[arrList];
            }
            for(NSDictionary *dicEnter in arrList){
                Empresa *empTemp = [Empresa new];
                empTemp.codigoCliente = [dicEnter objectForKey:@"codigo"];
                empTemp.descripcionCliente = [dicEnter objectForKey:@"descripcion"];
                [arrEnterprisesTemp addObject:empTemp];
            }
            arrEnterprises = arrEnterprisesTemp;
            [[UsuarioActual currentUser] setArrEmpresas:arrEnterprises];
            [self.tableView reloadData];
        }
    }
}
@end
