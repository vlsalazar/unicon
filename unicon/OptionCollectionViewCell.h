//
//  OptionCollectionViewCell.h
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface OptionCollectionViewCell:UICollectionViewCell
@property(nonatomic,weak) IBOutlet UIImageView *iconImgView;
@property(nonatomic,weak) IBOutlet UILabel *titleLbl;
@property(nonatomic,weak) IBOutlet UIView *rightView;
@property(nonatomic,weak) IBOutlet UIView *bottomView;
@end