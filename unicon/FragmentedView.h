//
//  FragmentedView.h
//  unicon
//
//  Created by DSB Mobile on 17/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#define colorFinVaciado [UIColor colorWithRed:9.0/255.0 green:146.0/255.0 blue:51.0/255.0 alpha:1]
#define colorIniVaciado [UIColor colorWithRed:0 green:39.0/255.0 blue:176.0/255.0 alpha:1]
#define colorEspObra [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1]
#define colorTransito [UIColor colorWithRed:1.0 green:135.0/255.0 blue:0.0 alpha:1]
#define colorProgramado [UIColor colorWithRed:213.0/255.0 green:209.0/255.0 blue:182.0/255.0 alpha:1]
@interface FragmentedView:UIView{
    NSMutableArray *arrViews;
}
@property(nonatomic) float porcFinVaciado;
@property(nonatomic) float porcIniVaciado;
@property(nonatomic) float porcEspObra;
@property(nonatomic) float porcTransito;
@property(nonatomic) float porcProgramado;
@end