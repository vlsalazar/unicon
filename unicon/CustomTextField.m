//
//  CustomTextField.m
//  unicon
//
//  Created by victor salazar on 1/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "CustomTextField.h"
@implementation CustomTextField
-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 8, 8);
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 8, 8);
}
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if(action == @selector(paste:)){
        return false;
    }
    return [super canPerformAction:action withSender:sender];
}
@end