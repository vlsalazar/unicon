//
//  Usuario+CoreDataProperties.m
//  unicon
//
//  Created by victor salazar on 3/21/17.
//  Copyright © 2017 Victor Salazar. All rights reserved.
//
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | INICIO
#import "Usuario+CoreDataProperties.h"
@implementation Usuario (CoreDataProperties)
+(NSFetchRequest<Usuario *> *)fetchRequest{
	return [[NSFetchRequest alloc] initWithEntityName:@"Usuario"];
}
@dynamic acuerdosHabilitado;
@dynamic certificadosHabilitado;
@dynamic codigoCliente;
@dynamic codigoUsuario;
@dynamic consultasHabilitado;
@dynamic correo;
@dynamic creditosHabilitado;
@dynamic descripcionCliente;
@dynamic estatusHabilitado;
@dynamic gestionHabilitado;
@dynamic nombre;
@dynamic usuario;
@end
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | FIN
