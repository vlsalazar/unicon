//
//  RepHisDetViewController.m
//  unicon
//
//  Created by victor salazar on 24/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "RepHisDetViewController.h"
@implementation RepHisDetViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    UIColor *colorBlue = [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1.0];
    id guia = [self.dicDespacho objectForKey:@"guia"];
    id orderCode = [self.dicDespacho objectForKey:@"orderCode"];
    id shortProdDescr = [self.dicDespacho objectForKey:@"shortProdDescr"];
    id delvQty = [self.dicDespacho objectForKey:@"delvQty"];
    id truckCode = [self.dicDespacho objectForKey:@"truckCode"];
    id usageDescr = [self.dicDespacho objectForKey:@"usageDescr"];
    id teo = [self.dicDespacho objectForKey:@"teo"];
    id schlNum = [self.dicDespacho objectForKey:@"schlNum"];
    id plantName = [self.dicDespacho objectForKey:@"plantName"];
    id toJobTime = [self.dicDespacho objectForKey:@"toJobTime"];
    id onJobTime = [self.dicDespacho objectForKey:@"onJobTime"];
    id beginUnldTime = [self.dicDespacho objectForKey:@"beginUnldTime"];
    id endUnldTime = [self.dicDespacho objectForKey:@"endUnldTime"];
    id deEtapa = [self.dicDespacho objectForKey:@"deEtapa"];
    
    NSMutableAttributedString *finalAttrString = [NSMutableAttributedString new];
    
    NSString *strNroPedido = orderCode?[orderCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrNroPedido = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Nro. Pedido: %@\n", strNroPedido]];
    [attrNroPedido setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 12)];
    [attrNroPedido setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(13, strNroPedido.length)];
    [finalAttrString appendAttributedString:attrNroPedido];
    
    NSString *strProgNro = schlNum?schlNum:@"";
    NSMutableAttributedString *attrProgNro = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Prog. Nro.: %@\n", strProgNro]];
    [attrProgNro setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 11)];
    [attrProgNro setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(12, strProgNro.length)];
    [finalAttrString appendAttributedString:attrProgNro];
    
    NSString *strGuia = guia?guia:@"";
    NSMutableAttributedString *attrGuia = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Guía: %@\n", strGuia]];
    [attrGuia setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 5)];
    [attrGuia setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(6, strGuia.length)];
    [finalAttrString appendAttributedString:attrGuia];
    
    NSString *strPlanta = plantName?[plantName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrPlanta = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Planta: %@\n", strPlanta]];
    [attrPlanta setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 7)];
    [attrPlanta setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(8, strPlanta.length)];
    [finalAttrString appendAttributedString:attrPlanta];
    
    NSString *strCamion = truckCode?[truckCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrCamion = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Camión: %@\n", strCamion]];
    [attrCamion setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 7)];
    [attrCamion setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(8, strCamion.length)];
    [finalAttrString appendAttributedString:attrCamion];
    
    NSString *strDiseno = shortProdDescr?[shortProdDescr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrDiseno = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Diseño: %@\n", strDiseno]];
    [attrDiseno setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 7)];
    [attrDiseno setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(8, strDiseno.length)];
    [finalAttrString appendAttributedString:attrDiseno];
    
    NSString *strVolumen = delvQty?delvQty:@"";
    NSMutableAttributedString *attrVolumen = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Volumen: %@\n", strVolumen]];
    [attrVolumen setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 8)];
    [attrVolumen setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(9, strVolumen.length)];
    [finalAttrString appendAttributedString:attrVolumen];
    
    NSString *strElemento = usageDescr?[usageDescr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrElemento = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Elemento a vaciar: %@\n", strElemento]];
    [attrElemento setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 18)];
    [attrElemento setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(19, strElemento.length)];
    [finalAttrString appendAttributedString:attrElemento];
    
    NSString *strSalObra = toJobTime?[NSString stringWithFormat:@"%@h", [ToolBox getHHMMForStringDate:toJobTime]]:@"";
    NSMutableAttributedString *attrSalObra = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Salida a obra: %@\n", strSalObra]];
    [attrSalObra setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 14)];
    [attrSalObra setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(15, strSalObra.length)];
    [finalAttrString appendAttributedString:attrSalObra];
    
    NSString *strLlegObra = onJobTime?[NSString stringWithFormat:@"%@h", [ToolBox getHHMMForStringDate:onJobTime]]:@"";
    NSMutableAttributedString *attrLlegObra = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Llegada a obra: %@\n", strLlegObra]];
    [attrLlegObra setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 15)];
    [attrLlegObra setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(16, strLlegObra.length)];
    [finalAttrString appendAttributedString:attrLlegObra];
    
    NSString *strIniVac = beginUnldTime?[NSString stringWithFormat:@"%@h", [ToolBox getHHMMForStringDate:beginUnldTime]]:@"";
    NSMutableAttributedString *attrIniVac = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Inicio de Vaciado: %@\n", strIniVac]];
    [attrIniVac setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 18)];
    [attrIniVac setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(19, strIniVac.length)];
    [finalAttrString appendAttributedString:attrIniVac];
    
    NSString *strFinVac = endUnldTime?[NSString stringWithFormat:@"%@h", [ToolBox getHHMMForStringDate:endUnldTime]]:@"";
    NSMutableAttributedString *attrFinVac = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Fin de Vaciado: %@\n", strFinVac]];
    [attrFinVac setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 15)];
    [attrFinVac setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(16, strFinVac.length)];
    [finalAttrString appendAttributedString:attrFinVac];
    
    NSString *strTeo = teo?teo:@"";
    NSMutableAttributedString *attrTeo = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"TEO: %@\n", strTeo]];
    [attrTeo setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 4)];
    [attrTeo setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(5, strTeo.length)];
    [finalAttrString appendAttributedString:attrTeo];
    
    NSString *strEstatus = deEtapa?[deEtapa stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    NSMutableAttributedString *attrEstatus = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Estatus: %@\n", strEstatus]];
    [attrEstatus setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(0, 8)];
    [attrEstatus setAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: colorBlue} range:NSMakeRange(9, strEstatus.length)];
    [finalAttrString appendAttributedString:attrEstatus];
    
    self.detTxtView.attributedText = finalAttrString;
}
@end