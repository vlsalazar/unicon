//
//  EntregaDetalleViewController.m
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EntregaDetalleViewController.h"
#define tipoEntregaProducto @"PRD"
#define tipoEntregaServicio @"SER"
#define tipoEntregaConcepto @"CNP"
@implementation EntregaDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSString *strTipoProducto = [self.dicEntrega objectForKey:@"tipoProducto"];
    NSString *strCantidad = [self.dicEntrega objectForKey:@"cantidadResumen"];
    NSString *strPUnitario = [self.dicEntrega objectForKey:@"precioUnitario"];
    NSString *strTotal = [self.dicEntrega objectForKey:@"totalResumen"];
    self.tituloLbl.text = [self.dicEntrega objectForKey:@"descripcionArticulo"];
    self.tipoLbl.text = [strTipoProducto isEqualToString:tipoEntregaProducto]?@"PRODUCTO":([strTipoProducto isEqualToString:tipoEntregaServicio]?@"SERVICIO":([strTipoProducto isEqualToString:tipoEntregaConcepto]?@"CONCEPTO":@""));
    self.disenioLbl.text = [[self.dicEntrega objectForKey:@"nombreDisenno"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.cantidadLbl.text = (strCantidad.length == 0)?@"":[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strCantidad.doubleValue]];
    self.uMedidaLbl.text = [self.dicEntrega objectForKey:@"unidadDeMedida"];
    self.pUnitarioLbl.text = (strPUnitario.length == 0)?@"":[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strPUnitario.doubleValue]];
    self.totalLbl.text = (strTotal.length == 0)?@"":[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strTotal.doubleValue]];
}
@end