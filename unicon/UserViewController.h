//
//  UsuarioViewController.h
//  unicon
//
//  Created by victor salazar on 4/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SideMenuViewController.h"
#import "MenuViewController.h"
@interface UserViewController:UIViewController{
    BOOL animating;
    SideMenuViewController *sideMenuViewCont;
    MenuViewController *menuViewCont;
}
@property(nonatomic,weak) IBOutlet UIView *sideMenuView;
-(void)showSideMenuWithOption:(int)option;
-(void)hideSideMenuWithOption:(NSString *)option;
-(void)showMainMenu;
@end