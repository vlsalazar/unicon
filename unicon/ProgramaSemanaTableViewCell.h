//
//  RowTwoEntriesTableViewCell.h
//  unicon
//
//  Created by victor salazar on 10/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ProgramaSemanaTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *nPedidoLbl;
@property(nonatomic,weak) IBOutlet UILabel *tPedidoLbl;
@property(nonatomic,weak) IBOutlet UILabel *disenioLbl;
@property(nonatomic,weak) IBOutlet UILabel *volumenLbl;
@property(nonatomic,weak) IBOutlet UILabel *estadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *horaLbl;
@end