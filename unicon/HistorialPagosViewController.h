//
//  HistorialPagosViewController.h
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface HistorialPagosViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    BOOL esSoles, esDolares;
    NSNumber *totalSoles, *totalDolares;
}
@property(nonatomic,weak) NSArray *arrHistorialPagos;
@property(nonatomic,weak) IBOutlet UILabel *headerPagoSoleslbl;
@property(nonatomic,weak) IBOutlet UILabel *headerPagoDolareslbl;
@property(nonatomic,weak) IBOutlet UILabel *totalSoleslbl;
@property(nonatomic,weak) IBOutlet UILabel *totalDolareslbl;
@end