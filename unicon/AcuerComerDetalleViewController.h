//
//  AcuerComerDetalleViewController.h
//  unicon
//
//  Created by victor salazar on 29/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface AcuerComerDetalleViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate>{
    NSMutableArray *arrDocs;
}
@property(nonatomic,weak) NSDictionary *dicAcuerComer;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UILabel *numAcuerLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaAcuerLbl;
@end