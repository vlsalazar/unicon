//
//  MenuViewController.m
//  unicon
//
//  Created by DSB Mobile on 1/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "MenuViewController.h"
#import "OptionCollectionViewCell.h"
#import "UserViewController.h"
@implementation MenuViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.enabled = true;
    UsuarioActual *userActual = [UsuarioActual currentUser];
    self.navigationItem.title = [userActual nombre];
    arrMenu = userActual.arrMenu;
    UIBarButtonItem *menuBarBtnItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"menuSI"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(actionMenu:)];
    self.navigationItem.leftBarButtonItem = menuBarBtnItem;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewDidRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self viewDidRotate:self];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [segue.destinationViewController setValue:selectedOption forKey:@"option"];
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
-(void)optionSelected:(NSString *)option{
    if(option.length > 0){
        selectedOption = option;
        [self performSegueWithIdentifier:@"showFilterViewCont" sender:self];
    }
}
-(void)viewDidRotate:(id)sender{
    [self.menuCollectionView reloadData];
}
-(void)actionMenu:(id)sender{
    UserViewController *usuViewCont = (UserViewController *)self.navigationController.parentViewController;
    [usuViewCont showSideMenuWithOption:-1];
}
#pragma mark - UICollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrMenu.count;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float cellHeight = ceil(self.menuCollectionView.frame.size.height/3.0);
    if(cellHeight < 180){
        cellHeight = 180;
    }
    return CGSizeMake(self.menuCollectionView.frame.size.width/2.0, cellHeight);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    OptionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"optionCell" forIndexPath:indexPath];
    cell.bottomView.hidden = false;
    if(indexPath.row >= 4){
        cell.bottomView.hidden = true;
    }
    cell.rightView.hidden = false;
    if(indexPath.row%2 == 1){
        cell.rightView.hidden = true;
    }
    NSDictionary *dicOption = arrMenu[indexPath.item];
    BOOL enabled = [dicOption[@"enabled"] boolValue];
    cell.iconImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_%@", [dicOption[@"image"] lowercaseString], enabled?@"hab":@"deshab"]];
    cell.titleLbl.text = dicOption[@"title"];
    cell.userInteractionEnabled = enabled;
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.item == 2){
        selectedOption = @"2";
        [self performSegueWithIdentifier:@"showFilterViewCont" sender:self];
    }else{
        UserViewController *usuViewCont = (UserViewController *)self.navigationController.parentViewController;
        [usuViewCont showSideMenuWithOption:(int)indexPath.item];
    }
}
@end
