//
//  ComprobantesViewController.h
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ComprobantesViewController:UIViewController<UITableViewDataSource, UITableViewDelegate, UIDocumentInteractionControllerDelegate>{
    NSMutableArray *arrItemsSelected;
}
@property(nonatomic,weak) NSArray *arrComprobantes;
@property(nonatomic,weak) IBOutlet UITableView *comprobantesTableView;
@end