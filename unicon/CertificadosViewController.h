//
//  CertificadosViewController.h
//  unicon
//
//  Created by victor salazar on 2/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface CertificadosViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate>{
    NSMutableArray *arrItemsSelected;
}
@property(nonatomic,weak) NSMutableArray *arrCertificados;
@property(nonatomic,weak) IBOutlet UITableView *certificadosTableView;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightLeyendaView;
@end