//
//  ViewController.m
//  unicon
//
//  Created by DSB Mobile on 27/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "LoginViewController.h"
#import "Obra.h"
#import "Usuario+CoreDataClass.h"
#define UsernameTextField 1
#define EnterprisesTextField 2
#define PasswordTextField 3
@implementation LoginViewController
#pragma mark - UITextField
-(void)viewDidLoad {
    [super viewDidLoad];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [self loadLoginViewIpad:(self.view.frame.size.width == 1024)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceChangedOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
    }else{
        self.containerEntPassView.layer.cornerRadius = 5.0f;
        self.searchBtn.layer.cornerRadius = 5.0f;
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    arrEnterprises = nil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        ipadView.usernameTxtFld.text = @"";
        ipadView.enterprisesTxtFld.text = @"";
        ipadView.passwordTxtFld.text = @"";
        [ipadView.searchBtn setHidden:false];
        ipadView.enterprisesTxtFld.hidden = false;
        ipadView.enterprisesTxtFld.enabled = false;
        [ipadView.usernameRightSpaceConst setPriority:250];
        [ipadView.entPassHeightCont setConstant:80];
        [ipadView.searchBtn setEnabled:false];
        [ipadView.passwordTxtFld setEnabled:false];
        [ipadView.loginBtn setBackgroundColor:[UIColor lightGrayColor]];
        [ipadView.loginBtn setEnabled:false];
    }else{
        self.usernameTxtFld.text = @"";
        self.enterprisesTxtFld.text = @"";
        self.passwordTxtFld.text = @"";
        [self.searchBtn setHidden:false];
        self.enterprisesTxtFld.enabled = false;
        self.enterprisesTxtFld.hidden = false;
        [self.usernameRightSpaceConst setPriority:250];
        [self.entPassHeightCont setConstant:60];
        [self.searchBtn setEnabled:false];
        [self.passwordTxtFld setEnabled:false];
        [self.loginBtn setBackgroundColor:[UIColor lightGrayColor]];
        [self.loginBtn setEnabled:false];
    }
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
#pragma mark - Action
-(IBAction)actionTap:(id)sender{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [ipadView.usernameTxtFld resignFirstResponder];
        [ipadView.enterprisesTxtFld resignFirstResponder];
        [ipadView.passwordTxtFld resignFirstResponder];
    }else{
        [self.usernameTxtFld resignFirstResponder];
        [self.enterprisesTxtFld resignFirstResponder];
        [self.passwordTxtFld resignFirstResponder];
    }
}
-(IBAction)actionChangeBaseURL:(UILongPressGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateBegan){
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"URL raiz" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"Ingrese el URL raiz aqui";
            textField.text = [[NSUserDefaults standardUserDefaults] objectForKey:baseURLKey];
            [textField addTarget:self action:@selector(alertControllerTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            UITextField *baseURLTxtFld = alertCont.textFields.firstObject;
            [[NSUserDefaults standardUserDefaults] setObject:baseURLTxtFld.text forKey:baseURLKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
        [alertCont addAction:okAction];
        [self presentViewController:alertCont animated:true completion:nil];
    }
}
-(void)alertControllerTextFieldDidChange:(UITextField *)sender{
    UIAlertController *alertCont = (UIAlertController *)self.presentedViewController;
    UIAlertAction *okAction = alertCont.actions.lastObject;
    okAction.enabled = (sender.text.length > 0);
}
-(IBAction)actionSearch:(id)sender{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [ipadView.usernameTxtFld resignFirstResponder];
    }else{
        [self.usernameTxtFld resignFirstResponder];
    }
    [self getEnterprises];
}
-(IBAction)actionLogin:(id)sender{
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    if(!loggingIn){
        loggingIn = true;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [ipadView.usernameTxtFld resignFirstResponder];
            [ipadView.enterprisesTxtFld resignFirstResponder];
            [ipadView.passwordTxtFld resignFirstResponder];
        }else{
            [self.usernameTxtFld resignFirstResponder];
            [self.enterprisesTxtFld resignFirstResponder];
            [self.passwordTxtFld resignFirstResponder];
        }
        NSString *username = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? ipadView.usernameTxtFld.text : self.usernameTxtFld.text;
        NSString *password = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? ipadView.passwordTxtFld.text : self.passwordTxtFld.text;
        BOOL isUniconUser = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? ipadView.searchBtn.hidden : self.searchBtn.hidden;
        if(!isUniconUser){
            username = [dicCurrentEnterprise objectForKey:@"codigoUsuario"];
        }
        NSDictionary *dicParams = @{@"usuario": username, @"clave": password};
        [ServiceConector connectToUrl:[ServiceUrl getUrlLogin] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            loggingIn = false;
            if(error){
                if([ToolBox isErrorConnectionInternet:error.code]){
                    [ToolBox showAlertaErrorConnectionInViewCont:self];
                }else{
                    if(error.code == NSURLErrorTimedOut){
                        [ToolBox showAlertaTimeOutInViewCont:self];
                    }else{
                        [ToolBox showAlertaErrorInViewCont:self];
                    }
                }
                [SVProgressHUD dismiss];
            }else{
                NSError *jsonError;
                NSDictionary *dicResult = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
                if(jsonError){
                    [ToolBox showAlertaErrorInViewCont:self];
                    [SVProgressHUD dismiss];
                }else{
                    NSString *strMensaje = [dicResult objectForKey:@"mensaje"];
                    if(strMensaje.length == 0){
                        dicUser = [dicResult objectForKey:@"usuario"];
                        UsuarioActual *currentUser = [UsuarioActual currentUser];
                        currentUser.correo = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? ipadView.usernameTxtFld.text : self.usernameTxtFld.text;
                        currentUser.nombre = dicUser[@"nombre"];
                        currentUser.usuario = dicUser[@"usuario"];
                        currentUser.codigoUsuario = dicUser[@"codigoUsuario"];
                        currentUser.codigoCliente = dicUser[@"codigoCliente"];
                        currentUser.descripcionCliente = dicUser[@"descripcionCliente"];
                        
                        NSMutableArray *arrMenu = [ToolBox getMenu];
                        NSArray *vistas = [dicUser objectForKey:@"vistas"];
                        NSArray *arrAcciones = nil;
                        for(NSDictionary *dicVista in vistas){
                            if([[dicVista objectForKey:@"objeto"] isEqualToString:@"menu.jspx"]){
                                arrAcciones = [dicVista objectForKey:@"acciones"];
                                break;
                            }
                        }
                        if(arrAcciones){
                            BOOL estatusEnabled = [self findItem:@"menuProgramacionDespacho" inArrAcciones:arrAcciones];
                            BOOL gestionEnabled = [self findItem:@"menuDesempenioObra" inArrAcciones:arrAcciones];
                            BOOL certificadoEnabled = [self findItem:@"menuCertificadosDeCalidad" inArrAcciones:arrAcciones];
                            BOOL creditosEnabled = [self findItem:@"menuFacturacionCreditosCobranza" inArrAcciones:arrAcciones];
                            BOOL acuerdosEnabled = [self findItem:@"menuDocumentosComerciales" inArrAcciones:arrAcciones];
                            BOOL consultasEnabled = [self findItem:@"menuEstatusReclamos" inArrAcciones:arrAcciones];
                            [[arrMenu objectAtIndex:0] setObject:[NSNumber numberWithBool:estatusEnabled] forKey:@"enabled"];
                            [[arrMenu objectAtIndex:1] setObject:[NSNumber numberWithBool:gestionEnabled] forKey:@"enabled"];
                            [[arrMenu objectAtIndex:2] setObject:[NSNumber numberWithBool:certificadoEnabled] forKey:@"enabled"];
                            [[arrMenu objectAtIndex:3] setObject:[NSNumber numberWithBool:acuerdosEnabled] forKey:@"enabled"];
                            [[arrMenu objectAtIndex:4] setObject:[NSNumber numberWithBool:creditosEnabled] forKey:@"enabled"];
                            [[arrMenu objectAtIndex:5] setObject:[NSNumber numberWithBool:consultasEnabled] forKey:@"enabled"];
                        }
                        currentUser.arrMenu = arrMenu;
                        // 20170321 | Victor Salazar | Inicio de sesión automatico - Se almacena el usuario en la BD y cuando el usuario es cliente ya no se carga las obras en esta clase. | INICIO
                        [Usuario guardarUsuarioActual:currentUser];
                        [SVProgressHUD dismiss];
                        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
                        [self performSegueWithIdentifier:@"showMenu" sender:self];
                        // 20170321 | Victor Salazar | Inicio de sesión automatico - Se almacena el usuario en la BD y cuando el usuario es cliente ya no se carga las obras en esta clase. | FIN
                    }else{
                        [SVProgressHUD dismiss];
                        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Alerta" message:strMensaje preferredStyle:UIAlertControllerStyleAlert];
                        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
                        [self presentViewController:alertCont animated:true completion:nil];
                    }
                }
            }
        }];
    }
}
#pragma mark - Auxiliar
-(void)getEnterprises{
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    NSString *username = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? ipadView.usernameTxtFld.text : self.usernameTxtFld.text;
    NSDictionary *dicParams = @{@"usuario": username, @"clave": @""};
    NSLog(@"url: %@", [ServiceUrl getUrlEmpresas]);
    [ServiceConector connectToUrl:[ServiceUrl getUrlEmpresas] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        NSLog(@"error: %@", error);
        [SVProgressHUD dismiss];
        loggingIn = false;
        if(error){
            if([ToolBox isErrorConnectionInternet:error.code]){
                [ToolBox showAlertaErrorConnectionInViewCont:self];
            }else{
                if(error.code == NSURLErrorTimedOut){
                    [ToolBox showAlertaTimeOutInViewCont:self];
                }else{
                    [ToolBox showAlertaErrorInViewCont:self];
                }
            }
        }else{
            NSError *jsonError;
            id dicResult = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                [ToolBox showAlertaErrorInViewCont:self];
            }else{
                if(dicResult != [NSNull null]){
                    id usuarioBean = [dicResult objectForKey:@"usuarioBean"];
                    if([usuarioBean isKindOfClass:[NSDictionary class]]){
                        usuarioBean = @[usuarioBean];
                    }
                    arrEnterprises = usuarioBean;
                    UIPickerView *enterprisesPickerView = [[UIPickerView alloc] init];
                    enterprisesPickerView.delegate = self;
                    dicCurrentEnterprise = arrEnterprises.firstObject;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                        ipadView.enterprisesTxtFld.enabled = true;
                        ipadView.passwordTxtFld.enabled = true;
                        ipadView.enterprisesTxtFld.inputView = enterprisesPickerView;
                        ipadView.enterprisesTxtFld.text = [dicCurrentEnterprise objectForKey:@"descripcionCliente"];
                    }else{
                        self.enterprisesTxtFld.enabled = true;
                        self.passwordTxtFld.enabled = true;
                        self.enterprisesTxtFld.inputView = enterprisesPickerView;
                        self.enterprisesTxtFld.text = [dicCurrentEnterprise objectForKey:@"descripcionCliente"];
                    }
                }else{
                    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Alerta" message:@"No existen usuarios registrados para esta cuenta." preferredStyle:UIAlertControllerStyleAlert];
                    [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:alertCont animated:true completion:nil];
                }
            }
        }
    }];
}
-(BOOL)findItem:(NSString *)item inArrAcciones:(NSArray *)arrAcciones{
    BOOL find = false;
    for(NSDictionary *dicAccion in arrAcciones){
        if([[dicAccion objectForKey:@"item"] isEqualToString:item]){
            find = true;
            break;
        }
    }
    return find;
}
#pragma mark - UITextField
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(textField.tag == UsernameTextField){
        if(arrEnterprises.count > 0){
            arrEnterprises = nil;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                ipadView.enterprisesTxtFld.text = @"";
                ipadView.enterprisesTxtFld.enabled = false;
                ipadView.passwordTxtFld.text = @"";
                ipadView.passwordTxtFld.enabled = false;
                ipadView.loginBtn.enabled = false;
            }else{
                self.enterprisesTxtFld.text = @"";
                self.enterprisesTxtFld.enabled = false;
                self.passwordTxtFld.text = @"";
                self.passwordTxtFld.enabled = false;
                self.loginBtn.enabled = false;
            }
        }
        NSString *strUniconRegex = @"[A-Z0-9a-z._%+-]+@unicon.com.pe";
        NSPredicate *uniconPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strUniconRegex];
        if([uniconPred evaluateWithObject:newString]){
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [ipadView.searchBtn setHidden:true];
                ipadView.enterprisesTxtFld.hidden = true;
                [ipadView.usernameRightSpaceConst setPriority:750];
                [ipadView.entPassHeightCont setConstant:40];
                [ipadView.passwordTxtFld setEnabled:true];
            }else{
                [self.searchBtn setHidden:true];
                self.enterprisesTxtFld.hidden = true;
                [self.usernameRightSpaceConst setPriority:750];
                [self.entPassHeightCont setConstant:30];
                [self.passwordTxtFld setEnabled:true];
            }
        }else{
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [ipadView.searchBtn setHidden:false];
                ipadView.enterprisesTxtFld.hidden = false;
                [ipadView.usernameRightSpaceConst setPriority:250];
                [ipadView.entPassHeightCont setConstant:80];
                NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.+[A-Za-z]{2,}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                [ipadView.searchBtn setEnabled:[emailTest evaluateWithObject:newString]];
                [ipadView.passwordTxtFld setEnabled:false];
            }else{
                [self.searchBtn setHidden:false];
                self.enterprisesTxtFld.hidden = false;
                [self.usernameRightSpaceConst setPriority:250];
                [self.entPassHeightCont setConstant:60];
                NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.+[A-Za-z]{2,}";
                NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                [self.searchBtn setEnabled:[emailTest evaluateWithObject:newString]];
                [self.passwordTxtFld setEnabled:false];
            }
        }
    }else if(textField.tag == PasswordTextField){
        if(newString.length == 0){
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [ipadView.loginBtn setBackgroundColor:[UIColor lightGrayColor]];
                [ipadView.loginBtn setEnabled:false];
            }else{
                [self.loginBtn setBackgroundColor:[UIColor lightGrayColor]];
                [self.loginBtn setEnabled:false];
            }
        }else{
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [ipadView.loginBtn setBackgroundColor:[UIColor colorWithRed:29.0/255.0 green:26.0/255.0 blue:59.0/255.0 alpha:1.0]];
                [ipadView.loginBtn setEnabled:true];
            }else{
                [self.loginBtn setBackgroundColor:[UIColor colorWithRed:29.0/255.0 green:26.0/255.0 blue:59.0/255.0 alpha:1.0]];
                [self.loginBtn setEnabled:true];
            }
        }
    }
    return true;
}
#pragma mark - Rotation
-(void)deviceChangedOrientation{
    [self loadLoginViewIpad:(self.view.frame.size.width == 1024)];
}
-(void)loadLoginViewIpad:(BOOL)isLandscape{
    NSString *currentUsername = @"";
    NSString *currentEnterprise = @"";
    NSString *currentPassword = @"";
    BOOL searchBtnEnabled = false;
    BOOL loginBtnEnabled = false;
    BOOL usernameIsFirstResponder = false;
    if(ipadView){
        BOOL isCurrentLandscape = (ipadView.frame.size.width == 1024);
        if(isCurrentLandscape == !isLandscape){
            return;
        }
        currentUsername = ipadView.usernameTxtFld.text;
        currentEnterprise = ipadView.enterprisesTxtFld.text;
        currentPassword = ipadView.passwordTxtFld.text;
        searchBtnEnabled = ipadView.searchBtn.enabled;
        loginBtnEnabled = ipadView.loginBtn.enabled;
        usernameIsFirstResponder = ipadView.usernameTxtFld.isFirstResponder;
        [ipadView removeFromSuperview];
    }
    ipadView = [[[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"LoginViewIPad%@", isLandscape ? @"L" : @"P"] owner:self options:0] firstObject];
    UITapGestureRecognizer *tapGesture = [[ipadView gestureRecognizers] firstObject];
    [tapGesture addTarget:self action:@selector(actionTap:)];
    UILongPressGestureRecognizer *longGesture = [[ipadView gestureRecognizers] lastObject];
    [longGesture addTarget:self action:@selector(actionChangeBaseURL:)];
    ipadView.frame = isLandscape?CGRectMake(0, 0, 1024, 768):CGRectMake(0, 0, 768, 1024);
    ipadView.containerEntPassView.layer.cornerRadius = 5.0f;
    ipadView.containerUsernameView.layer.cornerRadius = 5.0f;
    ipadView.searchBtn.layer.cornerRadius = 5.0f;
    ipadView.searchBtn.enabled = searchBtnEnabled;
    ipadView.usernameTxtFld.delegate = self;
    ipadView.usernameTxtFld.text = currentUsername;
    ipadView.passwordTxtFld.delegate = self;
    ipadView.passwordTxtFld.text = currentPassword;
    ipadView.enterprisesTxtFld.delegate = self;
    ipadView.enterprisesTxtFld.text = currentEnterprise;
    ipadView.loginBtn.enabled = loginBtnEnabled;
    if(usernameIsFirstResponder){
        [ipadView.usernameTxtFld becomeFirstResponder];
    }
    [ipadView.searchBtn addTarget:self action:@selector(actionSearch:) forControlEvents:UIControlEventTouchUpInside];
    [ipadView.loginBtn addTarget:self action:@selector(actionLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ipadView];
}
#pragma mark - UIPickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrEnterprises.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[arrEnterprises objectAtIndex:row] objectForKey:@"descripcionCliente"];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    dicCurrentEnterprise = [arrEnterprises objectAtIndex:row];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        ipadView.enterprisesTxtFld.text = [dicCurrentEnterprise objectForKey:@"descripcionCliente"];;
    }else{
        self.enterprisesTxtFld.text = [dicCurrentEnterprise objectForKey:@"descripcionCliente"];
    }
}
@end
