//
//  DepachoHoyViewController.m
//  unicon
//
//  Created by victor salazar on 14/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "DespachoHoyViewController.h"
#import "DespachoHoyPedidoTableViewCell.h"
@implementation DespachoHoyViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    originalHeightLeyendaView = self.heightLeyendaView.constant;
    self.titleLbl.text = [self.dicObra objectForKey:@"descObr"];
    id pedidos = [self.dicObra objectForKey:@"pedidos"];
    if([pedidos isKindOfClass:[NSDictionary class]]){
        pedidos = @[pedidos];
    }
    arrPedidos = pedidos;
    self.addressLbl.text = [arrPedidos.firstObject objectForKey:@"delvAddr"];
    obraRefCtrl = [[UIRefreshControl alloc] init];
    [obraRefCtrl addTarget:self action:@selector(actionRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.pedidosTableView addSubview:obraRefCtrl];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.pedidosTableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceChangedOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self performSelector:@selector(startRefreshing) withObject:nil afterDelay:20];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [currentCon cancel];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"mostrarMapa"]){
        [segue.destinationViewController setValue:[arrPedidos objectAtIndex:[sender tag]] forKey:@"dicPedido"];
    }else if([segue.identifier isEqualToString:@"mostrarLista"]){
        [segue.destinationViewController setValue:[arrPedidos objectAtIndex:[sender tag]] forKey:@"dicPedido"];
    }
}
#pragma mark - TableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(self.view.frame.size.width < self.view.frame.size.height){
            return 274.0;
        }else{
            return 232.0;
        }
    }else{
        if(self.view.frame.size.width < self.view.frame.size.height){
            return 218.0;
        }else{
            return 139.0;
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrPedidos.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DespachoHoyPedidoTableViewCell *cell;
    if((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) && (self.view.frame.size.width > self.view.frame.size.height)){
        cell = [tableView dequeueReusableCellWithIdentifier:@"pedidoEspCell" forIndexPath:indexPath];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"pedidoCell" forIndexPath:indexPath];
    }
    UIColor *blueColor = [UIColor colorWithRed:29.0/255.0 green:26.0/255.0 blue:59.0/255.0 alpha:1.0];
    NSDictionary *dicPedido = [arrPedidos objectAtIndex:indexPath.row];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormat.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    NSDate *dOrderDate = [dateFormat dateFromString:[dicPedido objectForKey:@"orderDate"]];
    NSDate *dStartTime = [dateFormat dateFromString:[dicPedido objectForKey:@"startTime"]];
    NSMutableAttributedString *attrStrPedido = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Pedido: %@", [dicPedido objectForKey:@"orderCode"]]];
    [attrStrPedido setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12], NSForegroundColorAttributeName: blueColor} range:NSMakeRange(0, 7)];
    cell.pedidoLbl.attributedText = attrStrPedido;
    dateFormat.dateFormat = @"dd/MM/yyy";
    cell.fechaLbl.text = [dateFormat stringFromDate:dOrderDate];
    dateFormat.dateFormat = @"HH:mm";
    NSMutableAttributedString *attrStrHoraInicio = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Hora Inicio: %@", [dateFormat stringFromDate:dStartTime]]];
    [attrStrHoraInicio setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12], NSForegroundColorAttributeName: blueColor} range:NSMakeRange(0, 12)];
    cell.horaInicioLbl.attributedText = attrStrHoraInicio;
    NSMutableAttributedString *attrStrFrecVac = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Frecuencia de Vaciado: %@", [dicPedido objectForKey:@"truckSpacingMins"]]];
    [attrStrFrecVac setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12], NSForegroundColorAttributeName: blueColor} range:NSMakeRange(0, 22)];
    cell.frecVacLbl.attributedText = attrStrFrecVac;
    NSMutableAttributedString *attrStrTipo = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Tipo: %@", [dicPedido objectForKey:@"deTiPed"]]];
    [attrStrTipo setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12], NSForegroundColorAttributeName: blueColor} range:NSMakeRange(0, 5)];
    cell.tipoLbl.attributedText = attrStrTipo;
    NSString *strOrderQty = [dicPedido objectForKey:@"orderQty"];
    NSMutableAttributedString *attrStrProgramado = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Programado: %@ m3", strOrderQty]];
    [attrStrProgramado setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12]} range:NSMakeRange(0, 11)];
    [attrStrProgramado setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:8], NSForegroundColorAttributeName: blueColor, NSBaselineOffsetAttributeName: @3} range:NSMakeRange(14 + strOrderQty.length, 1)];
    cell.programadoLbl.attributedText = attrStrProgramado;
    NSString *strDelvQty = [dicPedido objectForKey:@"delvQty"];
    NSMutableAttributedString *attrStrAvance = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Avance: %@ m3", strDelvQty]];
    [attrStrAvance setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12]} range:NSMakeRange(0, 7)];
    [attrStrAvance setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:8], NSForegroundColorAttributeName: blueColor, NSBaselineOffsetAttributeName: @3} range:NSMakeRange(10 + strDelvQty.length, 1)];
    cell.avanceLbl.attributedText = attrStrAvance;
    cell.codigoOrdenLbl.text = [dicPedido objectForKey:@"shortescr"];
    cell.progOrdenView.progress = [[dicPedido objectForKey:@"delvProg"] floatValue];
    [cell.progOrdenView setNeedsDisplay];
    NSMutableAttributedString *attrStrEstado = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Estado: %@", [dicPedido objectForKey:@"deEsOrdr"]]];
    [attrStrEstado setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12], NSForegroundColorAttributeName: blueColor} range:NSMakeRange(0, 7)];
    cell.estadoLbl.attributedText = attrStrEstado;
    int ctTktNone = [[dicPedido objectForKey:@"ctTktNone"] intValue];
    int ctTkt = [[dicPedido objectForKey:@"ctTkt"] intValue];
    cell.tiquetesLbl.text = [NSString stringWithFormat:@"%i Tiquete(s) Programado(s) y %i Tiquete(s) Despachado(s)", ctTktNone, ctTkt];
    int wProgamado = [[dicPedido objectForKey:@"et1With"] intValue];
    int wTransito = [[dicPedido objectForKey:@"et2With"] intValue];
    int wEsperandoObra = [[dicPedido objectForKey:@"et3With"] intValue];
    int wInicioVaciado = [[dicPedido objectForKey:@"et4With"] intValue];
    int wFinVaciado = [[dicPedido objectForKey:@"et5With"] intValue];
    int total = wProgamado + wTransito + wEsperandoObra + wInicioVaciado + wFinVaciado;
    cell.porcEstFrgView.porcFinVaciado = (float)wFinVaciado/total;
    cell.porcEstFrgView.porcIniVaciado = (float)wInicioVaciado/total;
    cell.porcEstFrgView.porcEspObra = (float)wEsperandoObra/total;
    cell.porcEstFrgView.porcTransito = (float)wTransito/total;
    cell.porcEstFrgView.porcProgramado = (float)wProgamado/total;
    [cell.porcEstFrgView setNeedsDisplay];
    if((ctTkt + ctTktNone) == 0){
        [cell.listaBtn removeFromSuperview];
    }
    cell.mapaBtn.tag = indexPath.row;
    cell.listaBtn.tag = indexPath.row;
    return cell;
}
#pragma mark - IBAction
-(IBAction)actionMapa:(id)sender{
    [self performSegueWithIdentifier:@"mostrarMapa" sender:sender];
}
-(IBAction)actionLista:(id)sender{
    [self performSegueWithIdentifier:@"mostrarLista" sender:sender];
}
-(IBAction)actionLeyenda:(id)sender{
    float newHeight;
    if(self.heightLeyendaView.constant == originalHeightLeyendaView){
        newHeight = 0;
    }else{
        newHeight = originalHeightLeyendaView;
    }
    self.heightLeyendaView.constant = newHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)actionRefresh:(UIRefreshControl *)refCtrl{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
    currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlProgramaSemana] andParams:[[UsuarioActual currentUser] dicBusqDespHoy]];
    currentData = [NSMutableData data];
    [currentCon start];
}
#pragma mark - NSURLConnection
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [obraRefCtrl endRefreshing];
    if([ToolBox isErrorConnectionInternet:error.code]){
        [ToolBox showAlertaErrorConnectionInViewCont:self];
    }else{
        if(error.code == NSURLErrorTimedOut){
            [ToolBox showAlertaTimeOutInViewCont:self];
        }else{
            [ToolBox showAlertaErrorInViewCont:self];
        }
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [currentData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [currentData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [obraRefCtrl endRefreshing];
    NSError *jsonError;
    id dicResultTemp = [NSJSONSerialization JSONObjectWithData:currentData options:NSJSONReadingAllowFragments error:&jsonError];
    currentData = nil;
    if(jsonError){
        [ToolBox showAlertaErrorInViewCont:self];
    }else{
        id obrasEnDespacho = [dicResultTemp objectForKey:@"obraEnDespachoBean"];
        if([obrasEnDespacho isKindOfClass:[NSDictionary class]]){
            obrasEnDespacho = @[obrasEnDespacho];
        }
        for(NSDictionary *dicObraTemp in obrasEnDespacho){
            if([dicObraTemp[@"descObr"] isEqualToString:self.dicObra[@"descObr"]]){
                self.dicObra = dicObraTemp;
                id pedidos = [self.dicObra objectForKey:@"pedidos"];
                if([pedidos isKindOfClass:[NSDictionary class]]){
                    pedidos = @[pedidos];
                }
                arrPedidos = pedidos;
                [self.pedidosTableView reloadData];
            }
        }
    }
    [self performSelector:@selector(startRefreshing) withObject:nil afterDelay:20];
}
#pragma mark - DeviceOrientation
-(void)deviceChangedOrientation{
    [self.pedidosTableView reloadData];
}
#pragma mark - Auxiliar
-(void)startRefreshing{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
    [obraRefCtrl beginRefreshing];
    [self actionRefresh:obraRefCtrl];
    [self.pedidosTableView setContentOffset:CGPointMake(0, -obraRefCtrl.frame.size.height) animated:true];
}
@end
