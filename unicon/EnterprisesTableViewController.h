//
//  EnterprisesTableViewController.h
//  unicon
//
//  Created by DSB Mobile on 10/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EnterprisesTableViewController:UITableViewController<UITextFieldDelegate, NSURLConnectionDataDelegate, NSURLConnectionDelegate>{
    NSArray *arrEnterprises;
    BOOL searching;
    NSURLConnection *currentCon;
    NSMutableData *finalData;
}
@property(nonatomic,weak) IBOutlet UITextField *searchTxtFld;
@property(nonatomic,weak) IBOutlet UIBarButtonItem *searchButton;
@end