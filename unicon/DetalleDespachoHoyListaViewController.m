//
//  DetalleDespachoHoyListaViewController.m
//  unicon
//
//  Created by victor salazar on 23/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "DetalleDespachoHoyListaViewController.h"
@implementation DetalleDespachoHoyListaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    id plantCode = [self.dicDespacho objectForKey:@"plantCode"];
    id guia = [self.dicDespacho objectForKey:@"guia"];
    id plantName = [self.dicDespacho objectForKey:@"plantName"];
    id truckCode = [self.dicDespacho objectForKey:@"truckCode"];
    id toJobTime = [self.dicDespacho objectForKey:@"toJobTime"];
    id onJobTime = [self.dicDespacho objectForKey:@"onJobTime"];
    id beginUnldTime = [self.dicDespacho objectForKey:@"beginUnldTime"];
    id endUnldTime = [self.dicDespacho objectForKey:@"endUnldTime"];
    id deEtapa = [self.dicDespacho objectForKey:@"deEtapa"];
    NSString *plantZ = @"";
    if(plantCode){
        plantCode = [plantCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(![plantCode isEqualToString:@"ZZL"]){
            plantZ = [plantCode substringToIndex:1];
        }
    }
    BOOL isPlantZ = false;
    if([plantZ isEqualToString:@"Z"]){
        isPlantZ = true;
    }
    
    NSMutableAttributedString *finalAtrString = [NSMutableAttributedString new];
    
    NSMutableParagraphStyle *paraStyle = [NSMutableParagraphStyle new];
    paraStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *titleAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", self.strTitle] attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSParagraphStyleAttributeName:paraStyle, NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0]}];
    [finalAtrString appendAttributedString:titleAtrString];
    
    NSAttributedString *guiaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Guía: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:guiaTitleAtrString];
    NSAttributedString *guiaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", guia?guia:@""] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:guiaValueAtrString];
    
    NSAttributedString *plantaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Planta: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:plantaTitleAtrString];
    NSAttributedString *plantaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", plantName?[plantName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@""] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:plantaValueAtrString];
    
    NSAttributedString *camionTitleAtrString = [[NSAttributedString alloc] initWithString:@"Camión: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:camionTitleAtrString];
    NSAttributedString *camionValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", truckCode?[truckCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@""] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:camionValueAtrString];
    
    NSAttributedString *disenioTitleAtrString = [[NSAttributedString alloc] initWithString:@"Diseño: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:disenioTitleAtrString];
    NSAttributedString *disenioValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [[self.dicDespacho objectForKey:@"shortProdDescr"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:disenioValueAtrString];
    
    NSAttributedString *volumenTitleAtrString = [[NSAttributedString alloc] initWithString:@"Volumen: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:volumenTitleAtrString];
    NSString *delvQtyStr = [self.dicDespacho objectForKey:@"delvQty"];
    NSMutableAttributedString *volumenValueAtrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@m3\n", delvQtyStr] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [volumenValueAtrString addAttributes:@{NSBaselineOffsetAttributeName: @3} range:NSMakeRange(delvQtyStr.length + 1, 1)];
    [finalAtrString appendAttributedString:volumenValueAtrString];
    
    NSAttributedString *elementoTitleAtrString = [[NSAttributedString alloc] initWithString:@"Elemento a vaciar: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:elementoTitleAtrString];
    NSAttributedString *elementoValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [[self.dicDespacho objectForKey:@"usageDescr"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:elementoValueAtrString];
    
    NSAttributedString *salidaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Salida a obra: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:salidaTitleAtrString];
    NSString *strToJobTime = @"";
    if(toJobTime && !isPlantZ){
        strToJobTime = [NSString stringWithFormat:@"%@h", ([toJobTime length] == 25)?[ToolBox getHHMMForStringDate:toJobTime]:[ToolBox getHHMMForStringDate2:toJobTime]];
    }
    NSAttributedString *salidaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strToJobTime] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:salidaValueAtrString];
    
    NSAttributedString *llegadaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Llegada a obra: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:llegadaTitleAtrString];
    NSString *strOnJobTime = @"";
    if(onJobTime && !isPlantZ){
        strOnJobTime = [NSString stringWithFormat:@"%@h", ([onJobTime length] == 25)?[ToolBox getHHMMForStringDate:onJobTime]:[ToolBox getHHMMForStringDate2:onJobTime]];
    }
    NSAttributedString *llegadaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strOnJobTime] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:llegadaValueAtrString];
    
    NSAttributedString *iniVacTitleAtrString = [[NSAttributedString alloc] initWithString:@"Inicio de Vaciado: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:iniVacTitleAtrString];
    NSString *strBeginUnldTime = @"";
    if(beginUnldTime && !isPlantZ){
        strBeginUnldTime = [NSString stringWithFormat:@"%@h", ([beginUnldTime length] == 25)?[ToolBox getHHMMForStringDate:beginUnldTime]:[ToolBox getHHMMForStringDate2:beginUnldTime]];
    }
    NSAttributedString *iniVacValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strBeginUnldTime] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:iniVacValueAtrString];
    
    NSAttributedString *finVacTitleAtrString = [[NSAttributedString alloc] initWithString:@"Fin de Vaciado: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:finVacTitleAtrString];
    NSString *strEndUnldTime = @"";
    if(endUnldTime && !isPlantZ){
        strEndUnldTime = [NSString stringWithFormat:@"%@h", ([endUnldTime length] == 25)?[ToolBox getHHMMForStringDate:endUnldTime]:[ToolBox getHHMMForStringDate2:endUnldTime]];
    }
    NSAttributedString *finVacValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strEndUnldTime] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:finVacValueAtrString];
    
    NSAttributedString *teoTitleAtrString = [[NSAttributedString alloc] initWithString:@"TEO: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:teoTitleAtrString];
    double teo = [[self.dicDespacho objectForKey:@"teo"] doubleValue];
    NSString *strTeo = @"";
    if((teo != 0 && !isPlantZ)){
        strTeo = [NSString stringWithFormat:@"%.0f min", teo];
    }
    NSAttributedString *teoValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strTeo] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:teoValueAtrString];
    
    NSAttributedString *estatusTitleAtrString = [[NSAttributedString alloc] initWithString:@"Estatus: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:estatusTitleAtrString];
    NSString *strEstatus = @"";
    if(deEtapa && !isPlantZ){
        strEstatus = [deEtapa stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    NSAttributedString *estatusValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", strEstatus] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:estatusValueAtrString];
    
    self.detailTxtView.attributedText = finalAtrString;
}
@end