//
//  CertificadosViewController.m
//  unicon
//
//  Created by victor salazar on 2/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "CertificadosViewController.h"
#import "CertificadoTableViewCell.h"
@implementation CertificadosViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrItemsSelected = [NSMutableArray array];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem *menuBarBtnItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"info"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(actionLeyenda:)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.parentViewController.navigationItem.rightBarButtonItem = menuBarBtnItem;
    }else{
        self.navigationItem.rightBarButtonItem = menuBarBtnItem;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceChangedOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.certificadosTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:[self.arrCertificados objectAtIndex:indexPath.row] forKey:@"dicCertificado"];
}
#pragma mark - IBAction
-(void)actionLeyenda:(id)sender{
    float newHeight;
    if(self.heightLeyendaView.constant == 0){
        newHeight = (self.navigationController.view.frame.size.width > self.navigationController.view.frame.size.height)?40:65;
    }else{
        newHeight = 0;
    }
    self.heightLeyendaView.constant = newHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(IBAction)actionSwitch:(UISwitch *)sender{
    if(sender.on){
        if(arrItemsSelected.count == 0){
            [arrItemsSelected addObject:[NSNumber numberWithInteger:sender.tag]];
        }else{
            NSDictionary *dicCurrentCert = [self.arrCertificados objectAtIndex:sender.tag];
            int currentStatus = [[dicCurrentCert objectForKey:@"status"] intValue];
            NSDictionary *dicCertTemp = [self.arrCertificados objectAtIndex:[[arrItemsSelected firstObject] intValue]];
            int statusTemp = [[dicCertTemp objectForKey:@"status"] intValue];
            if(((currentStatus == 1) && (statusTemp == 1)) || ((currentStatus != 1) && (statusTemp != 1))){
                [arrItemsSelected addObject:[NSNumber numberWithInteger:sender.tag]];
            }
        }
    }else{
        for(NSNumber *index in arrItemsSelected){
            if(index.intValue == sender.tag){
                [arrItemsSelected removeObject:index];
                break;
            }
        }
    }
}
-(IBAction)actionExport:(id)sender{
    if(arrItemsSelected.count == 0){
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Atención" message:@"Debe seleccionar una guía para poder exportar" preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertCont animated:true completion:nil];
    }else{
        NSMutableArray *arrGuiasRemision = [NSMutableArray array];
        BOOL esComunicado = false;
        for(NSNumber *index in arrItemsSelected){
            NSDictionary *dicCert = [self.arrCertificados objectAtIndex:index.intValue];
            int status = [[dicCert objectForKey:@"status"] intValue];
            esComunicado = (status == 1);
            [arrGuiasRemision addObject:@{@"numeroGuia": [dicCert objectForKey:@"numeroGuia"], @"serieGuia": [dicCert objectForKey:@"serieGuia"], @"motivoPerdida": [dicCert objectForKey:@"motivoPerdida"]}];
        }
        NSDictionary *dicParams = @{@"guias": arrGuiasRemision, @"usuario": [[UsuarioActual currentUser] codigoUsuario]};
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        [ServiceConector connectToUrl:(esComunicado?[ServiceUrl getUrlCertificadosPDFComunicado]:[ServiceUrl getUrlCertificadosPDFCertificado]) withParameters:dicParams withResponse:^(id receivedData, NSError *error){
            [SVProgressHUD dismiss];
            if(error){
                if([ToolBox isErrorConnectionInternet:error.code]){
                    [ToolBox showAlertaErrorConnectionInViewCont:self];
                }else{
                    if(error.code == NSURLErrorTimedOut){
                        [ToolBox showAlertaTimeOutInViewCont:self];
                    }else{
                        [ToolBox showAlertaErrorInViewCont:self];
                    }
                }
            }else{
                NSData *currentData = receivedData;
                if(currentData.length == 0){
                    [ToolBox showNoDataInViewCont:self];
                }else{
                    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *strDocumentDirectory = arr.firstObject;
                    NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                    [currentData writeToFile:filePath atomically:true];
                    UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                    docIntCont.name = @"";
                    docIntCont.delegate = self;
                    [docIntCont presentPreviewAnimated:true];
                }
            }
        }];
    }
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrCertificados.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary *dicCert = [self.arrCertificados objectAtIndex:indexPath.row];
    id diasFaltantes = [dicCert objectForKey:@"diasFaltantes"];
    int intDFaltantes = [diasFaltantes intValue];
    NSString *strEstadoEnsayoCompresion = [[dicCert objectForKey:@"estadoEnsayoCompresion"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strMotivoPerdida = [[dicCert objectForKey:@"motivoPerdida"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CertificadoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    cell.nGuiaLbl.text = [NSString stringWithFormat:@"%@-%@", [dicCert objectForKey:@"serieGuia"], [dicCert objectForKey:@"numeroGuia"]];
    cell.obraLbl.text = [dicCert objectForKey:@"descripcionObra"];
    BOOL esDiasFaltantes = (intDFaltantes > 0) && ([strEstadoEnsayoCompresion isEqualToString:@"PEN"]) && !([strMotivoPerdida isEqualToString:@"PER"]) && !([strMotivoPerdida isEqualToString:@"FAL"]);
    cell.dFaltantesLbl.text = esDiasFaltantes?[NSString stringWithFormat:@"Falta%@ %i día%@", (intDFaltantes > 1)?@"n":@"", intDFaltantes, (intDFaltantes > 1)?@"s":@""]:@"";
    int status = [self getStatusForDicCert:dicCert];
    [dicCert setObject:[NSNumber numberWithInt:status] forKey:@"status"];
    if(status == 1){
        cell.barView.backgroundColor = AzulColor;
    }else if(status == 2){
        cell.barView.backgroundColor = NaranjaCertColor;
    }else if(status == 3){
        cell.barView.backgroundColor = VerdeCertColor;
    }else{
        cell.barView.backgroundColor = [UIColor clearColor];
    }
    cell.selSwitch.hidden = !((![strEstadoEnsayoCompresion isEqualToString:@"ANU"] && ![strEstadoEnsayoCompresion isEqualToString:@"PAU"] && ![strEstadoEnsayoCompresion isEqualToString:@"REV"] && ![strEstadoEnsayoCompresion isEqualToString:@"RCH"]) || [strMotivoPerdida isEqualToString:@"PER"] || [strMotivoPerdida isEqualToString:@"FAL"]);
    BOOL bFgMuestreo = [[dicCert objectForKey:@"fgMuestreo"] isEqualToString:@"true"];
    BOOL bFgPerdida = [[dicCert objectForKey:@"fgPerdida"] isEqualToString:@"true"];
    NSString *strPromResis = [dicCert objectForKey:@"promedioResistencias"];
    double promedioResistencias = [strPromResis doubleValue];
    cell.selSwitch.enabled = !(!bFgMuestreo || ((promedioResistencias == 0) && ![strMotivoPerdida isEqualToString:@"PER"] && ![strMotivoPerdida isEqualToString:@"FAL"]));
    cell.selSwitch.on = false;
    cell.selSwitch.tag = indexPath.row;
    for(NSNumber *index in arrItemsSelected){
        if(index.intValue == cell.selSwitch.tag){
            cell.selSwitch.on = true;
        }
    }
    BOOL mostrarResultados = (![strEstadoEnsayoCompresion isEqualToString:@"ANU"] && ![strEstadoEnsayoCompresion isEqualToString:@"PAU"] && ![strEstadoEnsayoCompresion isEqualToString:@"REV"] && ![strEstadoEnsayoCompresion isEqualToString:@"RCH"]) && !bFgPerdida;
    cell.r7Lbl.text = mostrarResultados?strPromResis:@"";
    cell.fVaciadoLbl.text = [ToolBox getddMMyyyyForStringDate:[dicCert objectForKey:@"fechaVaciado"]];
    cell.fEnsayoLbl.text = [ToolBox getddMMyyyyForStringDate:[dicCert objectForKey:@"fechaCertificado"]];
    return cell;
}
#pragma mark - Auxiliar
-(int)getStatusForDicCert:(NSDictionary *)dicCert{
    NSString *strEstadoEnsayoCompresion = [[dicCert objectForKey:@"estadoEnsayoCompresion"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strMotivoPerdida = [[dicCert objectForKey:@"motivoPerdida"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strFgPerdida = [dicCert objectForKey:@"fgPerdida"];
    BOOL bFgPerdida = [strFgPerdida isEqualToString:@"true"];
    id diasFaltantes = [dicCert objectForKey:@"diasFaltantes"];
    int intDFaltantes = [diasFaltantes intValue];
    if(bFgPerdida && (strMotivoPerdida.length > 0)) {
        return 1;
    }
    if([strEstadoEnsayoCompresion isEqualToString:@"ACE"]){
        if(intDFaltantes == 0){
            return 3;
        }else{
            return 2;
        }
    }
    if(bFgPerdida && (strMotivoPerdida.length == 0)){
        return 2;
    }
    if(!bFgPerdida && ![strEstadoEnsayoCompresion isEqualToString:@"ACE"]){
        return 2;
    }
    return -1;
}
-(void)deviceChangedOrientation{
    [self.certificadosTableView reloadData];
}
#pragma mark - UIDocumentInteractionCont
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
@end
