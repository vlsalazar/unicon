//
//  RepHisViewController.m
//  unicon
//
//  Created by victor salazar on 24/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "RepHisViewController.h"
#import "RepHisDetTableViewCell.h"
@implementation RepHisViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    id despachos = [self.dicRepHisDespacho objectForKey:@"despachos"];
    if([despachos isKindOfClass:[NSDictionary class]]){
        arrDespachos = @[despachos];
    }else{
        arrDespachos = despachos;
    }
    float totalTeo = 0.0;
    float totalVolumen = 0.0;
    for(NSDictionary *dicDespacho in arrDespachos){
        float teo = [[dicDespacho objectForKey:@"teo"] floatValue];
        float delvQty = [[dicDespacho objectForKey:@"delvQty"] floatValue];
        totalTeo += teo;
        totalVolumen += delvQty;
    }
    totalTeo = totalTeo/(float)arrDespachos.count;
    
    NSString *strFecha = [ToolBox getddMMyyyyForStringDate:[self.dicRepHisDespacho objectForKey:@"fechaDespacho"]];
    NSMutableAttributedString *attrStrFecha = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Fecha: %@", strFecha]];
    [attrStrFecha setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15]} range:NSMakeRange(0, 6)];
    self.fechaLbl.attributedText = attrStrFecha;
    
    NSString *strTeo = [NSString stringWithFormat:@"%.2f", totalTeo];
    NSMutableAttributedString *attrStrTeo = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"TEO: %@", strTeo]];
    [attrStrTeo setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15]} range:NSMakeRange(0, 4)];
    self.teoLbl.attributedText = attrStrTeo;
    
    NSString *strVolumen = [NSString stringWithFormat:@"%.2f", totalVolumen];
    NSMutableAttributedString *attrStrVolumen = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Volumen: %@ m3", strVolumen]];
    [attrStrVolumen setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15]} range:NSMakeRange(0, 8)];
    [attrStrVolumen setAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:10], NSBaselineOffsetAttributeName: @4} range:NSMakeRange(11 + strVolumen.length, 1)];
    self.volumenLbl.attributedText = attrStrVolumen;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.despachosTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:[arrDespachos objectAtIndex:indexPath.row] forKey:@"dicDespacho"];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrDespachos.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RepHisDetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    NSDictionary *dicDespacho = [arrDespachos objectAtIndex:indexPath.row];
    id guia = [dicDespacho objectForKey:@"guia"];
    id orderCode = [dicDespacho objectForKey:@"orderCode"];
    id shortProdDescr = [dicDespacho objectForKey:@"shortProdDescr"];
    id delvQty = [dicDespacho objectForKey:@"delvQty"];
    id truckCode = [dicDespacho objectForKey:@"truckCode"];
    id usageDescr = [dicDespacho objectForKey:@"usageDescr"];
    id teo = [dicDespacho objectForKey:@"teo"];
    id schlNum = [dicDespacho objectForKey:@"schlNum"];
    id plantName = [dicDespacho objectForKey:@"plantName"];
    cell.guiaLbl.text = guia?guia:@"";
    cell.nPedidoLbl.text = orderCode?[orderCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    cell.disenoLbl.text = shortProdDescr?[shortProdDescr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    cell.volumenLbl.text = delvQty?delvQty:@"";
    cell.camionLbl.text = truckCode?[truckCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    cell.elementoLbl.text = usageDescr?[usageDescr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    cell.teoLbl.text = teo?teo:@"";
    cell.progNroLbl.text = schlNum?schlNum:@"";
    cell.plantaLbl.text = plantName?[plantName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]:@"";
    return cell;
}
@end