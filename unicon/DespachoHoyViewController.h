//
//  DepachoHoyViewController.h
//  unicon
//
//  Created by victor salazar on 14/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface DespachoHoyViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>{
    float originalHeightLeyendaView;
    NSArray *arrPedidos;
    UIRefreshControl *obraRefCtrl;
    NSURLConnection *currentCon;
    NSMutableData *currentData;
}
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightLeyendaView;
@property(nonatomic,weak) IBOutlet UILabel *titleLbl;
@property(nonatomic,weak) IBOutlet UILabel *addressLbl;
@property(nonatomic,weak) IBOutlet UITableView *pedidosTableView;
@property(nonatomic,weak) NSDictionary *dicObra;
@end