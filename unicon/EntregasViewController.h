//
//  EntregasViewController.h
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EntregasViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *arrEntregasPorTipo;
}
@property(nonatomic,weak) NSArray *arrEntregas;
@property(nonatomic,weak) IBOutlet UITableView *entregasTableView;
@property(nonatomic,weak) IBOutlet UILabel *subtotalLbl;
@property(nonatomic,weak) IBOutlet UILabel *igvLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalLbl;
@end