//
//  ToolBox.h
//  SieWeb
//
//  Created by DSB Mobile on 5/5/15.
//  Copyright (c) 2015 DSBMobile. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define NaranjaColor [UIColor colorWithRed:1.0 green:153.0/255.0 blue:0.0 alpha:1.0]
#define NaranjaCertColor [UIColor colorWithRed:1.0 green:135.0/255.0 blue:0.0 alpha:1.0]
#define VerdeColor [UIColor colorWithRed:0.0 green:158.0/255.0 blue:71.0/255.0 alpha:1.0]
#define VerdeCertColor [UIColor colorWithRed:0.0 green:146.0/255.0 blue:47.0/255.0 alpha:1.0]
#define AzulColor [UIColor colorWithRed:0.0 green:43.0/255.0 blue:178.0/255.0 alpha:1.0]

#define ParColor [UIColor colorWithRed:249.0/255.0 green:249.0/255.0 blue:249.0/255.0 alpha:1.0]
#define ImparColor [UIColor colorWithRed:237.0/255.0 green:237.0/255.0 blue:237.0/255.0 alpha:1.0]

@interface ToolBox:NSObject
+(NSMutableArray *)getMenu;
+(UIColor *)getColorYellow;
+(UIColor *)getColorBlue;
+(NSString *)convertDateToString:(NSDate *)date;
+(void)showAlertaErrorConnectionInViewCont:(UIViewController *)viewCont;
+(void)showAlertaErrorInViewCont:(UIViewController *)viewCont;
+(void)showNoDataInViewCont:(UIViewController *)viewCont;
+(void)showNoDataInViewCont:(UIViewController *)viewCont withOkHandler:(void (^)(UIAlertAction *action))handler;
+(void)showAlertaTimeOutInViewCont:(UIViewController *)viewCont;
+(void)showAlertaWithMessage:(NSString *)message inViewCont:(UIViewController *)viewCont;
+(void)showAlertErrorWithError:(NSError *)error withOkHandler:(void (^)(UIAlertAction *action))handler inViewCont:(UIViewController *)viewCont;
+(NSString *)getHHMMForStringDate:(NSString *)strDate;
+(NSString *)getHHMMForStringDate2:(NSString *)strDate;
+(NSString *)getddMMyyyyForStringDate:(NSString *)strDate;
+(NSString *)getddMMyyyyForStringDate2:(NSString *)strDate;
+(NSString *)getddMMyyyyForStringDate3:(NSString *)strDate;
+(NSArray *)getArrEstadosAcuerdosComerciales;
+(NSArray *)getArrMetodosFacturacionAcuerdosComerciales;
+(NSArray *)getArrTipoResultadosCertificados;
+(NSString *)getStringFormNumberFormatterWithNumber:(NSNumber *)number;
+(BOOL)isErrorConnectionInternet:(NSInteger)errorCode;
@end
