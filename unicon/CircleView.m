//
//  CircleView.m
//  unicon
//
//  Created by victor salazar on 15/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "CircleView.h"
@implementation CircleView
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        self.layer.cornerRadius = self.frame.size.width/2.0;
    }
    return self;
}
@end