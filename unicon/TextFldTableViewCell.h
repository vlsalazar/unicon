//
//  TextFldTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface TextFldTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *titleLbl;
@property(nonatomic,weak) IBOutlet UITextField *valueTxtFld;
@end