//
//  User.h
//  unicon
//
//  Created by victor salazar on 31/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "Empresa.h"
#import "Obra.h"
#import "Usuario+CoreDataClass.h"
@interface UsuarioActual:NSObject
@property(nonatomic,strong) NSString *correo;
@property(nonatomic,strong) NSString *nombre;
@property(nonatomic,strong) NSString *usuario;
@property(nonatomic,strong) NSString *codigoUsuario;
@property(nonatomic,strong) NSString *codigoCliente;//vacio usuario unicon
@property(nonatomic,strong) NSString *descripcionCliente;//vacio usuario unicon

@property(nonatomic,strong) NSArray *arrObras;
@property(nonatomic,strong) NSArray *arrObrasCredito;
@property(nonatomic,strong) NSArray *arrEmpresas;
@property(nonatomic,strong) Empresa *empresaSeleccionada;
@property(nonatomic,strong) Obra *obraSeleccionada;
@property(nonatomic,strong) Obra *obraCreditoSeleccionada;
@property(nonatomic) BOOL pidioObras;
@property(nonatomic) BOOL pidioObrasCreditos;
@property(nonatomic,strong) NSMutableArray *arrMenu;
@property(nonatomic,strong) NSDictionary *dicBusqDespHoy;
+(UsuarioActual *)currentUser;
+(void)cargarUsuarioActualInfo:(Usuario *)usuario;
+(NSString *)codigoCliente;
+(NSString *)codigoUsuario;
-(BOOL)esUsuarioUnicon;
@end
