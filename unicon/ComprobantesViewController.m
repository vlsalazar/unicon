//
//  ComprobantesViewController.m
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ComprobantesViewController.h"
#import "ComprobanteTableViewCell.h"
@implementation ComprobantesViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrItemsSelected = [NSMutableArray array];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.comprobantesTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:[self.arrComprobantes objectAtIndex:indexPath.row] forKey:@"dicComprobante"];
}
#pragma mark - IBAction
-(IBAction)actionSwitch:(UISwitch *)sender{
    if(sender.on){
        [arrItemsSelected addObject:[NSNumber numberWithInteger:sender.tag]];
    }else{
        for(NSNumber *index in arrItemsSelected){
            if(index.intValue == sender.tag){
                [arrItemsSelected removeObject:index];
                break;
            }
        }
    }
}
-(IBAction)actionExport:(id)sender{
    if(arrItemsSelected.count == 0){
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Atención" message:@"Debe seleccionar una guía para poder exportar" preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertCont animated:true completion:nil];
    }else{
        NSMutableArray *arrDocumentos = [NSMutableArray array];
        for(NSNumber *index in arrItemsSelected){
            NSDictionary *dicComprobante = [self.arrComprobantes objectAtIndex:index.intValue];
            [arrDocumentos addObject:@{@"secuenciaDocumento": [dicComprobante objectForKey:@"secuenciaDocumento"]}];
        }
        NSDictionary *dicParams = @{@"documentos": arrDocumentos};
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        [ServiceConector connectToUrl:[ServiceUrl getUrlComprobanteRecepcionPDF] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
            [SVProgressHUD dismiss];
            if(error){
                if([ToolBox isErrorConnectionInternet:error.code]){
                    [ToolBox showAlertaErrorConnectionInViewCont:self];
                }else{
                    if(error.code == NSURLErrorTimedOut){
                        [ToolBox showAlertaTimeOutInViewCont:self];
                    }else{
                        [ToolBox showAlertaErrorInViewCont:self];
                    }
                }
            }else{
                NSData *currentData = receivedData;
                if(currentData.length == 0){
                    [ToolBox showNoDataInViewCont:self];
                }else{
                    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *strDocumentDirectory = arr.firstObject;
                    NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                    [currentData writeToFile:filePath atomically:true];
                    UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                    docIntCont.name = @"";
                    docIntCont.delegate = self;
                    [docIntCont presentPreviewAnimated:true];
                }
            }
        }];
    }
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrComprobantes.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicComprobante = [self.arrComprobantes objectAtIndex:indexPath.row];
    ComprobanteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    cell.secuenciaLbl.text = [dicComprobante objectForKey:@"secuenciaDocumento"];
    cell.numCompLbl.text = [NSString stringWithFormat:@"%@-%@", [dicComprobante objectForKey:@"serieDocumento"], [dicComprobante objectForKey:@"numeroCorrelativoDocumento"]];
    cell.fechaLbl.text = [ToolBox getddMMyyyyForStringDate3:[dicComprobante objectForKey:@"fechaEmision"]];
    cell.montoLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicComprobante objectForKey:@"montoPercepcionSoles"] doubleValue]]];
    cell.porcPerLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicComprobante objectForKey:@"porcentajePercepcion"] doubleValue]]];
    cell.selectedSwitch.on = false;
    cell.selectedSwitch.tag = indexPath.row;
    for(NSNumber *index in arrItemsSelected){
        if(index.intValue == cell.selectedSwitch.tag){
            cell.selectedSwitch.on = true;
        }
    }
    return cell;
}
#pragma mark - UIDocumentInteractionCont
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
@end