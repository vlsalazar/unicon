//
//  ViewController.h
//  unicon
//
//  Created by DSB Mobile on 27/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "LoginIpadView.h"
@interface LoginViewController:UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    LoginIpadView *ipadView;
    BOOL loggingIn;
    NSDictionary *dicUser, *dicCurrentEnterprise;
    NSArray *arrEnterprises;
}
@property(nonatomic,weak) IBOutlet UITextField *usernameTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *enterprisesTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *passwordTxtFld;
@property(nonatomic,weak) IBOutlet UIButton *searchBtn;
@property(nonatomic,weak) IBOutlet UIButton *loginBtn;
@property(nonatomic,weak) IBOutlet UIView *containerEntPassView;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *usernameRightSpaceConst;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *entPassHeightCont;
@end