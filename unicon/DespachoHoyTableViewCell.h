//
//  DespachoHoyTableViewCell.h
//  unicon
//
//  Created by victor salazar on 13/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ProgressView.h"
@interface DespachoHoyTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet ProgressView *progressView;
@property(nonatomic,weak) IBOutlet UILabel *estatusLbl;
@property(nonatomic,weak) IBOutlet UIButton *backBtn;
@end