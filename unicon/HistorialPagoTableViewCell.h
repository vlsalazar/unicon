//
//  HistorialPagoTableViewCell.h
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface HistorialPagoTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *acuerdoLbl;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UILabel *pagoSolesLbl;
@property(nonatomic,weak) IBOutlet UIView *separadorSolesDolaresView;
@property(nonatomic,weak) IBOutlet UILabel *pagoDolaresLbl;
@end