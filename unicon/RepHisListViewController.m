//
//  RepHisListViewController.m
//  unicon
//
//  Created by victor salazar on 10/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "RepHisListViewController.h"
#import "RepHisTableViewCell.h"
@implementation RepHisListViewController
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [segue.destinationViewController setValue:dicResult forKey:@"dicRepHisDespacho"];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrReportes.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RepHisTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *dicDespacho = [self.arrReportes objectAtIndex:indexPath.row];
    cell.fechaLbl.text = [ToolBox getddMMyyyyForStringDate:[dicDespacho objectForKey:@"orderDate"]];
    NSNumberFormatter *numFormat = [NSNumberFormatter new];
    numFormat.numberStyle = NSNumberFormatterDecimalStyle;
    numFormat.maximumFractionDigits = 2;
    numFormat.minimumFractionDigits = 2;
    numFormat.roundingMode = NSNumberFormatterRoundHalfEven;
    cell.teoLbl.text = [numFormat stringFromNumber:[NSNumber numberWithFloat:[[dicDespacho objectForKey:@"teo"] floatValue]]];
    cell.cantM3Lbl.text = [numFormat stringFromNumber:[NSNumber numberWithFloat:[[dicDespacho objectForKey:@"delvQty"] floatValue]]];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicDespacho = [self.arrReportes objectAtIndex:indexPath.row];
    NSString *strFecha = [ToolBox getddMMyyyyForStringDate:[dicDespacho objectForKey:@"orderDate"]];
    NSMutableDictionary *dicParamsTemp = [NSMutableDictionary dictionaryWithDictionary:self.dicParams];
    [dicParamsTemp setObject:strFecha forKey:@"desde"];
    [dicParamsTemp setObject:strFecha forKey:@"hasta"];
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    [ServiceConector connectToUrl:[ServiceUrl getUrlReporteHistorico] withParameters:dicParamsTemp withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        if(error){
            if([ToolBox isErrorConnectionInternet:error.code]){
                [ToolBox showAlertaErrorConnectionInViewCont:self];
            }else{
                if(error.code == NSURLErrorTimedOut){
                    [ToolBox showAlertaTimeOutInViewCont:self];
                }else{
                    [ToolBox showAlertaErrorInViewCont:self];
                }
            }
        }else{
            NSError *jsonError = nil;
            id result = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                [ToolBox showAlertaErrorInViewCont:self];
            }else{
                if([result isEqual:[NSNull null]]){
                    [ToolBox showNoDataInViewCont:self];
                }else{
                    dicResult = [result objectForKey:@"reporteHistoricoDespachoBean"];
                    [self performSegueWithIdentifier:@"showReporteHistorico" sender:self];
                }
            }
        }
    }];
}
@end