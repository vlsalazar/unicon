//
//  MainListTableViewController.m
//  unicon
//
//  Created by DSB Mobile on 5/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "MainListTableViewController.h"
#import "DespachoHoyTableViewCell.h"
@implementation MainListTableViewController
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([self.option isEqualToString:@"0-0"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        [segue.destinationViewController setValue:[self.arrList objectAtIndex:indexPath.row] forKey:@"dicObra"];
    }else if([self.option isEqualToString:@"0-1"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        [segue.destinationViewController setValue:[self.arrList objectAtIndex:indexPath.row] forKey:@"dicObra"];
    }
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.option isEqualToString:@"0-1"]){
        return 90.0f;
    }
    return 40.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.option isEqualToString:@"0-0"]){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.textLabel.text = [[self.arrList objectAtIndex:indexPath.row] objectForKey:@"descObr"];
        return cell;
    }else if ([self.option isEqualToString:@"0-1"]){
        DespachoHoyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"despachoHoyCell" forIndexPath:indexPath];
        NSDictionary *dicObra = [self.arrList objectAtIndex:indexPath.row];
        cell.backgroundColor = (indexPath.row%2 == 0)?[UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0]:[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
        cell.obraLbl.text = [dicObra objectForKey:@"descObr"];
        cell.progressView.progress = [[dicObra objectForKey:@"porcAvanceObra"] floatValue];
        return cell;
    }
    return nil;
}
@end