//
//  AcuerComerDetalleViewController.m
//  unicon
//
//  Created by victor salazar on 29/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "AcuerComerDetalleViewController.h"
#import "AcuerComerDocTableViewCell.h"
@implementation AcuerComerDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrDocs = [NSMutableArray array];
    id documentos = [self.dicAcuerComer objectForKey:@"documentos"];
    if([documentos isKindOfClass:[NSDictionary class]]){
        documentos = @[documentos];
    }
    for(NSDictionary *dicDoc in documentos){
        NSString *tipoDocumento = [dicDoc objectForKey:@"tipodocumento"];
        if([tipoDocumento isEqualToString:@"OCO"] || [tipoDocumento isEqualToString:@"CGV"] || [tipoDocumento isEqualToString:@"CON"] || [tipoDocumento isEqualToString:@"COT"]){
            [arrDocs addObject:dicDoc];
        }
    }
    self.obraLbl.text = [[self.dicAcuerComer objectForKey:@"obra"] objectForKey:@"descripcion"];
    self.numAcuerLbl.text = [self.dicAcuerComer objectForKey:@"codigo"];
    self.fechaAcuerLbl.text = [ToolBox getddMMyyyyForStringDate:[self.dicAcuerComer objectForKey:@"fechaInicioAcuerdo"]];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrDocs.count;;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AcuerComerDocTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"acuerComerDocCell" forIndexPath:indexPath];
    NSDictionary *dicDoc = [arrDocs objectAtIndex:indexPath.row];
    id rutaDoc = [dicDoc objectForKey:@"rutaDocumento"];
    NSString *strRuta = @"";
    if(![rutaDoc isEqual:[NSNull null]]){
        if([rutaDoc length] > 0){
            NSArray *arrRuta = [rutaDoc componentsSeparatedByString:@"\\"];
            strRuta = arrRuta.lastObject;
        }
    }
    cell.docLbl.text = [NSString stringWithFormat:@"%@ / %@ / %@", [dicDoc objectForKey:@"numeroDocumento"], [ToolBox getddMMyyyyForStringDate2:[dicDoc objectForKey:@"fechaRegistro"]], strRuta];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicDoc = [arrDocs objectAtIndex:indexPath.row];
    NSString *tipoDocumento = [dicDoc objectForKey:@"tipodocumento"];
    NSDictionary *dicParams;
    NSString *strUrl;
    if([tipoDocumento isEqualToString:@"COT"]){
        dicParams = @{@"codigoCotizacion": [self.dicAcuerComer objectForKey:@"numeroCotizacion"]};
        strUrl = [ServiceUrl getUrlAcuerdosComercialesCotizacion];
    }else{
        dicParams = @{@"numeroDocumento": [dicDoc objectForKey:@"numeroDocumento"], @"codigoAcuerdo": [self.dicAcuerComer objectForKey:@"codigo"]};
        strUrl = [ServiceUrl getUrlAcuerdosComercialesDocumentos];
    }
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    [ServiceConector connectToUrl:strUrl withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        if(error){
            if([ToolBox isErrorConnectionInternet:error.code]){
                [ToolBox showAlertaErrorConnectionInViewCont:self];
            }else{
                if(error.code == NSURLErrorTimedOut){
                    [ToolBox showAlertaTimeOutInViewCont:self];
                }else{
                    [ToolBox showAlertaErrorInViewCont:self];
                }
            }
        }else{
            if([receivedData length] == 0){
                [ToolBox showNoDataInViewCont:self];
            }else{
                NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *strDocumentDirectory = arr.firstObject;
                NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                [receivedData writeToFile:filePath atomically:true];
                UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                docIntCont.name = @"";
                docIntCont.delegate = self;
                [docIntCont presentPreviewAnimated:true];
            }
        }
    }];
}
#pragma mark - Auxiliar
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
@end