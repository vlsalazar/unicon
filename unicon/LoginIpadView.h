//
//  LoginIpadView.h
//  unicon
//
//  Created by DSB Mobile on 28/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface LoginIpadView:UIView
@property(nonatomic,weak) IBOutlet UITextField *usernameTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *enterprisesTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *passwordTxtFld;
@property(nonatomic,weak) IBOutlet UIButton *searchBtn;
@property(nonatomic,weak) IBOutlet UIButton *loginBtn;
@property(nonatomic,weak) IBOutlet UIView *containerUsernameView;
@property(nonatomic,weak) IBOutlet UIView *containerEntPassView;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *usernameRightSpaceConst;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *entPassHeightCont;
@end