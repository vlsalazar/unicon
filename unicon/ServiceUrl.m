//
//  ServiceUrl.m
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ServiceUrl.h"
@implementation ServiceUrl
+(NSString *)baseUrl{
    //"https://sitest.unicon.com.pe";
    //"http://172.25.0.61";
    //"https://si.unicon.com.pe";
    return [NSString stringWithFormat:@"%@/autoservicio/rest/RSAutoservicio/", [[NSUserDefaults standardUserDefaults] objectForKey:baseURLKey]];
}
+(NSString *)getUrlLogin{
    return [NSString stringWithFormat:@"%@getUsuarioGenAutoservicio", [self baseUrl]];
}
+(NSString *)getUrlEmpresas{
    return [NSString stringWithFormat:@"%@getEmpresasAutoservicio", [self baseUrl]];
}
+(NSString *)getUrlObras{
    return [NSString stringWithFormat:@"%@obras/cliente", [self baseUrl]];
}
+(NSString *)getUrlObrasCredito{
    return [NSString stringWithFormat:@"%@obras/creditos/cliente", [self baseUrl]];
}
+(NSString *)getUrlProgramaSemana{
    return [NSString stringWithFormat:@"%@obrasEnDespacho", [self baseUrl]];
}
+(NSString *)getUrlEmpresasUsuarioUnicon{
    return [NSString stringWithFormat:@"%@clientes/usuario", [self baseUrl]];
}
+(NSString *)getUrlProgramaSemanaPDF{
    return [NSString stringWithFormat:@"%@reportes/pedidosCS", [self baseUrl]];
}
+(NSString *)getUrlDespachoHoyMapa{
    return [NSString stringWithFormat:@"%@truck/locations", [self baseUrl]];
}
+(NSString *)getUrlDespachoHoyLista{
    return [NSString stringWithFormat:@"%@despachos/commandSeries", [self baseUrl]];
}
+(NSString *)getUrlReporteHistoricoResumen{
    return [NSString stringWithFormat:@"%@despachos/historico/resumen", [self baseUrl]];
}
+(NSString *)getUrlReporteHistorico{
    return [NSString stringWithFormat:@"%@despachos/historico", [self baseUrl]];
}
+(NSString *)getUrlReporteHistoricoPDF{
    return [NSString stringWithFormat:@"%@reportes/historico/pedidosCs", [self baseUrl]];
}
+(NSString *)getUrlGestionDescarga{
    return [NSString stringWithFormat:@"%@reporte/teo", [self baseUrl]];
}
+(NSString *)getUrlGestionProgramacion{
    return [NSString stringWithFormat:@"%@reporte/cumplimientoProgramacion", [self baseUrl]];
}
+(NSString *)getUrlPuntualidadServicio{
    return [NSString stringWithFormat:@"%@reporte/puntualidad", [self baseUrl]];
}
+(NSString *)getUrlAcuerdosComerciales{
    return [NSString stringWithFormat:@"%@acuerdos", [self baseUrl]];
}
+(NSString *)getUrlAcuerdosComercialesCotizacion{
    return [NSString stringWithFormat:@"%@reportes/cotizacion", [self baseUrl]];
}
+(NSString *)getUrlAcuerdosComercialesDocumentos{
    return [NSString stringWithFormat:@"%@reportes/documentoComercial", [self baseUrl]];
}
+(NSString *)getUrlAcuerdosComercialesPDF{
    return [NSString stringWithFormat:@"%@reportes/acuerdoComercial", [self baseUrl]];
}
+(NSString *)getUrlCertificados{
    return [NSString stringWithFormat:@"%@guiasDeRemision", [self baseUrl]];
}
+(NSString *)getUrlCertificadosPDFCertificado{
    return [NSString stringWithFormat:@"%@reportes/certificado", [self baseUrl]];
}
+(NSString *)getUrlCertificadosPDFComunicado{
    return [NSString stringWithFormat:@"%@reportes/comunicado", [self baseUrl]];
}
+(NSString *)getUrlEstadoCuenta{
    return [NSString stringWithFormat:@"%@estadoCuenta/resumen", [self baseUrl]];
}
+(NSString *)getUrlEstadoCuentaPDF{
    return [NSString stringWithFormat:@"%@reporte/estadoCuenta", [self baseUrl]];
}
+(NSString *)getUrlEntregaClienteObra{
    return [NSString stringWithFormat:@"%@entregas/resumen", [self baseUrl]];
}
+(NSString *)getUrlEntregaClienteObraPDF{
    return [NSString stringWithFormat:@"%@reporte/entregas", [self baseUrl]];
}
+(NSString *)getUrlHistorialPagos{
    return [NSString stringWithFormat:@"%@historialPagos/resumen", [self baseUrl]];
}
+(NSString *)getUrlHistorialPagosPDF{
    return [NSString stringWithFormat:@"%@reporte/historialPagos", [self baseUrl]];
}
+(NSString *)getUrlComprobanteRecepcion{
    return [NSString stringWithFormat:@"%@documentosPercepcion", [self baseUrl]];
}
+(NSString *)getUrlComprobanteRecepcionPDF{
    return [NSString stringWithFormat:@"%@reporte/documentoPercepcion", [self baseUrl]];
}
+(NSString *)getUrlEstadoCredito{
    return [NSString stringWithFormat:@"%@estadisticoCredito", [self baseUrl]];
}
+(NSString *)getUrlEstadoCreditoPDF{
    return [NSString stringWithFormat:@"%@reporte/estadoCredito", [self baseUrl]];
}
@end