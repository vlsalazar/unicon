//
//  CertificadosDetalleViewController.h
//  unicon
//
//  Created by DSB Mobile on 3/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface CertificadosDetalleViewController:UIViewController<UIDocumentInteractionControllerDelegate>
@property(nonatomic,weak) NSDictionary *dicCertificado;
@property(nonatomic,weak) IBOutlet UILabel *tituloLbl;
@property(nonatomic,weak) IBOutlet UILabel *numGuiaLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaVacLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaEnLbl;
@property(nonatomic,weak) IBOutlet UILabel *estadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *resultadosTituloLbl;
@property(nonatomic,weak) IBOutlet UILabel *resultadosLbl;
@property(nonatomic,weak) IBOutlet UILabel *diasFaltantesLbl;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightExportConstraint;
@end