//
//  OrderDetailViewController.h
//  unicon
//
//  Created by DSB Mobile on 11/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ProgamaSemanaDetalleViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicOrder;
@property(nonatomic,weak) NSString *strTitle;
@property(nonatomic,weak) IBOutlet UITextView *detailTxtView;
@end