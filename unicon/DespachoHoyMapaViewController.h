//
//  DespachoHoyMapaViewController.h
//  unicon
//
//  Created by victor salazar on 18/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface DespachoHoyMapaViewController:UIViewController<NSURLConnectionDataDelegate, NSURLConnectionDelegate, GMSMapViewDelegate>{
    BOOL didFirstLoad;
    NSDictionary *dicParams;
    NSURLConnection *currentCon;
    NSMutableData *currentData;
    NSArray *arrResult;
    NSMutableArray *arrMarkers;
}
@property(nonatomic,weak) NSDictionary *dicPedido;
@property(nonatomic,weak) IBOutlet GMSMapView *mapView;
@end