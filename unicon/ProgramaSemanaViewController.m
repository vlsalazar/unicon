//
//  TwoEntriesTableViewController.m
//  unicon
//
//  Created by DSB Mobile on 5/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ProgramaSemanaViewController.h"
#import "ProgramaSemanaTableViewCell.h"
@implementation ProgramaSemanaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    id pedidos = [self.dicObra objectForKey:@"pedidos"];
    if([pedidos isKindOfClass:[NSDictionary class]]){
        arrPedidos = @[pedidos];
    }else{
        arrPedidos = pedidos;
    }
    self.obraLbl.text = [self.dicObra objectForKey:@"descObr"];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    int row = (int)[[self.programasTableView indexPathForSelectedRow] row];
    [segue.destinationViewController setValue:[arrPedidos objectAtIndex:row] forKey:@"dicOrder"];
    [segue.destinationViewController setValue:[self.dicObra objectForKey:@"descObr"] forKey:@"strTitle"];
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrPedidos.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProgramaSemanaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    NSDictionary *dicPedido = [arrPedidos objectAtIndex:(indexPath.row)];
    cell.fechaLbl.text = [ToolBox getddMMyyyyForStringDate:[dicPedido objectForKey:@"orderDate"]];
    cell.nPedidoLbl.text = [dicPedido objectForKey:@"orderCode"];
    cell.disenioLbl.text = [dicPedido objectForKey:@"shortescr"];
    cell.volumenLbl.text = [dicPedido objectForKey:@"orderQty"];
    cell.tPedidoLbl.text = [dicPedido objectForKey:@"deTiPed"];
    cell.estadoLbl.text = [dicPedido objectForKey:@"deEsOrdr"];
    cell.horaLbl.text = [ToolBox getHHMMForStringDate:[dicPedido objectForKey:@"startTime"]];
    return cell;
}
@end