//
//  Usuario+CoreDataClass.h
//  unicon
//
//  Created by victor salazar on 3/21/17.
//  Copyright © 2017 Victor Salazar. All rights reserved.
//
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | INICIO
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class UsuarioActual;
NS_ASSUME_NONNULL_BEGIN
@interface Usuario:NSManagedObject
+(Usuario * _Nullable)obtenerUsuarioActual;
+(void)eliminarUsuario;
+(void)guardarUsuarioActual:(UsuarioActual *)usuarioActual;
@end
NS_ASSUME_NONNULL_END
#import "Usuario+CoreDataProperties.h"
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | FIN
