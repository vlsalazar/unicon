//
//  FilterTableViewController.h
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Obra.h"
#import "Empresa.h"
@interface FilterViewController:UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, NSURLConnectionDataDelegate, NSURLConnectionDelegate, UIDocumentInteractionControllerDelegate>{
    BOOL checkDatePicker, isSearch, esCreditos, reloadObras;
    int indEmpCell, indObrCell, indMetFac, indEstado, indTipoRes;
    Obra *currentObra;
    Empresa *currentEmpresa;
    NSArray *arrObras, *arrResult, *arrMetodosFacturacion, *arrEstados, *arrTiposResultados;
    NSDictionary *dicObra, *dicParamsRepHis;
    NSMutableArray *arrInfoCells;
    NSMutableData *currentData;
    NSString *tipoConsulta;
    NSURLConnection *currentCon;
    UITextField *currentTxtFld;
}
@property(nonatomic,weak) IBOutlet UITableView *filterTableView;
@property(nonatomic,weak) IBOutlet UIButton *exportBtn;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightBottomView;
@property(nonatomic,strong) NSString *option;

@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightMainView;
@property(nonatomic,weak) IBOutlet UIView *contentView;

-(void)loadObras;
@end