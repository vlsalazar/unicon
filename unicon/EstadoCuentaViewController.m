//
//  EstadoCuentaViewController.m
//  unicon
//
//  Created by victor salazar on 6/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EstadoCuentaViewController.h"
#import "EstadoCuentaTableViewCell.h"
@implementation EstadoCuentaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    for(NSDictionary *dicEstCuenta in self.arrEstadoCuenta){
        NSString *tipoMoneda = [dicEstCuenta objectForKey:@"tipoMoneda"];
        if([tipoMoneda isEqualToString:@"S"] || [tipoMoneda isEqualToString:@"A"]){
            esSoles = true;
        }
        if([tipoMoneda isEqualToString:@"D"] || [tipoMoneda isEqualToString:@"A"]){
            esDolares = true;
        }
    }
    if(esSoles){
        self.totalDeudaTotalSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalDeudaTotalSoles]];
        self.totalDeudaVencidaSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalDeudaVencidaSoles]];
        self.totalIFacSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalIFacSoles]];
    }
    if(esDolares){
        self.totalDeudaTotalDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalDeudaTotalDolares]];
        self.totalDeudaVencidaDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalDeudaVencidaDolares]];
        self.totalIFacDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[self getTotalIFacDolares]];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.estadosCuentasTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:[self.arrEstadoCuenta objectAtIndex:indexPath.row] forKey:@"dicEstadoCuenta"];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceChangedOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrEstadoCuenta.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EstadoCuentaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    NSDictionary *dicEstadoCuenta = [self.arrEstadoCuenta objectAtIndex:indexPath.row];
    cell.acuerdoLbl.text = [dicEstadoCuenta objectForKey:@"acuerdoComercial"];
    cell.obraLbl.text = [dicEstadoCuenta objectForKey:@"descripcionObra"];
    if(esSoles){
        cell.deudaVencidaSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"deudaVencidaSoles"] doubleValue]]];
        cell.deudaTotalSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"deudaPendienteSoles"] doubleValue]]];
        cell.iFacSolesLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"importeOriginalSoles"] doubleValue]]];
    }else{
        [cell.iFacSolesLbl removeFromSuperview];
        [cell.iFacSolesView removeFromSuperview];
        [cell.deudaTotalSolesLbl removeFromSuperview];
        [cell.deudaTotalSolesView removeFromSuperview];
        [cell.deudaVencidaSolesLbl removeFromSuperview];
        [cell.deudaVencidaSolesView removeFromSuperview];
        [self.iFacSolesLbl removeFromSuperview];
        [self.deudaTotalSolesLbl removeFromSuperview];
        [self.deudaVencidaSolesLbl removeFromSuperview];
        [self.totalIFacSolesLbl removeFromSuperview];
        [self.totalDeudaTotalSolesLbl removeFromSuperview];
        [self.totalDeudaVencidaSolesLbl removeFromSuperview];
    }
    if(esDolares){
        cell.deudaVencidaDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"deudaVencidaDolares"] doubleValue]]];
        cell.deudaTotalDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"deudaPendienteDolares"] doubleValue]]];
        cell.iFacDolaresLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[dicEstadoCuenta objectForKey:@"importeOriginalDolares"] doubleValue]]];
    }else{
        [cell.iFacDolaresLbl removeFromSuperview];
        [cell.iFacDolaresView removeFromSuperview];
        [cell.deudaTotalDolaresLbl removeFromSuperview];
        [cell.deudaTotalDolaresView removeFromSuperview];
        [cell.deudaVencidaDolaresLbl removeFromSuperview];
        [cell.deudaVencidaSolesView removeFromSuperview];
        [self.iFacDolaresLbl removeFromSuperview];
        [self.deudaTotalDolaresLbl removeFromSuperview];
        [self.deudaVencidaDolaresLbl removeFromSuperview];
        [self.totalIFacDolaresLbl removeFromSuperview];
        [self.totalDeudaTotalDolaresLbl removeFromSuperview];
        [self.totalDeudaVencidaDolaresLbl removeFromSuperview];
    }
    return cell;
}
#pragma mark - Rotation
-(void)deviceChangedOrientation{
    [self.estadosCuentasTableView reloadData];
}
#pragma mark - Auxiliar
-(NSNumber *)getTotalDeudaVencidaSoles{
    if(!totalDeudaVencidaSoles){
        double totalDeudaVencidaSolesTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalDeudaVencidaSolesTemp += [[dicEstadoCuenta objectForKey:@"deudaVencidaSoles"] doubleValue];
        }
        totalDeudaVencidaSoles = [NSNumber numberWithDouble:totalDeudaVencidaSolesTemp];
    }
    return totalDeudaVencidaSoles;
}
-(NSNumber *)getTotalDeudaVencidaDolares{
    if(!totalDeudaVencidaDolares){
        double totalDeudaVencidaDolaresTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalDeudaVencidaDolaresTemp += [[dicEstadoCuenta objectForKey:@"deudaVencidaDolares"] doubleValue];
        }
        totalDeudaVencidaDolares = [NSNumber numberWithDouble:totalDeudaVencidaDolaresTemp];
    }
    return totalDeudaVencidaDolares;
}
-(NSNumber *)getTotalDeudaTotalSoles{
    if(!totalDeudaTotalSales){
        double totalDeudaTotalSolesTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalDeudaTotalSolesTemp += [[dicEstadoCuenta objectForKey:@"deudaPendienteSoles"] doubleValue];
        }
        totalDeudaTotalSales = [NSNumber numberWithDouble:totalDeudaTotalSolesTemp];
    }
    return totalDeudaTotalSales;
}
-(NSNumber *)getTotalDeudaTotalDolares{
    if(!totalDeudaTotalDolares){
        double totalDeudaVencidaDolaresTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalDeudaVencidaDolaresTemp += [[dicEstadoCuenta objectForKey:@"deudaPendienteDolares"] doubleValue];
        }
        totalDeudaTotalDolares = [NSNumber numberWithDouble:totalDeudaVencidaDolaresTemp];
    }
    return totalDeudaTotalDolares;
}
-(NSNumber *)getTotalIFacSoles{
    if(!totalIFacSoles){
        double totalIFacSolesTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalIFacSolesTemp += [[dicEstadoCuenta objectForKey:@"importeOriginalSoles"] doubleValue];
        }
        totalIFacSoles = [NSNumber numberWithDouble:totalIFacSolesTemp];
    }
    return totalIFacSoles;
}
-(NSNumber *)getTotalIFacDolares{
    if(!totalIFacDolares){
        double totalIFacDolaresTemp = 0.0;
        for(NSDictionary *dicEstadoCuenta in self.arrEstadoCuenta){
            totalIFacDolaresTemp += [[dicEstadoCuenta objectForKey:@"importeOriginalDolares"] doubleValue];
        }
        totalIFacDolares = [NSNumber numberWithDouble:totalIFacDolaresTemp];
    }
    return totalIFacDolares;
}
@end