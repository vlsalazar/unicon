//
//  DoubleTextFieldTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 1/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface DoubleTextFieldTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *titleLbl;
@property(nonatomic,weak) IBOutlet UITextField *firstTxtFld;
@property(nonatomic,weak) IBOutlet UITextField *secondTxtFld;
@end