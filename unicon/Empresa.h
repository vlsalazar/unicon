//
//  Empresa.h
//  unicon
//
//  Created by victor salazar on 6/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface Empresa:NSObject
@property(nonatomic,strong) NSString *codigoCliente;
@property(nonatomic,strong) NSString *descripcionCliente;
@end