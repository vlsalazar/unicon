//
//  OrderDetailViewController.m
//  unicon
//
//  Created by DSB Mobile on 11/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ProgamaSemanaDetalleViewController.h"
@implementation ProgamaSemanaDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSMutableAttributedString *finalAtrString = [NSMutableAttributedString new];
    
    NSMutableParagraphStyle *paraStyle = [NSMutableParagraphStyle new];
    paraStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *titleAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n\n", self.strTitle] attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSParagraphStyleAttributeName:paraStyle, NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0]}];
    [finalAtrString appendAttributedString:titleAtrString];
    
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    NSDate *orderDate = [dateFormat dateFromString:[self.dicOrder objectForKey:@"orderDate"]];
    NSDate *startTimeDate = [dateFormat dateFromString:[self.dicOrder objectForKey:@"startTime"]];
    NSAttributedString *fechaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Fecha: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:fechaTitleAtrString];
    dateFormat.dateFormat = @"dd/MM/yyyy";
    NSAttributedString *fechaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [dateFormat stringFromDate:orderDate]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:fechaValueAtrString];
    
    NSAttributedString *horaTitleAtrString = [[NSAttributedString alloc] initWithString:@"Hora: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:horaTitleAtrString];
    dateFormat.dateFormat = @"hh:mm'h'";
    NSAttributedString *horaValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [dateFormat stringFromDate:startTimeDate]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:horaValueAtrString];
    
    NSAttributedString *direccionTitleAtrString = [[NSAttributedString alloc] initWithString:@"Dirección: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:direccionTitleAtrString];
    NSAttributedString *direccionValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [self.dicOrder objectForKey:@"delvAddr"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:direccionValueAtrString];
    
    NSAttributedString *nPedidoTitleAtrString = [[NSAttributedString alloc] initWithString:@"Número de Pedido: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:nPedidoTitleAtrString];
    NSAttributedString *nPedidoValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [self.dicOrder objectForKey:@"orderCode"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:nPedidoValueAtrString];
    
    NSAttributedString *tPedidoTitleAtrString = [[NSAttributedString alloc] initWithString:@"Tipo de Pedido: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:tPedidoTitleAtrString];
    NSAttributedString *tPedidoValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [self.dicOrder objectForKey:@"deTiPed"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:tPedidoValueAtrString];
    
    NSAttributedString *disenioTitleAtrString = [[NSAttributedString alloc] initWithString:@"Diseño: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:disenioTitleAtrString];
    NSAttributedString *disenioValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", [self.dicOrder objectForKey:@"shortescr"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:disenioValueAtrString];
    
    NSAttributedString *volumenTitleAtrString = [[NSAttributedString alloc] initWithString:@"Volumen: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:volumenTitleAtrString];
    NSString *orderQtyStr = [self.dicOrder objectForKey:@"orderQty"];
    NSMutableAttributedString *volumenValueAtrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@m3\n", orderQtyStr] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [volumenValueAtrString addAttributes:@{NSBaselineOffsetAttributeName: @3} range:NSMakeRange(orderQtyStr.length + 1, 1)];
    [finalAtrString appendAttributedString:volumenValueAtrString];
    
    NSAttributedString *estadoTitleAtrString = [[NSAttributedString alloc] initWithString:@"Estado: " attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:estadoTitleAtrString];
    NSAttributedString *estadoValueAtrString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [self.dicOrder objectForKey:@"deEsOrdr"]] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:15], NSForegroundColorAttributeName: [UIColor colorWithRed:2.0/255.0 green:28.0/255.0 blue:78.0/255.0 alpha:1]}];
    [finalAtrString appendAttributedString:estadoValueAtrString];
    self.detailTxtView.attributedText = finalAtrString;
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
@end