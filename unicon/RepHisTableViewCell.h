//
//  RepHisTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 23/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface RepHisTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *teoLbl;
@property(nonatomic,weak) IBOutlet UILabel *cantM3Lbl;
@end