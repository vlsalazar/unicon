//
//  SideMenuViewController.m
//  unicon
//
//  Created by DSB Mobile on 1/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"
#import "MenuViewController.h"
#import "ToolBox.h"
#import "UserViewController.h"
@implementation SideMenuViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrOptions = [[UsuarioActual currentUser] arrMenu];
    self.currentIndex = [NSNumber numberWithInt:-1];
}
-(void)setCurrentIndex:(NSNumber *)currentIndex{
    _currentIndex = currentIndex;
    suboptionsCount = 0;
    if(self.currentIndex.intValue > -1){
        suboptionsCount = (int)[arrOptions[self.currentIndex.intValue][@"suboptions"] count];
    }
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
#pragma mark - Action
-(IBAction)actionLogout:(id)sender{
    UsuarioActual *usu = [UsuarioActual currentUser];
    usu.correo = @"";
    usu.nombre = @"";
    usu.usuario = @"";
    usu.arrObras = nil;
    usu.arrEmpresas = nil;
    usu.empresaSeleccionada = nil;
    usu.obraSeleccionada = nil;
    usu.codigoCliente = @"";
    usu.descripcionCliente = @"";
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Se elimina la información del usuario almacenado dentro de la BD | INICIO
    [Usuario eliminarUsuario];
    if(self.presentingViewController != nil){
        [self dismissViewControllerAnimated:true completion:nil];
    }else{
        [self.parentViewController performSegueWithIdentifier:@"showLogin" sender:nil];
    }
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Se elimina la información del usuario almacenado dentro de la BD | FIN
}
-(IBAction)actionDismiss:(id)sender{
    UserViewController *userViewCont = (UserViewController *)self.parentViewController;
    [userViewCont hideSideMenuWithOption:@""];
}
-(IBAction)actionMainMenu:(id)sender{
    UserViewController *userViewCont = (UserViewController *)self.parentViewController;
    [userViewCont showMainMenu];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrOptions.count + suboptionsCount;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int minIndex = self.currentIndex.intValue + 1;
    int maxIndex = self.currentIndex.intValue + suboptionsCount;
    int row = (int)indexPath.row;
    if(row >= minIndex && row <= maxIndex){
        UITableViewCell *suboptionCell = [tableView dequeueReusableCellWithIdentifier:@"suboptionCell" forIndexPath:indexPath];
        suboptionCell.textLabel.text = arrOptions[self.currentIndex.intValue][@"suboptions"][row - minIndex];
        suboptionCell.userInteractionEnabled = true;
        return suboptionCell;
    }else{
        if(row > maxIndex){
            row -= suboptionsCount;
        }
    }
    SideMenuTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell" forIndexPath:indexPath];
    NSDictionary *dicOption = arrOptions[row];
    BOOL enabled = [[dicOption objectForKey:@"enabled"] boolValue];
    cell.iconImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"side%@_%@", dicOption[@"image"], enabled?@"hab":@"deshab"]];
    cell.titleLbl.text = dicOption[@"title"];
    cell.collapseImgView.hidden = ([dicOption[@"suboptions"] count] == 0)?true:false;
    cell.userInteractionEnabled = enabled;
    cell.collapseImgView.transform = CGAffineTransformIdentity;
    if(!cell.collapseImgView.hidden && (row == self.currentIndex.intValue)){
        cell.collapseImgView.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    int minIndex = self.currentIndex.intValue;
    int maxIndex = self.currentIndex.intValue + suboptionsCount;
    int row = (int)indexPath.row;
    if(row > minIndex && row <= maxIndex){
        NSString *option = [NSString stringWithFormat:@"%@-%@", self.currentIndex, [NSNumber numberWithInt:(row - minIndex - 1)]];
        if([option isEqualToString:@"3-1"]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Atención" message:@"¿Desea descargar el pdf correspondiente?" preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"SI" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
                NSString *strUrl = @"http://www.unicon.com.pe/uniconDocs/condicionesGeneralesDeVentas.pdf";
                [ServiceConector connectNormalToUrl:strUrl withParameters:nil withResponse:^(id receivedData, NSError *error){
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
                    if(error){
                        if([ToolBox isErrorConnectionInternet:error.code]){
                            [ToolBox showAlertaErrorConnectionInViewCont:self];
                        }else{
                            if(error.code == NSURLErrorTimedOut){
                                [ToolBox showAlertaTimeOutInViewCont:self];
                            }else{
                                [ToolBox showAlertaErrorInViewCont:self];
                            }
                        }
                    }else{
                        NSData *data = receivedData;
                        if([data length] == 0){
                            [ToolBox showNoDataInViewCont:self];
                        }else{
                            NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                            NSString *strDocumentDirectory = arr.firstObject;
                            NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                            [data writeToFile:filePath atomically:true];
                            UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                            docIntCont.name = @"";
                            docIntCont.delegate = self;
                            [docIntCont presentPreviewAnimated:true];
                        }
                    }
                }];
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
        }else{
            UserViewController *userViewCont = (UserViewController *)self.parentViewController;
            [userViewCont hideSideMenuWithOption:option];
        }
    }else{
        if(row > maxIndex){
            row -= suboptionsCount;
        }
        if([self.currentIndex isEqualToNumber:@(row)]){
            self.currentIndex = [NSNumber numberWithInt:-1];
            suboptionsCount = 0;
        }else{
            self.currentIndex = [NSNumber numberWithInt:row];
            suboptionsCount = (int)[arrOptions[self.currentIndex.intValue][@"suboptions"] count];
        }
        [self.menuTableView reloadData];
        if(row == 2){
            UserViewController *userViewCont = (UserViewController *)self.parentViewController;
            [userViewCont hideSideMenuWithOption:@"2"];
        }
    }
}
@end
