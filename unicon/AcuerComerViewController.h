//
//  AcuerComerViewController.h
//  unicon
//
//  Created by DSB Mobile on 25/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface AcuerComerViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate>{
    NSMutableArray *arrItemsSelected;
}
@property(nonatomic,weak) NSArray *arrAcuerComers;
@property(nonatomic,weak) IBOutlet UITableView *acuerComerTableView;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint *heightLeyendaView;
@end