//
//  RepHisListViewController.h
//  unicon
//
//  Created by victor salazar on 10/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface RepHisListViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSDictionary *dicResult;
}
@property(nonatomic,weak) NSArray *arrReportes;
@property(nonatomic,weak) NSDictionary *dicParams;
@end