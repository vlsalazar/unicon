//
//  RepHisViewController.h
//  unicon
//
//  Created by victor salazar on 24/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface RepHisViewController:UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSArray *arrDespachos;
}
@property(nonatomic,weak) NSDictionary *dicRepHisDespacho;
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *teoLbl;
@property(nonatomic,weak) IBOutlet UILabel *volumenLbl;
@property(nonatomic,weak) IBOutlet UITableView *despachosTableView;
@end