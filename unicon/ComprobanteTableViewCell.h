//
//  ComprobanteTableViewCell.h
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ComprobanteTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *secuenciaLbl;
@property(nonatomic,weak) IBOutlet UILabel *numCompLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *porcPerLbl;
@property(nonatomic,weak) IBOutlet UILabel *montoLbl;
@property(nonatomic,weak) IBOutlet UISwitch *selectedSwitch;
@end