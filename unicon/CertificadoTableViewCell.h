//
//  CertificadoTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 2/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface CertificadoTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UIView *barView;
@property(nonatomic,weak) IBOutlet UILabel *nGuiaLbl;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UILabel *fVaciadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *fEnsayoLbl;
@property(nonatomic,weak) IBOutlet UILabel *r7Lbl;
@property(nonatomic,weak) IBOutlet UILabel *dFaltantesLbl;
@property(nonatomic,weak) IBOutlet UISwitch *selSwitch;;
@end