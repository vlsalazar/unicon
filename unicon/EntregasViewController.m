//
//  EntregasViewController.m
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "EntregasViewController.h"
#import "EntregaTableViewCell.h"

#define tipoEntregaProducto @"PRD"
#define tipoEntregaServicio @"SER"
#define tipoEntregaConcepto @"CNP"

@implementation EntregasViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    NSMutableArray *arrProductos = [NSMutableArray array];
    NSMutableArray *arrServicios = [NSMutableArray array];
    NSMutableArray *arrConceptos = [NSMutableArray array];
    double totalProductos = 0.0;
    double totalServicios = 0.0;
    double totalConceptos = 0.0;
    double subtotal = [[self.arrEntregas.firstObject objectForKey:@"subTotal"] doubleValue];
    double igv = [[self.arrEntregas.firstObject objectForKey:@"subTotalIgv"] doubleValue];
    double total = [[self.arrEntregas.firstObject objectForKey:@"total"] doubleValue];
    for(NSDictionary *dicEntrega in self.arrEntregas){
        NSString *strTipoProducto = [dicEntrega objectForKey:@"tipoProducto"];
        NSString *strTotalTipo = [dicEntrega objectForKey:@"totalTipo"];
        if([strTipoProducto isEqualToString:tipoEntregaProducto]){
            [arrProductos addObject:dicEntrega];
            totalProductos = [strTotalTipo doubleValue];
        }else if([strTipoProducto isEqualToString:tipoEntregaServicio]){
            [arrServicios addObject:dicEntrega];
            totalServicios = [strTotalTipo doubleValue];
        }else if([strTipoProducto isEqualToString:tipoEntregaConcepto]){
            [arrConceptos addObject:dicEntrega];
            totalConceptos = [strTotalTipo doubleValue];
        }
    }
    NSDictionary *dicProductos = @{@"titulo": @"PRODUCTO", @"items": arrProductos, @"total": [NSNumber numberWithDouble:totalProductos]};
    NSDictionary *dicServicios = @{@"titulo": @"SERVICIO", @"items": arrServicios, @"total": [NSNumber numberWithDouble:totalServicios]};
    NSDictionary *dicConceptos = @{@"titulo": @"CONCEPTO", @"items": arrConceptos, @"total": [NSNumber numberWithDouble:totalConceptos]};
    arrEntregasPorTipo = @[dicProductos, dicServicios, dicConceptos];
    self.subtotalLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:subtotal]];
    self.igvLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:igv]];
    self.totalLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:total]];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.entregasTableView indexPathForSelectedRow];
    NSDictionary *dicTemp = [arrEntregasPorTipo objectAtIndex:indexPath.section];
    NSArray *items = [dicTemp objectForKey:@"items"];
    int index = (int)indexPath.row - 1;
    NSDictionary *dicEntrega = [items objectAtIndex:index];
    [segue.destinationViewController setValue:dicEntrega forKey:@"dicEntrega"];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrEntregasPorTipo.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[arrEntregasPorTipo objectAtIndex:section] objectForKey:@"items"] count] + 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicTemp = [arrEntregasPorTipo objectAtIndex:indexPath.section];
    if(indexPath.row == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headCell" forIndexPath:indexPath];
        cell.textLabel.text = [dicTemp objectForKey:@"titulo"];
        return cell;
    }else{
        NSArray *items = [dicTemp objectForKey:@"items"];
        if(indexPath.row == (items.count + 1)){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"footCell" forIndexPath:indexPath];
            cell.textLabel.text = [ToolBox getStringFormNumberFormatterWithNumber:[dicTemp objectForKey:@"total"]];
            cell.contentView.backgroundColor = (items.count%2 == 0)?ParColor:ImparColor;
            return cell;
        }else{
            int index = (int)indexPath.row - 1;
            UIColor *colorTemp = [UIColor colorWithRed:43.0/255.0 green:42.0/255.0 blue:76.0/255.0 alpha:1.0];
            NSDictionary *dicEntrega = [items objectAtIndex:index];
            NSString *strTipoProducto = [dicEntrega objectForKey:@"tipoProducto"];
            NSString *strNombreDisenno = [[dicEntrega objectForKey:@"nombreDisenno"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *strCantidadResumen = [[dicEntrega objectForKey:@"cantidadResumen"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *strPrecioUnitario = [[dicEntrega objectForKey:@"precioUnitario"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *strTotalResumen = [[dicEntrega objectForKey:@"totalResumen"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            BOOL esBlanco = ((strTipoProducto.length == 0) && (strNombreDisenno.length > 0));
            
            EntregaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.contentView.backgroundColor = (indexPath.row%2 == 1)?ParColor:ImparColor;
            cell.articuloLbl.text = [[dicEntrega objectForKey:@"descripcionArticulo"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            cell.articuloLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.articuloLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            
            cell.disenioLbl.text = strNombreDisenno;
            cell.disenioLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.disenioLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            cell.disenioLbl.textAlignment = ((strTipoProducto.length == 0) && (strNombreDisenno.length == 0))?NSTextAlignmentRight:NSTextAlignmentCenter;
            
            cell.cantidadLbl.text = (![strCantidadResumen isEqualToString:@""])?[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strCantidadResumen.doubleValue]]:strCantidadResumen;
            cell.cantidadLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.cantidadLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            
            cell.pUnitarioLbl.text = (![strPrecioUnitario isEqualToString:@""])?[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strPrecioUnitario.doubleValue]]:strPrecioUnitario;
            cell.pUnitarioLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.pUnitarioLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            
            cell.uMedidaLbl.text = [[dicEntrega objectForKey:@"unidadDeMedida"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            cell.uMedidaLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.uMedidaLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            
            cell.totalLbl.text = (![strTotalResumen isEqualToString:@""])?[ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:strTotalResumen.doubleValue]]:strTotalResumen;
            cell.totalLbl.textColor = esBlanco?[UIColor whiteColor]:colorTemp;
            cell.totalLbl.font = (strTipoProducto.length == 0)?[UIFont boldSystemFontOfSize:15]:[UIFont systemFontOfSize:15];
            
            cell.userInteractionEnabled = (strTipoProducto.length > 0);
            return cell;
        }
    }
}
@end