//
//  EstadoCuentaDetalleViewController.h
//  unicon
//
//  Created by victor salazar on 7/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EstadoCuentaDetalleViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicEstadoCuenta;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UILabel *acuerdoLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleImpFacSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *impFacSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleImpFacDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *impFacDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleDeudaTotalSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleDeudaTotalDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleDeudaVencidaSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaSolesLbl;
@property(nonatomic,weak) IBOutlet UILabel *titleDeudaVencidaDolaresLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaVencidaDolaresLbl;
@end