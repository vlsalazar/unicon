//
//  ServiceConector.h
//  SieWeb
//
//  Created by DSB Mobile on 5/8/15.
//  Copyright (c) 2015 DSBMobile. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface ServiceConector:NSObject
+(void)internetConnectionStatus:(void (^)(BOOL connected))internetStatus;
+(NSURLConnection *)createConnectionWithDelegate:(id<NSURLConnectionDataDelegate,NSURLConnectionDelegate>)delegate withUrl:(NSString *)strUrl andParams:(id)params;
+(void)connectToUrl:(NSString *)urlString withParameters:(id)parameters withResponse:(void (^)(id receivedData, NSError *error))responseHandler;
+(void)connectNormalToUrl:(NSString *)urlString withParameters:(id)parameters withResponse:(void (^)(id receivedData, NSError *error))responseHandler;
@end