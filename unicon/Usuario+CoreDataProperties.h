//
//  Usuario+CoreDataProperties.h
//  unicon
//
//  Created by victor salazar on 3/21/17.
//  Copyright © 2017 Victor Salazar. All rights reserved.
//
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | INICIO
#import "Usuario+CoreDataClass.h"
NS_ASSUME_NONNULL_BEGIN
@interface Usuario (CoreDataProperties)
+ (NSFetchRequest<Usuario *> *)fetchRequest;
@property (nonatomic) BOOL acuerdosHabilitado;
@property (nonatomic) BOOL certificadosHabilitado;
@property (nullable, nonatomic, copy) NSString *codigoCliente;
@property (nullable, nonatomic, copy) NSString *codigoUsuario;
@property (nonatomic) BOOL consultasHabilitado;
@property (nullable, nonatomic, copy) NSString *correo;
@property (nonatomic) BOOL creditosHabilitado;
@property (nullable, nonatomic, copy) NSString *descripcionCliente;
@property (nonatomic) BOOL estatusHabilitado;
@property (nonatomic) BOOL gestionHabilitado;
@property (nullable, nonatomic, copy) NSString *nombre;
@property (nullable, nonatomic, copy) NSString *usuario;
@end
NS_ASSUME_NONNULL_END
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | FIN
