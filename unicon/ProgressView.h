//
//  ProgressView.h
//  unicon
//
//  Created by victor salazar on 13/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ProgressView:UIView{
    UIView *avanceView;
}
@property(nonatomic,weak) IBOutlet UILabel *avanceLbl;
@property(nonatomic) float progress;
@end