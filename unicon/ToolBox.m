//
//  ToolBox.m
//  SieWeb
//
//  Created by DSB Mobile on 5/5/15.
//  Copyright (c) 2015 DSBMobile. All rights reserved.
//
#import "ToolBox.h"
@implementation ToolBox
+(NSMutableArray *)getMenu{
    NSMutableDictionary *dicEstatus = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Estatus", @"title": @"Estatus de Pedidos", @"enabled": @1, @"suboptions": @[@"Programación de pedidos", @"Mi despacho para hoy", @"Reporte Histórico"]}];
    NSMutableDictionary *dicGestion = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Gestion", @"title": @"Gestión de Obra", @"enabled": @1, @"suboptions": @[@"Gestión de Descarga", @"Gestión de Programación", @"Puntualidad del Servicio"]}];
    NSMutableDictionary *dicCertificados = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Certificados", @"title": @"Certificados de Calidad", @"enabled": @1, @"suboptions": @[]}];
    NSMutableDictionary *dicAcuerdos = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Acuerdos", @"title": @"Acuerdos Comerciales", @"enabled": @1, @"suboptions": @[@"Acuerdos Comerciales", @"Condiciones Generales: Suministro de Concreto y Servicio de Bombeo"]}];
    NSMutableDictionary *dicCreditos = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Creditos", @"title": @"Créditos y Cobranzas", @"enabled": @1, @"suboptions": @[@"Estado de Cuenta", @"Entrega por razón social y obra", @"Historial de Pagos", @"Comprobante de percepción", @"Estado de Crédito"]}];
    NSMutableDictionary *dicConsultas = [NSMutableDictionary dictionaryWithDictionary:@{@"image": @"Consultas", @"title": @"Consultas en Proceso", @"enabled": @1, @"suboptions": @[]}];
    return [NSMutableArray arrayWithArray:@[dicEstatus, dicGestion, dicCertificados, dicAcuerdos, dicCreditos, dicConsultas]];
}
+(UIColor *)getColorYellow{
    return [UIColor colorWithRed:1.0 green:206.0/255.0 blue:38.0/255.0 alpha:1.0];
}
+(UIColor *)getColorBlue{
    return [UIColor colorWithRed:38.0/255.0 green:36.0/255.0 blue:63.0/255.0 alpha:1.0];
}
+(NSString *)convertDateToString:(NSDate *)date{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    return [dateFormatter stringFromDate:date];
}
+(void)showAlertaErrorConnectionInViewCont:(UIViewController *)viewCont{
    [self showAlertaWithMessage:@"No tiene conexión a Internet" inViewCont:viewCont];
}
+(void)showAlertaErrorInViewCont:(UIViewController *)viewCont{
    [self showAlertaWithMessage:@"Hubo un error." inViewCont:viewCont];
}
+(void)showNoDataInViewCont:(UIViewController *)viewCont{
    [self showAlertViewContWithTitle:@"Atención" withMessage:@"No hay datos disponibles." inViewCont:viewCont withOkHandler:nil];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un metodo para crear una alerta y establecer un metodo cuando se presione el boton ok | INICIO
+(void)showNoDataInViewCont:(UIViewController *)viewCont withOkHandler:(void (^)(UIAlertAction *))handler{
    [self showAlertViewContWithTitle:@"Atención" withMessage:@"No hay datos disponibles." inViewCont:viewCont withOkHandler:handler];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un metodo para crear una alerta y establecer un metodo cuando se presione el boton ok | FIN
+(void)showAlertaTimeOutInViewCont:(UIViewController *)viewCont{
    [self showAlertaWithMessage:@"La consulta se demoró más de lo esperado. Intentelo más tarde." inViewCont:viewCont];
}
+(void)showAlertaWithMessage:(NSString *)message inViewCont:(UIViewController *)viewCont{
    [self showAlertViewContWithTitle:@"Alerta" withMessage:message inViewCont:viewCont withOkHandler:nil];
}
+(void)showAlertViewContWithTitle:(NSString *)title withMessage:(NSString *)message inViewCont:(UIViewController *)viewCont withOkHandler:(void (^)(UIAlertAction *action))handler{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:handler]];
    [viewCont presentViewController:alertCont animated:true completion:nil];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un metodo para crear una alerta que muestre el mensaje dependiendo del error | INICIO
+(void)showAlertErrorWithError:(NSError *)error withOkHandler:(void (^)(UIAlertAction *))handler inViewCont:(UIViewController *)viewCont{
    NSString *strMessage = @"Hubo un error.";
    if([ToolBox isErrorConnectionInternet:error.code]){
        strMessage = @"No tiene conexión a Internet";
    }else if(error.code == NSURLErrorTimedOut){
        strMessage = @"La consulta se demoró más de lo esperado. Intentelo más tarde.";
    }
    [self showAlertViewContWithTitle:@"Alerta" withMessage:strMessage inViewCont:viewCont withOkHandler:handler];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un metodo para crear una alerta que muestre el mensaje dependiendo del error | FIN
+(NSString *)getddMMyyyyForStringDate2:(NSString *)strDate{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
    dateFormatter.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDate *dateTemp = [dateFormatter dateFromString:strDate];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    return [dateFormatter stringFromDate:dateTemp];
}
+(NSString *)getHHMMForStringDate2:(NSString *)strDate{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";
    dateFormatter.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDate *dateTemp = [dateFormatter dateFromString:strDate];
    dateFormatter.dateFormat = @"HH:mm";
    return [dateFormatter stringFromDate:dateTemp];
}
+(NSString *)getddMMyyyyForStringDate3:(NSString *)strDate{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss.S";
    dateFormatter.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDate *dateTemp = [dateFormatter dateFromString:strDate];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    return [dateFormatter stringFromDate:dateTemp];
}
+(NSString *)getHHMMForStringDate:(NSString *)strDate{
    return [self getFormatString:@"HH:mm" forStringDate:strDate];
}
+(NSString *)getddMMyyyyForStringDate:(NSString *)strDate{
    return [self getFormatString:@"dd/MM/yyyy" forStringDate:strDate];
}
+(NSString *)getFormatString:(NSString *)format forStringDate:(NSString *)strDate{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    NSDate *dateTemp = [dateFormatter dateFromString:strDate];
    dateFormatter.dateFormat = format;
    return [dateFormatter stringFromDate:dateTemp];
}
+(NSArray *)getArrEstadosAcuerdosComerciales{
    return @[@{@"k": @"", @"v": @"TODOS"}, @{@"k": @"ACT", @"v": @"ACTIVO"}, @{ @"k": @"PEN", @"v": @"PENDIENTE"}, @{ @"k": @"SUS", @"v": @"SUSPENDIDO"}, @{ @"k": @"TER", @"v": @"TERMINADO"}];
}
+(NSArray *)getArrMetodosFacturacionAcuerdosComerciales{
    return @[@{@"k": @"", @"v": @"TODOS"}, @{@"k": @"CON", @"v": @"CONTADO"}, @{ @"k": @"COI", @"v": @"CONTADO INMEDIATO"}, @{ @"k": @"CRE", @"v": @"CREDITO"}, @{ @"k": @"VAL", @"v": @"VALORIZACION"}];
}
+(NSArray *)getArrTipoResultadosCertificados{
    return @[@{@"k": @"", @"v": @"TODOS"}, @{@"k": @"CER", @"v": @"CERTIFICADO GENERADO"}, @{ @"k": @"PRO", @"v": @"CERTIFICADO EN PROCESO"}, @{ @"k": @"COM", @"v": @"CARTA GENERADA"}];
}
+(NSString *)getStringFormNumberFormatterWithNumber:(NSNumber *)number{
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
    numberFormatter.minimumFractionDigits = 2;
    numberFormatter.maximumFractionDigits = 2;
    numberFormatter.perMillSymbol = @",";
    return [numberFormatter stringFromNumber:number];
}
+(BOOL)isErrorConnectionInternet:(NSInteger)errorCode{
    if((errorCode == NSURLErrorCannotFindHost) || (errorCode == NSURLErrorCannotConnectToHost) || (errorCode == NSURLErrorNetworkConnectionLost) || (errorCode == NSURLErrorDNSLookupFailed) || (errorCode == NSURLErrorHTTPTooManyRedirects) || (errorCode == NSURLErrorNotConnectedToInternet) || (errorCode == NSURLErrorSecureConnectionFailed) || (errorCode == NSURLErrorCannotLoadFromNetwork)){
        return true;
    }else{
        return false;
    }
}
@end
