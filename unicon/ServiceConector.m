//
//  ServiceConector.m
//  SieWeb
//
//  Created by DSB Mobile on 5/8/15.
//  Copyright (c) 2015 DSBMobile. All rights reserved.
//
#import "ServiceConector.h"
@implementation ServiceConector
+(void)connectToUrl:(NSString *)urlString withParameters:(id)parameters withResponse:(void (^)(id receivedData, NSError *error))responseHandler{
    NSError *reqError = nil;
    NSMutableURLRequest *req = [self createRequestWithUrl:urlString andParams:parameters error:&reqError];
    if(reqError){
        responseHandler(nil,reqError);
    }else{
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
            responseHandler(data,connectionError);
        }];
    }
}
+(void)connectNormalToUrl:(NSString *)urlString withParameters:(id)parameters withResponse:(void (^)(id receivedData, NSError *error))responseHandler{
    NSError *reqError = nil;
    NSMutableURLRequest *req = [self createNormalRequestWithUrl:urlString andParams:parameters error:&reqError];
    if(reqError){
        responseHandler(nil,reqError);
    }else{
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError){
            responseHandler(data,connectionError);
        }];
    }
}
+(NSURLConnection *)createConnectionWithDelegate:(id<NSURLConnectionDataDelegate,NSURLConnectionDelegate>)delegate withUrl:(NSString *)strUrl andParams:(id)params{
    NSError *reqError;
    NSMutableURLRequest *req = [self createRequestWithUrl:strUrl andParams:params error:&reqError];
    if(reqError){
        NSLog(@"error %s: %@", __FUNCTION__, reqError);
        return nil;
    }else{
        return [NSURLConnection connectionWithRequest:req delegate:delegate];
    }
}
+(NSMutableURLRequest *)createRequestWithUrl:(NSString *)strUrl andParams:(id)params error:(NSError **)reqError{
    NSData *dataBody = nil;
    if(params){
        if([params isKindOfClass:[NSString class]]){
            dataBody = [params dataUsingEncoding:NSUTF8StringEncoding];
        }else{
            NSError *jsonError = nil;
            dataBody = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError];
            if(jsonError){
                *reqError = jsonError;
                return nil;
            }
        }
    }
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [req addValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [req setHTTPMethod:@"POST"];
    if(dataBody){
        [req setHTTPBody:dataBody];
    }
    return req;
}
+(NSMutableURLRequest *)createNormalRequestWithUrl:(NSString *)strUrl andParams:(id)params error:(NSError **)reqError{
    NSData *dataBody = nil;
    if(params){
        if([params isKindOfClass:[NSString class]]){
            dataBody = [params dataUsingEncoding:NSUTF8StringEncoding];
        }else{
            NSError *jsonError = nil;
            dataBody = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError];
            if(jsonError){
                *reqError = jsonError;
                return nil;
            }
        }
    }
    NSURL *url = [NSURL URLWithString:strUrl];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    if(dataBody){
        [req setHTTPBody:dataBody];
    }
    return req;
}
+(void)internetConnectionStatus:(void (^)(BOOL connected))internetStatus{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if(data.length > 0 && connectionError == nil){
            internetStatus(YES);
        }else {
            internetStatus(NO);
        }
    }];
}
@end