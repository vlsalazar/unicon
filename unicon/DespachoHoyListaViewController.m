//
//  DespachoHoyListaViewController.m
//  unicon
//
//  Created by victor salazar on 22/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "DespachoHoyListaViewController.h"
#import "DespachoHoyListaDespachoCSTableViewCell.h"
#import "FragmentedView.h"
@implementation DespachoHoyListaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrResult = @[];
    originalHeightLeyendaView = self.heightLeyendaView.constant;
    UsuarioActual *currentUser = [UsuarioActual currentUser];
    NSString *codigoCliente = @"";
    if(currentUser.empresaSeleccionada){
        codigoCliente = currentUser.empresaSeleccionada.codigoCliente;
    }else{
        codigoCliente = currentUser.codigoCliente;
    }
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormat.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    
    NSDate *dOrderDate = [dateFormat dateFromString:[self.dicPedido objectForKey:@"orderDate"]];
    dateFormat.dateFormat = @"dd/MM/yyyy";
    NSString *sOrderDate = [dateFormat stringFromDate:dOrderDate];
    dicParams = @{@"desde": sOrderDate, @"hasta": sOrderDate, @"pedido": [self.dicPedido objectForKey:@"orderCode"], @"codigoCliente": codigoCliente, @"codigoUsuario": currentUser.usuario, @"tipoConsulta": @"1"};
    self.nomObraLbl.text = [self.dicPedido objectForKey:@"descObr"];
    obraRefCtrl = [[UIRefreshControl alloc] init];
    [obraRefCtrl addTarget:self action:@selector(actionRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.despachosObraHoyTableView addSubview:obraRefCtrl];
    [self requestList];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexpathSelected = [self.despachosObraHoyTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:self.nomObraLbl.text forKey:@"strTitle"];
    [segue.destinationViewController setValue:[arrResult objectAtIndex:indexpathSelected.row] forKey:@"dicDespacho"];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceChangedOrientation) name:UIDeviceOrientationDidChangeNotification object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [currentCon cancel];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
}
-(void)actionRefresh:(id)sender{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
    [self requestList];
}
-(IBAction)actionLeyenda:(id)sender{
    float newHeight;
    if(self.heightLeyendaView.constant == originalHeightLeyendaView){
        newHeight = 0;
    }else{
        newHeight = originalHeightLeyendaView;
    }
    self.heightLeyendaView.constant = newHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)requestList{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlDespachoHoyLista] andParams:dicParams];
    [currentCon start];
    currentData = [NSMutableData data];
}
#pragma mark - NSURLConnection
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    if([ToolBox isErrorConnectionInternet:error.code]){
        [ToolBox showAlertaErrorConnectionInViewCont:self];
    }else{
        if(error.code == NSURLErrorTimedOut){
            [ToolBox showAlertaTimeOutInViewCont:self];
        }else{
            [ToolBox showAlertaErrorInViewCont:self];
        }
    }
    if(obraRefCtrl.isRefreshing){
        [obraRefCtrl endRefreshing];
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [currentData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [currentData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    if (obraRefCtrl.isRefreshing) {
        [obraRefCtrl endRefreshing];
    }
    NSError *jsonError;
    id result = [NSJSONSerialization JSONObjectWithData:currentData options:NSJSONReadingAllowFragments error:&jsonError];
    if(jsonError){
        [ToolBox showAlertaErrorInViewCont:self];
    }else{
        id despachosCSBean = [result objectForKey:@"despachosCSBean"];
        if([despachosCSBean isKindOfClass:[NSDictionary class]]){
            arrResult = @[despachosCSBean];
        }else{
            arrResult = despachosCSBean;
        }
        [self.despachosObraHoyTableView reloadData];
    }
    [self performSelector:@selector(startRefreshing) withObject:nil afterDelay:20];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrResult.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DespachoHoyListaDespachoCSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"despachoCSCell" forIndexPath:indexPath];
    NSDictionary *dicItem = [arrResult objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    id camion = [dicItem objectForKey:@"truckCode"];
    id guia = [dicItem objectForKey:@"guia"];
    id diseno = [dicItem objectForKey:@"shortProdDescr"];
    id plantCode = [dicItem objectForKey:@"plantCode"];
    id toJobTime = [dicItem objectForKey:@"toJobTime"];
    id onJobTime = [dicItem objectForKey:@"onJobTime"];
    id nuEtapa = [dicItem objectForKey:@"nuEtapa"];
    id plantName = [dicItem objectForKey:@"plantName"];
    id usageDescr = [dicItem objectForKey:@"usageDescr"];
    id teo = [dicItem objectForKey:@"teo"];
    double dTeo = [teo doubleValue];
    int etapa = [nuEtapa intValue];
    if(![toJobTime isEqual:[NSNull null]]){
        if([toJobTime length] == 25){
            toJobTime = [ToolBox getHHMMForStringDate:toJobTime];
        }else{
            toJobTime = [ToolBox getHHMMForStringDate2:toJobTime];
        }
    }else{
        toJobTime = @"";
    }
    if(![onJobTime isEqual:[NSNull null]]){
        if([onJobTime length] == 25){
            onJobTime = [ToolBox getHHMMForStringDate:onJobTime];
        }else{
            onJobTime = [ToolBox getHHMMForStringDate2:onJobTime];
        }
    }else{
        onJobTime = @"";
    }
    NSString *plantZ = @"";
    if(plantCode){
        plantCode = [plantCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if(![plantCode isEqualToString:@"ZZL"]){
            plantZ = [plantCode substringToIndex:1];
        }
    }
    BOOL isPlantZ = false;
    if([plantZ isEqualToString:@"Z"]){
        isPlantZ = true;
    }
    NSArray *arrColors = @[[UIColor clearColor], colorProgramado, colorTransito, colorEspObra, colorIniVaciado, colorFinVaciado];
    if(isPlantZ){
        etapa = 0;
    }
    cell.camionLbl.text = ([camion isEqual:[NSNull null]])?@"":[camion stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    cell.disenioLbl.text = ([diseno isEqual:[NSNull null]])?@"":[diseno stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    cell.volumentLbl.text = [dicItem objectForKey:@"delvQty"];
    cell.salObraLbl.text = isPlantZ?@"":toJobTime;
    cell.llegObraLbl.text = isPlantZ?@"":onJobTime;
    cell.tipoView.backgroundColor = [arrColors objectAtIndex:etapa];
    cell.guiaLbl.text = ([guia isEqual:[NSNull null]])?@"":[guia stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    cell.plantaLbl.text = ([plantName isEqual:[NSNull null]])?@"":[plantName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    cell.elementoLbl.text = [usageDescr isEqual:[NSNull null]]?@"":[usageDescr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    cell.teoLbl.text = (!isPlantZ && (dTeo != 0.0))?[teo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]:@"";
    return cell;
}
-(void)deviceChangedOrientation{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [self.despachosObraHoyTableView reloadData];
    }
}
#pragma mark - Auxiliar
-(void)startRefreshing{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startRefreshing) object:nil];
    [obraRefCtrl beginRefreshing];
    [self actionRefresh:obraRefCtrl];
    [self.despachosObraHoyTableView setContentOffset:CGPointMake(0, -obraRefCtrl.frame.size.height) animated:true];
}
@end
