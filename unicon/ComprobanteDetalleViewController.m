//
//  ComprobanteDetalleViewController.m
//  unicon
//
//  Created by victor salazar on 9/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ComprobanteDetalleViewController.h"
@implementation ComprobanteDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.secuenciaLbl.text = [self.dicComprobante objectForKey:@"secuenciaDocumento"];
    self.numCompLbl.text = [NSString stringWithFormat:@"%@-%@", [self.dicComprobante objectForKey:@"serieDocumento"], [self.dicComprobante objectForKey:@"numeroCorrelativoDocumento"]];
    self.fechaLbl.text = [ToolBox getddMMyyyyForStringDate3:[self.dicComprobante objectForKey:@"fechaEmision"]];
    self.porcPerLbl.text = [NSString stringWithFormat:@"%@%c", [self.dicComprobante objectForKey:@"porcentajePercepcion"], '%'];
    self.montoLbl.text = [ToolBox getStringFormNumberFormatterWithNumber:[NSNumber numberWithDouble:[[self.dicComprobante objectForKey:@"montoPercepcionSoles"] doubleValue]]];
}
-(IBAction)actionExport:(id)sender{
    NSDictionary *dicParams = @{@"documentos": @[@{@"secuenciaDocumento": [self.dicComprobante objectForKey:@"secuenciaDocumento"]}]};
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    [ServiceConector connectToUrl:[ServiceUrl getUrlComprobanteRecepcionPDF] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        if(error){
            if([ToolBox isErrorConnectionInternet:error.code]){
                [ToolBox showAlertaErrorConnectionInViewCont:self];
            }else{
                if(error.code == NSURLErrorTimedOut){
                    [ToolBox showAlertaTimeOutInViewCont:self];
                }else{
                    [ToolBox showAlertaErrorInViewCont:self];
                }
            }
        }else{
            NSData *currentData = receivedData;
            if(currentData.length == 0){
                [ToolBox showNoDataInViewCont:self];
            }else{
                NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *strDocumentDirectory = arr.firstObject;
                NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                [currentData writeToFile:filePath atomically:true];
                UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                docIntCont.name = @"";
                docIntCont.delegate = self;
                [docIntCont presentPreviewAnimated:true];
            }
        }
    }];
}
#pragma mark - UIDocumentInteractionCont
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
@end