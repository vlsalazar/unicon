//
//  Usuario+CoreDataClass.m
//  unicon
//
//  Created by victor salazar on 3/21/17.
//  Copyright © 2017 Victor Salazar. All rights reserved.
//
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | INICIO
#import "Usuario+CoreDataClass.h"
#import "AppDelegate.h"
@implementation Usuario
+(Usuario * _Nullable)obtenerUsuarioActual{
    NSManagedObjectContext *moc = [AppDelegate getMOC];
    NSFetchRequest *fetchReq = [Usuario fetchRequest];
    NSError *error;
    NSArray *arrUsuarios = [moc executeFetchRequest:fetchReq error:&error];
    if(error == nil){
        return arrUsuarios.firstObject;
    }else{
        NSLog(@"%s, error: %@", __FUNCTION__, error.description);
        return nil;
    }
}
+(void)guardarUsuarioActual:(UsuarioActual *)usuarioActual{
    NSManagedObjectContext *moc = [AppDelegate getMOC];
    Usuario *nuevoUsuario = [NSEntityDescription insertNewObjectForEntityForName:@"Usuario" inManagedObjectContext:moc];
    nuevoUsuario.correo = usuarioActual.correo;
    nuevoUsuario.nombre = usuarioActual.nombre;
    nuevoUsuario.usuario = usuarioActual.usuario;
    nuevoUsuario.codigoUsuario = usuarioActual.codigoUsuario;
    nuevoUsuario.codigoCliente = usuarioActual.codigoCliente;
    nuevoUsuario.descripcionCliente = usuarioActual.descripcionCliente;
    NSArray *arrMenu = usuarioActual.arrMenu;
    NSDictionary *dicEstatus = [arrMenu objectAtIndex:0];
    NSDictionary *dicGestion = [arrMenu objectAtIndex:1];
    NSDictionary *dicCertificados = [arrMenu objectAtIndex:2];
    NSDictionary *dicAcuerdos = [arrMenu objectAtIndex:3];
    NSDictionary *dicCreditos = [arrMenu objectAtIndex:4];
    NSDictionary *dicConsultas = [arrMenu objectAtIndex:5];
    nuevoUsuario.estatusHabilitado = [[dicEstatus objectForKey:@"enabled"] boolValue];
    nuevoUsuario.gestionHabilitado = [[dicGestion objectForKey:@"enabled"] boolValue];
    nuevoUsuario.certificadosHabilitado = [[dicCertificados objectForKey:@"enabled"] boolValue];
    nuevoUsuario.acuerdosHabilitado = [[dicAcuerdos objectForKey:@"enabled"] boolValue];
    nuevoUsuario.creditosHabilitado = [[dicCreditos objectForKey:@"enabled"] boolValue];
    nuevoUsuario.consultasHabilitado = [[dicConsultas objectForKey:@"enabled"] boolValue];
    NSError *error;
    [moc save:&error];
    NSLog(@"%s error: %@", __FUNCTION__, error.description);
}
+(void)eliminarUsuario{
    Usuario *usuarioActual = [self obtenerUsuarioActual];
    if(usuarioActual != nil){
        NSManagedObjectContext *moc = [AppDelegate getMOC];
        [moc deleteObject:usuarioActual];
        [moc save:nil];
    }
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se creo un objecto para almacenar la información del usuario | FIN
@end
