//
//  AcuerComerViewController.m
//  unicon
//
//  Created by DSB Mobile on 25/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "AcuerComerViewController.h"
#import "AcuerComerDetTableViewCell.h"
@implementation AcuerComerViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    arrItemsSelected = [NSMutableArray array];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem *menuBarBtnItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"info"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(actionLeyenda:)];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.parentViewController.navigationItem.rightBarButtonItem = menuBarBtnItem;
    }else{
        self.navigationItem.rightBarButtonItem = menuBarBtnItem;
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.acuerComerTableView indexPathForSelectedRow];
    [segue.destinationViewController setValue:[self.arrAcuerComers objectAtIndex:indexPath.row] forKey:@"dicAcuerComer"];
}
#pragma mark - IBAction
-(void)actionLeyenda:(id)sender{
    float newHeight;
    if(self.heightLeyendaView.constant == 0){
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            newHeight = 72;
        }else{
            newHeight = (self.navigationController.view.frame.size.width > self.navigationController.view.frame.size.height)?72:112;
        }
    }else{
        newHeight = 0;
    }
    self.heightLeyendaView.constant = newHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(IBAction)actionSwitch:(UISwitch *)sender{
    if(sender.on){
        [arrItemsSelected addObject:[NSNumber numberWithInteger:sender.tag]];
    }else{
        for(NSNumber *index in arrItemsSelected){
            if(index.intValue == sender.tag){
                [arrItemsSelected removeObject:index];
                break;
            }
        }
    }
}
-(IBAction)actionExport:(id)sender{
    if(arrItemsSelected.count == 0){
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Atención" message:@"Debe seleccionar una guía para poder exportar" preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alertCont animated:true completion:nil];
    }else{
        [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
        NSMutableArray *arrCodigos = [NSMutableArray array];
        for(NSNumber *index in arrItemsSelected){
            NSDictionary *dicAcuerdoComercial = [self.arrAcuerComers objectAtIndex:index.integerValue];
            [arrCodigos addObject:[dicAcuerdoComercial objectForKey:@"codigo"]];
        }
        NSDictionary *dicParams = @{@"acuerdos": arrCodigos};
        [ServiceConector connectToUrl:[ServiceUrl getUrlAcuerdosComercialesPDF] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
            [SVProgressHUD dismiss];
            if(error){
                if([ToolBox isErrorConnectionInternet:error.code]){
                    [ToolBox showAlertaErrorConnectionInViewCont:self];
                }else{
                    if(error.code == NSURLErrorTimedOut){
                        [ToolBox showAlertaTimeOutInViewCont:self];
                    }else{
                        [ToolBox showAlertaErrorInViewCont:self];
                    }
                }
            }else{
                if([receivedData length] == 0){
                    [ToolBox showNoDataInViewCont:self];
                }else{
                    NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *strDocumentDirectory = arr.firstObject;
                    NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                    [receivedData writeToFile:filePath atomically:true];
                    UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                    docIntCont.name = @"";
                    docIntCont.delegate = self;
                    [docIntCont presentPreviewAnimated:true];
                }
            }
        }];
    }
}
#pragma mark - Auxiliar
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrAcuerComers.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AcuerComerDetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *dicAcuerdoComercial = [self.arrAcuerComers objectAtIndex:indexPath.row];
    NSString *strFechaInicioAcuerdo = [[dicAcuerdoComercial objectForKey:@"fechaInicioAcuerdo"] substringToIndex:10];
    NSArray *arrTemp = [[[strFechaInicioAcuerdo componentsSeparatedByString:@"-"] reverseObjectEnumerator] allObjects];
    cell.fechaAcuerLbl.text = [arrTemp componentsJoinedByString:@"/"];
    cell.numAcuerLbl.text = [dicAcuerdoComercial objectForKey:@"codigo"];
    cell.obraLbl.text = [[dicAcuerdoComercial objectForKey:@"obra"] objectForKey:@"descripcion"];
    cell.view1.backgroundColor = NaranjaColor;
    cell.view2.backgroundColor = NaranjaColor;
    cell.view3.backgroundColor = NaranjaColor;
    cell.view4.backgroundColor = NaranjaColor;
    cell.contentView.backgroundColor = (indexPath.row%2 == 0)?ParColor:ImparColor;
    id documentos = [dicAcuerdoComercial objectForKey:@"documentos"];
    if([documentos isKindOfClass:[NSDictionary class]]){
        documentos = @[documentos];
    }
    for(NSDictionary *dicDocumento in documentos){
        NSString *strTipoDoc = [dicDocumento objectForKey:@"tipodocumento"];
        if([strTipoDoc isEqualToString:@"COT"]){
            cell.view1.backgroundColor = VerdeColor;
        }else if ([strTipoDoc isEqualToString:@"CON"]){
            cell.view2.backgroundColor = VerdeColor;
        }else if ([strTipoDoc isEqualToString:@"OCO"]){
            cell.view3.backgroundColor = VerdeColor;
        }else if ([strTipoDoc isEqualToString:@"CGV"]){
            cell.view4.backgroundColor = VerdeColor;
        }
    }
    cell.selectedSwitch.on = false;
    cell.selectedSwitch.tag = indexPath.row;
    for(NSNumber *index in arrItemsSelected){
        if(index.intValue == cell.selectedSwitch.tag){
            cell.selectedSwitch.on = true;
        }
    }
    return cell;
}
@end