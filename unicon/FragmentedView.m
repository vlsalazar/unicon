//
//  FragmentedView.m
//  unicon
//
//  Created by DSB Mobile on 17/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "FragmentedView.h"
@implementation FragmentedView
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    if(arrViews){
        for(UIView *view in arrViews){
            [view removeFromSuperview];
        }
        [arrViews removeAllObjects];
    }else{
        arrViews = [NSMutableArray array];
    }
    float initialX = 0;
    float porcentages[] = {self.porcFinVaciado, self.porcIniVaciado, self.porcEspObra, self.porcTransito, self.porcProgramado};
    NSArray *arrColors = @[colorFinVaciado, colorIniVaciado, colorEspObra, colorTransito, colorProgramado];
    for(int i = 0; i < 5; i++){
        if(porcentages[i] > 0.0){
            UIView *viewFinVac = [[UIView alloc] initWithFrame:CGRectMake(initialX, 0, porcentages[i]*self.frame.size.width, self.frame.size.height)];
            viewFinVac.backgroundColor = [arrColors objectAtIndex:i];
            [self addSubview:viewFinVac];
            initialX += viewFinVac.frame.size.width;
            [arrViews addObject:viewFinVac];
        }
    }
}
@end