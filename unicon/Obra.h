//
//  Obra.h
//  unicon
//
//  Created by DSB Mobile on 4/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
@interface Obra:NSObject
@property(nonatomic,strong) NSString *codigo;
@property(nonatomic,strong) NSString *descripcion;
@end