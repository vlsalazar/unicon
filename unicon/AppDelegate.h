//
//  AppDelegate.h
//  unicon
//
//  Created by DSB Mobile on 27/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@interface AppDelegate:UIResponder<UIApplicationDelegate>
@property(nonatomic, strong) UIWindow *window;
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se agrego variables para el manejo de la BD y metodos para el acceso de la BD | INICIO
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
-(void)saveContext;
+(NSManagedObjectContext *)getMOC;
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se agrego variables para el manejo de la BD y metodos para el acceso de la BD | FIN
@end
