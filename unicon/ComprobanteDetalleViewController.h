//
//  ComprobanteDetalleViewController.h
//  unicon
//
//  Created by victor salazar on 9/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface ComprobanteDetalleViewController:UIViewController<UIDocumentInteractionControllerDelegate>
@property(nonatomic,weak) NSDictionary *dicComprobante;
@property(nonatomic,weak) IBOutlet UILabel *secuenciaLbl;
@property(nonatomic,weak) IBOutlet UILabel *numCompLbl;
@property(nonatomic,weak) IBOutlet UILabel *fechaLbl;
@property(nonatomic,weak) IBOutlet UILabel *porcPerLbl;
@property(nonatomic,weak) IBOutlet UILabel *montoLbl;
@end