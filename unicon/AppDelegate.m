//
//  AppDelegate.m
//  unicon
//
//  Created by DSB Mobile on 27/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "AppDelegate.h"
#import "Usuario+CoreDataClass.h"
#import <GoogleMaps/GoogleMaps.h>
@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
#pragma mark - Application
-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    [GMSServices provideAPIKey:@"AIzaSyDWanYfW-4eSMI5Vg6yML5QUnQUn8liWq8"];
    NSString *strBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:baseURLKey];
    if(!strBaseURL){
        [[NSUserDefaults standardUserDefaults] setObject:@"https://si.unicon.com.pe" forKey:baseURLKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Se valida si existe usuario actual para cargar la pantalla del menu principal | INICIO
    Usuario *usuarioActual = [Usuario obtenerUsuarioActual];
    if(usuarioActual != nil){
        [UsuarioActual cargarUsuarioActualInfo:usuarioActual];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewCont = [storyboard instantiateViewControllerWithIdentifier:@"userViewCont"];
        self.window.rootViewController = viewCont;
        [self.window makeKeyAndVisible];
    }
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Se valida si existe usuario actual para cargar la pantalla del menu principal | FIN
    return YES;
}
-(void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
-(void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
-(void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
-(void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
-(void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se implementaron de los metodos para el manejo de la BD | INICIO
#pragma mark - Core Data
+(NSManagedObjectContext *)getMOC{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.managedObjectContext;
}
-(NSManagedObjectContext *)managedObjectContext{
    if(_managedObjectContext != nil){
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if(coordinator != nil){
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}
-(NSManagedObjectModel *)managedObjectModel{
    if(_managedObjectModel != nil){
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"unicon" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}
-(NSPersistentStoreCoordinator *)persistentStoreCoordinator{
    if(_persistentStoreCoordinator != nil){
        return _persistentStoreCoordinator;
    }
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"unicon.sqlite"];
    NSError *error = nil;
    NSDictionary *dicOptions = @{NSMigratePersistentStoresAutomaticallyOption : [NSNumber numberWithBool:true]};
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:dicOptions error:&error]) {
        NSLog(@"Unresolved error %@", error);
        abort();
    }
    return _persistentStoreCoordinator;
}
-(void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if(managedObjectContext != nil){
        if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]){
            NSLog(@"Unresolved error %@", error);
            abort();
        }
    }
}
#pragma mark - Application's Documents directory
-(NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se implementaron de los metodos para el manejo de la BD | FIN
@end
