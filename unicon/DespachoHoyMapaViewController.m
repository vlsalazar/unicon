//
//  DespachoHoyMapaViewController.m
//  unicon
//
//  Created by victor salazar on 18/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "DespachoHoyMapaViewController.h"
@implementation DespachoHoyMapaViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    self.mapView.delegate = self;
    arrMarkers = [NSMutableArray array];
    didFirstLoad = false;
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    dateFormat.calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    dateFormat.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    dateFormat.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDate *dOrderDate = [dateFormat dateFromString:[self.dicPedido objectForKey:@"orderDate"]];
    dateFormat.dateFormat = @"dd/MM/yyyy";
    dicParams = @{@"fechaPedido": [dateFormat stringFromDate:dOrderDate], @"pedido": [self.dicPedido objectForKey:@"orderCode"]};
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(-12.0550735, -77.026335) zoom:9]];
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    [self requestMapa];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    [currentCon cancel];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(requestMapa) object:nil];
}
-(void)requestMapa{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    currentCon = [ServiceConector createConnectionWithDelegate:self withUrl:[ServiceUrl getUrlDespachoHoyMapa] andParams:dicParams];
    currentData = [NSMutableData data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    [SVProgressHUD dismiss];
    if([ToolBox isErrorConnectionInternet:error.code]){
        [ToolBox showAlertaErrorConnectionInViewCont:self];
    }else{
        if(error.code == NSURLErrorTimedOut){
            [ToolBox showAlertaTimeOutInViewCont:self];
        }else{
            [ToolBox showAlertaErrorInViewCont:self];
        }
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [currentData setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [currentData appendData:data];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [SVProgressHUD dismiss];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    NSError *jsonError;
    NSDictionary *dicResultTemp = [NSJSONSerialization JSONObjectWithData:currentData options:NSJSONReadingAllowFragments error:&jsonError];
    if(jsonError){
        [ToolBox showAlertaErrorInViewCont:self];
    }else{
        if([dicResultTemp isEqual:[NSNull null]]){
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:@"Alerta" message:@"No se pudo obtener información. ¿Desea intentarlo nuevamente?" preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"SI" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self requestMapa];
            }]];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertCont animated:true completion:nil];
        }else{
            id truckLocationBean = [dicResultTemp objectForKey:@"truckLocationBean"];
            if([truckLocationBean isKindOfClass:[NSDictionary class]]){
                arrResult = @[truckLocationBean];
            }else{
                arrResult = truckLocationBean;
            }
            if(arrResult.count > 0){
                for(GMSMarker *marker in arrMarkers){
                    marker.map = nil;
                }
                [arrMarkers removeAllObjects];
                [self.mapView clear];
            }
            for(NSDictionary *dicTruck in arrResult){
                NSString *tipo = [dicTruck objectForKey:@"tipo"];
                NSString *assetName = @"";
                if([dicTruck objectForKey:@"assetName"]){
                    assetName = [dicTruck objectForKey:@"assetName"];
                }
                if(!([tipo isEqualToString:@"truc"] && (assetName.length == 0))){
                    id longitud = [dicTruck objectForKey:@"longitud"];
                    id latitud = [dicTruck objectForKey:@"latutud"];
                    if([longitud isKindOfClass:[NSString class]] && [latitud isKindOfClass:[NSString class]]){
                        float lon = [longitud floatValue];
                        float lat = [latitud floatValue];
                        if(lon < 0 && lat < 0){
                            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lon, lat);
                            GMSMarker *markerTemp = [GMSMarker markerWithPosition:coor];
                            markerTemp.icon = [UIImage imageNamed:([tipo isEqualToString:@"truc"]?@"mapLeyVeh":([tipo isEqualToString:@"plnt"]?@"mapLeyPlanta":@"mapLeyObra"))];
                            markerTemp.title = [tipo isEqualToString:@"truc"]?[NSString stringWithFormat:@"Unidad: %@ - %@ - TEO(min): %@", assetName, [dicTruck objectForKey:@"deEtapa"], [dicTruck objectForKey:@"teo"]]:([tipo isEqualToString:@"plnt"]?[dicTruck objectForKey:@"plantName"]:[dicTruck objectForKey:@"deCamion"]);
                            markerTemp.snippet = [tipo isEqualToString:@"truc"]?[NSString stringWithFormat:@"%@ - %@", [dicTruck objectForKey:@"deCamion"], [dicTruck objectForKey:@"plantName"]]:([tipo isEqualToString:@"plnt"]?[dicTruck objectForKey:@"dirPta"]:[dicTruck objectForKey:@"dirObr"]);
                            markerTemp.map = self.mapView;
                            [arrMarkers addObject:markerTemp];
                        }
                    }
                }
            }
            if(arrMarkers.count > 0 && !didFirstLoad){
                didFirstLoad = true;
                GMSMarker *markerTemp = [arrMarkers firstObject];
                [self.mapView animateWithCameraUpdate:[GMSCameraUpdate setTarget:markerTemp.position zoom:13]];
            }
            [self performSelector:@selector(requestMapa) withObject:nil afterDelay:5];
        }
    }
}
-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    float maxWidth = 270;
    NSString *strTitle = marker.title;
    NSString *strSnippet = marker.snippet;
    UIFont *titleFont = [UIFont boldSystemFontOfSize:17];
    UIFont *snippetFont = [UIFont systemFontOfSize:15];
    CGRect rectTitle = [strTitle boundingRectWithSize:CGSizeMake(maxWidth, 1000) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: titleFont} context:nil];
    float finalHeightTitle = ceilf(rectTitle.size.height);
    CGRect rectSnippet = [strSnippet boundingRectWithSize:CGSizeMake(maxWidth, 1000) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: snippetFont} context:nil];
    float finalHeightSnippet = ceilf(rectSnippet.size.height);
    float finalWidth = MAX(rectTitle.size.width, rectSnippet.size.width);
    UIView *finalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, finalWidth + 10, finalHeightTitle + finalHeightSnippet + 20)];
    finalView.backgroundColor = [UIColor clearColor];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, finalWidth + 10, finalHeightTitle + finalHeightSnippet + 10)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, finalWidth, finalHeightTitle)];
    [titleLbl setText:strTitle];
    titleLbl.font = titleFont;
    titleLbl.numberOfLines = 0;
    [view addSubview:titleLbl];
    UILabel *snippetLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, finalHeightTitle + 5, finalWidth, finalHeightSnippet)];
    snippetLbl.text = strSnippet;
    snippetLbl.font = snippetFont;
    snippetLbl.numberOfLines = 0;
    [view addSubview:snippetLbl];
    [finalView addSubview:view];
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(finalView.frame.size.width / 2 - 9, view.frame.size.height)];
    [bezierPath addLineToPoint:CGPointMake(finalView.frame.size.width / 2 + 9, view.frame.size.height)];
    [bezierPath addLineToPoint:CGPointMake(finalView.frame.size.width /2, view.frame.size.height + 10)];
    [bezierPath addLineToPoint:CGPointMake(finalView.frame.size.width / 2 - 9, view.frame.size.height)];
    CAShapeLayer *triangle = [CAShapeLayer layer];
    triangle.backgroundColor = [[UIColor clearColor] CGColor];
    triangle.path = bezierPath.CGPath;
    triangle.fillColor = [[UIColor whiteColor] CGColor];
    [finalView.layer addSublayer:triangle];
    return finalView;
}
@end