//
//  DespachoHoyListaDespachoCSTableViewCell.h
//  unicon
//
//  Created by DSB Mobile on 22/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface DespachoHoyListaDespachoCSTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *guiaLbl;
@property(nonatomic,weak) IBOutlet UILabel *plantaLbl;
@property(nonatomic,weak) IBOutlet UILabel *camionLbl;
@property(nonatomic,weak) IBOutlet UILabel *disenioLbl;
@property(nonatomic,weak) IBOutlet UILabel *volumentLbl;
@property(nonatomic,weak) IBOutlet UILabel *elementoLbl;
@property(nonatomic,weak) IBOutlet UILabel *salObraLbl;
@property(nonatomic,weak) IBOutlet UILabel *llegObraLbl;
@property(nonatomic,weak) IBOutlet UILabel *teoLbl;
@property(nonatomic,weak) IBOutlet UIView *tipoView;
@end