//
//  SideMenuViewController.h
//  unicon
//
//  Created by DSB Mobile on 1/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface SideMenuViewController:UIViewController<UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate>{
    NSArray *arrOptions;
    int suboptionsCount;
}
@property(nonatomic,weak) IBOutlet UITableView *menuTableView;
@property(nonatomic,strong) NSNumber *currentIndex;
@end
