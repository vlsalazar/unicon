//
//  UsuarioViewController.m
//  unicon
//
//  Created by victor salazar on 4/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "UserViewController.h"
#import "Obra.h"
#import "Empresa.h"
@implementation UserViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    animating = false;
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Si el usuario actual es cliente UNICON se obtiene las obras | INICIO
    if(![[UsuarioActual currentUser] esUsuarioUnicon]){
        [self getObras];
    }
    // 20170321 | Victor Salazar | Inicio de sesión automatico - Si el usuario actual es cliente UNICON se obtiene las obras | FIN
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"sideMenuSegue"]){
        sideMenuViewCont = segue.destinationViewController;
    }else if([segue.identifier isEqualToString:@"menuSegue"]){
        menuViewCont = (MenuViewController *)[segue.destinationViewController topViewController];
    }
}
-(BOOL)prefersStatusBarHidden{
    return false;
}
#pragma mark - Auxiliar
-(void)showMainMenu{
    if(![menuViewCont.navigationController.viewControllers.lastObject isEqual:menuViewCont]){
        animating = true;
        [menuViewCont.navigationController popToRootViewControllerAnimated:true];
        [UIView animateWithDuration:0.5 animations:^{
            self.sideMenuView.transform = CGAffineTransformMakeTranslation(-self.sideMenuView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            self.sideMenuView.transform = CGAffineTransformIdentity;
            self.sideMenuView.hidden = true;
            animating = false;
        }];
    }
}
-(void)showSideMenuWithOption:(int)option{
    if(!animating){
        animating = true;
        sideMenuViewCont.currentIndex = @(option);
        [sideMenuViewCont.menuTableView reloadData];
        self.sideMenuView.transform = CGAffineTransformMakeTranslation(-self.sideMenuView.frame.size.width, 0);
        self.sideMenuView.hidden = false;
        [UIView animateWithDuration:0.5 animations:^{
            self.sideMenuView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            animating = false;
        }];
    }
}
-(void)hideSideMenuWithOption:(NSString *)option{
    if(!animating){
        animating = true;
        [UIView animateWithDuration:0.5 animations:^{
            self.sideMenuView.transform = CGAffineTransformMakeTranslation(-self.sideMenuView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            self.sideMenuView.transform = CGAffineTransformIdentity;
            self.sideMenuView.hidden = true;
            animating = false;
            [menuViewCont optionSelected:option];
        }];
    }
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se implementan los metodos de obtencion de obras y obras de credito | INICIO
-(void)getObras{
    UsuarioActual *currentUser = [UsuarioActual currentUser];
    [SVProgressHUD showWithStatus:@"Obteniendo Obras"];
    NSDictionary *dicParams = @{@"codigoCliente": currentUser.codigoCliente, @"codigoUsuario": currentUser.usuario};
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    [ServiceConector connectToUrl:[ServiceUrl getUrlObras] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        if(error){
            [ToolBox showAlertErrorWithError:error withOkHandler:^(UIAlertAction *action) {
                [self getObras];
            } inViewCont:self];
        }else{
            NSError *jsonError;
            id dicResult = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                [ToolBox showAlertErrorWithError:jsonError withOkHandler:^(UIAlertAction *action){
                    [self getObras];
                } inViewCont:self];
            }else{
                if(dicResult != [NSNull null]){
                    id objectObraTemp = [dicResult objectForKey:@"obra"];
                    if([objectObraTemp isKindOfClass:[NSDictionary class]]){
                        objectObraTemp = @[objectObraTemp];
                    }
                    NSMutableArray *arrObrasTemp = [NSMutableArray arrayWithCapacity:0];
                    Obra *obraTemp = [Obra new];
                    obraTemp.codigo = @"";
                    obraTemp.descripcion = @"TODOS";
                    [arrObrasTemp addObject:obraTemp];
                    for(NSDictionary *dicObra in objectObraTemp){
                        Obra *obraTemp = [Obra new];
                        obraTemp.codigo = [dicObra objectForKey:@"codigo"];
                        obraTemp.descripcion = [dicObra objectForKey:@"descripcion"];
                        [arrObrasTemp addObject:obraTemp];
                    }
                    currentUser.arrObras = arrObrasTemp;
                    [self getObrasCredito];
                }else{
                    [ToolBox showNoDataInViewCont:self withOkHandler:^(UIAlertAction *action) {
                        [self getObras];
                    }];
                }
            }
        }
    }];
}
-(void)getObrasCredito{
    [SVProgressHUD showWithStatus:@"Obteniendo Obras Credito"];
    UsuarioActual *currentUser = [UsuarioActual currentUser];
    NSDictionary *dicParams = @{@"codigoCliente": currentUser.codigoCliente, @"codigoUsuario": currentUser.usuario};
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    [ServiceConector connectToUrl:[ServiceUrl getUrlObrasCredito] withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        if(error){
            [ToolBox showAlertErrorWithError:error withOkHandler:^(UIAlertAction *action) {
                [self getObrasCredito];
            } inViewCont:self];
        }else{
            NSError *jsonError;
            id dicResult = [NSJSONSerialization JSONObjectWithData:receivedData options:NSJSONReadingAllowFragments error:&jsonError];
            if(jsonError){
                [ToolBox showAlertErrorWithError:jsonError withOkHandler:^(UIAlertAction *action){
                    [self getObrasCredito];
                } inViewCont:self];
            }else{
                if(dicResult != [NSNull null]){
                    id objectObraTemp = [dicResult objectForKey:@"obra"];
                    if([objectObraTemp isKindOfClass:[NSDictionary class]]){
                        objectObraTemp = @[objectObraTemp];
                    }
                    NSMutableArray *arrObrasTemp = [NSMutableArray arrayWithCapacity:0];
                    Obra *obraTemp = [Obra new];
                    obraTemp.codigo = @"";
                    obraTemp.descripcion = @"TODOS";
                    [arrObrasTemp addObject:obraTemp];
                    for(NSDictionary *dicObra in objectObraTemp){
                        Obra *obraTemp = [Obra new];
                        obraTemp.codigo = [dicObra objectForKey:@"codigo"];
                        obraTemp.descripcion = [dicObra objectForKey:@"descripcion"];
                        [arrObrasTemp addObject:obraTemp];
                    }
                    currentUser.arrObrasCredito = arrObrasTemp;
                }else{
                    [ToolBox showNoDataInViewCont:self withOkHandler:^(UIAlertAction *action) {
                        [self getObrasCredito];
                    }];
                }
            }
        }
    }];
}
// 20170321 | Victor Salazar | Inicio de sesión automatico - Se implementan los metodos de obtencion de obras y obras de credito | FIN
@end
