//
//  CertificadosDetalleViewController.m
//  unicon
//
//  Created by DSB Mobile on 3/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "CertificadosDetalleViewController.h"
@implementation CertificadosDetalleViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    int status = [[self.dicCertificado objectForKey:@"status"] intValue];
    id diasFaltantes = [self.dicCertificado objectForKey:@"diasFaltantes"];
    int intDFaltantes = [diasFaltantes intValue];
    BOOL bFgPerdida = [[self.dicCertificado objectForKey:@"fgPerdida"] isEqualToString:@"true"];
    BOOL bFgMuestreo = [[self.dicCertificado objectForKey:@"fgMuestreo"] isEqualToString:@"true"];
    NSString *strEstadoEnsayoCompresion = [[self.dicCertificado objectForKey:@"estadoEnsayoCompresion"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strMotivoPerdida = [[self.dicCertificado objectForKey:@"motivoPerdida"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strPromResis = [self.dicCertificado objectForKey:@"promedioResistencias"];
    double promedioResistencias = [strPromResis doubleValue];
    BOOL mostrarResultados = (![strEstadoEnsayoCompresion isEqualToString:@"ANU"] && ![strEstadoEnsayoCompresion isEqualToString:@"PAU"] && ![strEstadoEnsayoCompresion isEqualToString:@"REV"] && ![strEstadoEnsayoCompresion isEqualToString:@"RCH"]) && !bFgPerdida;
    BOOL esDiasFaltantes = (intDFaltantes > 0) && ([strEstadoEnsayoCompresion isEqualToString:@"PEN"]) && !([strMotivoPerdida isEqualToString:@"PER"]) && !([strMotivoPerdida isEqualToString:@"FAL"]);
    self.tituloLbl.text = [self.dicCertificado objectForKey:@"descripcionObra"];
    self.numGuiaLbl.text = [NSString stringWithFormat:@"%@-%@", [self.dicCertificado objectForKey:@"serieGuia"], [self.dicCertificado objectForKey:@"numeroGuia"]];
    self.fechaVacLbl.text = [ToolBox getddMMyyyyForStringDate:[self.dicCertificado objectForKey:@"fechaVaciado"]];
    self.fechaEnLbl.text = [ToolBox getddMMyyyyForStringDate:[self.dicCertificado objectForKey:@"fechaCertificado"]];
    self.estadoLbl.text = (status == 1)?@"Carta Generada":((status == 2)?@"Certificado en Proceso":((status == 3)?@"Certificado Generado":@""));
    NSMutableAttributedString *resulAttrString = [[NSMutableAttributedString alloc] initWithAttributedString:self.resultadosTituloLbl.attributedText];
    [resulAttrString addAttribute:NSBaselineOffsetAttributeName value:@3 range:NSMakeRange(resulAttrString.length - 2, 1)];
    self.resultadosTituloLbl.attributedText = resulAttrString;
    self.resultadosLbl.text = mostrarResultados?strPromResis:@"";
    self.diasFaltantesLbl.text = esDiasFaltantes?[NSString stringWithFormat:@"Falta%@ %i día%@", (intDFaltantes > 1)?@"n":@"", intDFaltantes, (intDFaltantes > 1)?@"s":@""]:@"";
    BOOL checkVisibility = ((![strEstadoEnsayoCompresion isEqualToString:@"ANU"] && ![strEstadoEnsayoCompresion isEqualToString:@"PAU"] && ![strEstadoEnsayoCompresion isEqualToString:@"REV"] && ![strEstadoEnsayoCompresion isEqualToString:@"RCH"]) || [strMotivoPerdida isEqualToString:@"PER"] || [strMotivoPerdida isEqualToString:@"FAL"]);
    BOOL checkEnabled = !(!bFgMuestreo || ((promedioResistencias == 0) && ![strMotivoPerdida isEqualToString:@"PER"] && ![strMotivoPerdida isEqualToString:@"FAL"]));
    if(!(checkEnabled && checkVisibility)){
        self.heightExportConstraint.constant = 0;
    }
}
#pragma mark - IBAction
-(IBAction)actionExportar:(id)sender{
    int status = [[self.dicCertificado objectForKey:@"status"] intValue];
    NSDictionary *dicParams = @{@"usuario": [[UsuarioActual currentUser] codigoUsuario], @"guias": @[@{@"numeroGuia": [self.dicCertificado objectForKey:@"numeroGuia"], @"serieGuia": [self.dicCertificado objectForKey:@"serieGuia"], @"motivoPerdida": @""}]};
    NSString *strURL = (status != 1)?[ServiceUrl getUrlCertificadosPDFCertificado]:[ServiceUrl getUrlCertificadosPDFComunicado];
    [SVProgressHUD showWithStatus:@"Cargando" maskType:SVProgressHUDMaskTypeBlack];
    [ServiceConector connectToUrl:strURL withParameters:dicParams withResponse:^(id receivedData, NSError *error){
        [SVProgressHUD dismiss];
        if(error){
            if([ToolBox isErrorConnectionInternet:error.code]){
                [ToolBox showAlertaErrorConnectionInViewCont:self];
            }else{
                if(error.code == NSURLErrorTimedOut){
                    [ToolBox showAlertaTimeOutInViewCont:self];
                }else{
                    [ToolBox showAlertaErrorInViewCont:self];
                }
            }
        }else{
            NSData *currentData = receivedData;
            if(currentData.length == 0){
                [ToolBox showNoDataInViewCont:self];
            }else{
                NSArray *arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *strDocumentDirectory = arr.firstObject;
                NSString *filePath = [strDocumentDirectory stringByAppendingPathComponent:@"file.pdf"];
                [currentData writeToFile:filePath atomically:true];
                UIDocumentInteractionController *docIntCont = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
                docIntCont.name = @"";
                docIntCont.delegate = self;
                [docIntCont presentPreviewAnimated:true];
            }
        }
    }];
}
#pragma mark - UIDocumentInteractionCont
-(UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}
@end
