//
//  AcuerComerDocTableViewCell.h
//  unicon
//
//  Created by victor salazar on 30/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface AcuerComerDocTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *docLbl;
@end