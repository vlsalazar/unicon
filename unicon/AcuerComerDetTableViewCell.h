//
//  AcuerComerDetTableViewCell.h
//  unicon
//
//  Created by victor salazar on 27/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface AcuerComerDetTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UIButton *backBtn;
@property(nonatomic,weak) IBOutlet UIView *view1;
@property(nonatomic,weak) IBOutlet UIView *view2;
@property(nonatomic,weak) IBOutlet UIView *view3;
@property(nonatomic,weak) IBOutlet UIView *view4;
@property(nonatomic,weak) IBOutlet UILabel *fechaAcuerLbl;
@property(nonatomic,weak) IBOutlet UILabel *numAcuerLbl;
@property(nonatomic,weak) IBOutlet UILabel *obraLbl;
@property(nonatomic,weak) IBOutlet UISwitch *selectedSwitch;
@end