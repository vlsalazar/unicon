//
//  EstadoCreditoViewController.h
//  unicon
//
//  Created by victor salazar on 8/07/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface EstadoCreditoViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicEstadoCredito;
@property(nonatomic,weak) IBOutlet UILabel *lineaCreditoLbl;
@property(nonatomic,weak) IBOutlet UILabel *deudaTotalLbl;
@property(nonatomic,weak) IBOutlet UILabel *facturadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *porFacturarLbl;
@property(nonatomic,weak) IBOutlet UILabel *saldoFavorLbl;
@property(nonatomic,weak) IBOutlet UILabel *sAdelantadosLbl;
@property(nonatomic,weak) IBOutlet UILabel *sEntregarLbl;
@property(nonatomic,weak) IBOutlet UILabel *sFacAdelantadaLbl;
@property(nonatomic,weak) IBOutlet UILabel *estadoLbl;
@property(nonatomic,weak) IBOutlet UILabel *totalLbl;
@end