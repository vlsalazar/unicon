//
//  User.m
//  unicon
//
//  Created by victor salazar on 31/05/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "UsuarioActual.h"
@implementation UsuarioActual
+(UsuarioActual *)currentUser{
    static UsuarioActual *currentUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentUser = [[self alloc] init];
    });
    return currentUser;
}
+(void)cargarUsuarioActualInfo:(Usuario *)usuario{
    UsuarioActual *currentUser = [UsuarioActual currentUser];
    currentUser.correo = usuario.correo;
    currentUser.nombre = usuario.nombre;
    currentUser.usuario = usuario.usuario;
    currentUser.codigoUsuario = usuario.codigoUsuario;
    currentUser.codigoCliente = usuario.codigoCliente;
    currentUser.descripcionCliente = usuario.descripcionCliente;
    NSMutableArray *arrMenu = [ToolBox getMenu];
    [[arrMenu objectAtIndex:0] setObject:[NSNumber numberWithBool:usuario.estatusHabilitado] forKey:@"enabled"];
    [[arrMenu objectAtIndex:1] setObject:[NSNumber numberWithBool:usuario.gestionHabilitado] forKey:@"enabled"];
    [[arrMenu objectAtIndex:2] setObject:[NSNumber numberWithBool:usuario.certificadosHabilitado] forKey:@"enabled"];
    [[arrMenu objectAtIndex:3] setObject:[NSNumber numberWithBool:usuario.acuerdosHabilitado] forKey:@"enabled"];
    [[arrMenu objectAtIndex:4] setObject:[NSNumber numberWithBool:usuario.creditosHabilitado] forKey:@"enabled"];
    [[arrMenu objectAtIndex:5] setObject:[NSNumber numberWithBool:usuario.consultasHabilitado] forKey:@"enabled"];
    currentUser.arrMenu = arrMenu;
}
+(NSString *)codigoCliente{
    UsuarioActual *usuario = [UsuarioActual currentUser];
    return [usuario esUsuarioUnicon] ? (usuario.empresaSeleccionada?usuario.empresaSeleccionada.codigoCliente:@"") : usuario.codigoCliente;
}
+(NSString *)codigoUsuario{
    UsuarioActual *usuario = [UsuarioActual currentUser];
    return [usuario esUsuarioUnicon] ? @"" : usuario.usuario;
}
-(BOOL)esUsuarioUnicon{
    return (self.codigoCliente.length == 0);
}
@end
