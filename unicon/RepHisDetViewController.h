//
//  RepHisDetViewController.h
//  unicon
//
//  Created by victor salazar on 24/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface RepHisDetViewController:UIViewController
@property(nonatomic,weak) NSDictionary *dicDespacho;
@property(nonatomic,weak) IBOutlet UITextView *detTxtView;
@end