//
//  ServiceUrl.h
//  unicon
//
//  Created by DSB Mobile on 2/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <Foundation/Foundation.h>
#define baseURLKey @"com.unicon.unicon.baseURL"
@interface ServiceUrl:NSObject
+(NSString *)getUrlLogin;
+(NSString *)getUrlEmpresas;
+(NSString *)getUrlObras;
+(NSString *)getUrlObrasCredito;
+(NSString *)getUrlProgramaSemana;
+(NSString *)getUrlProgramaSemanaPDF;
+(NSString *)getUrlEmpresasUsuarioUnicon;
+(NSString *)getUrlDespachoHoyMapa;
+(NSString *)getUrlDespachoHoyLista;
+(NSString *)getUrlReporteHistoricoResumen;
+(NSString *)getUrlReporteHistorico;
+(NSString *)getUrlReporteHistoricoPDF;
+(NSString *)getUrlGestionDescarga;
+(NSString *)getUrlGestionProgramacion;
+(NSString *)getUrlPuntualidadServicio;
+(NSString *)getUrlAcuerdosComerciales;
+(NSString *)getUrlAcuerdosComercialesCotizacion;
+(NSString *)getUrlAcuerdosComercialesDocumentos;
+(NSString *)getUrlAcuerdosComercialesPDF;
+(NSString *)getUrlCertificados;
+(NSString *)getUrlCertificadosPDFCertificado;
+(NSString *)getUrlCertificadosPDFComunicado;
+(NSString *)getUrlEstadoCuenta;
+(NSString *)getUrlEstadoCuentaPDF;
+(NSString *)getUrlEntregaClienteObra;
+(NSString *)getUrlEntregaClienteObraPDF;
+(NSString *)getUrlHistorialPagos;
+(NSString *)getUrlHistorialPagosPDF;
+(NSString *)getUrlComprobanteRecepcion;
+(NSString *)getUrlComprobanteRecepcionPDF;
+(NSString *)getUrlEstadoCredito;
+(NSString *)getUrlEstadoCreditoPDF;
@end