//
//  RepHisDetTableViewCell.h
//  unicon
//
//  Created by victor salazar on 24/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface RepHisDetTableViewCell:UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *nPedidoLbl;
@property(nonatomic,weak) IBOutlet UILabel *progNroLbl;
@property(nonatomic,weak) IBOutlet UILabel *guiaLbl;
@property(nonatomic,weak) IBOutlet UILabel *plantaLbl;
@property(nonatomic,weak) IBOutlet UILabel *camionLbl;
@property(nonatomic,weak) IBOutlet UILabel *disenoLbl;
@property(nonatomic,weak) IBOutlet UILabel *volumenLbl;
@property(nonatomic,weak) IBOutlet UILabel *elementoLbl;
@property(nonatomic,weak) IBOutlet UILabel *teoLbl;
@end