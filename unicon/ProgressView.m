//
//  ProgressView.m
//  unicon
//
//  Created by victor salazar on 13/06/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
#import "ProgressView.h"
@implementation ProgressView
-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 4;
        avanceView = [[UIView alloc] init];
        avanceView.backgroundColor = [ToolBox getColorBlue];
        [self addSubview:avanceView];
    }
    return self;
}
-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    float totalWidth = self.frame.size.width - 2.0;
    float avanceWidth = self.progress*totalWidth/100.0;
    avanceView.frame = CGRectMake(1, 1, avanceWidth, self.frame.size.height - 2.0);
}
-(void)setProgress:(float)progress{
    _progress = progress;
    [self bringSubviewToFront:self.avanceLbl];
    self.avanceLbl.text = [NSString stringWithFormat:@"%.0f%@",floor(self.progress), @"%"];
}
@end